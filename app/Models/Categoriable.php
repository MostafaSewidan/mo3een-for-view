<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Categoriable extends Model 
{

    protected $table = 'categoriables';
    public $timestamps = true;
    protected $fillable = array('category_id','categoriable_type', 'categoriable_id');

}