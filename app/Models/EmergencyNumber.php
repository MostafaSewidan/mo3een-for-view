<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmergencyNumber extends Model
{

    protected $table      = 'emergency_numbers';
    public    $timestamps = true;
    protected $fillable   = array('title', 'number', 'order');

    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }

    public function attachment()
    {
        return $this->morphOne('App\Models\Attachment', 'attachmentable');
    }

}
