<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Body extends Model
{
    protected $table      = 'bodies';
    public    $timestamps = true;
    protected $fillable   = array('full_name', 'order_id', 'death_date', 'national_id', 'transport_place',
                                  'death_number', 'reporter_name', 'phone', 'reporter_phone', 'notes');

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }
}
