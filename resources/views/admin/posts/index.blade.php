@extends('admin.layouts.main',[
								'page_header'		=> 'الإرشادات الطبية',
								'page_description'	=> 'عرض ',
								'link' => url('admin/posts/')
								])
@section('content')

    <div class="ibox box-primary">
        <div class="ibox-title">
            <div class="pull-right">


                <a href="{{url('admin/posts/create')}}" class="btn btn-primary">
                    <i class="fa fa-plus"></i> اضافة جديد
                </a>
            </div>
            <div class="clearfix"></div>
            <div class="box-body">
                <div class="box-header row">
                    {!! Form::open([
                    'method' => 'GET'
                ]) !!}
                    <div class="col-md-3">
                        <div class="form-group">
                            {!! Form::text('name',old('name'),[
                                'class' => 'form-control',
                                'placeholder' => 'البحث'
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::text('from',request()->input('from'),[
                            'class' => 'form-control datepicker',
                            'placeholder' => 'من تاريخ',
                            'autocomplete' => 'off'
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            {!! Form::text('to',request()->input('to'),[
                            'class' => 'form-control datepicker',
                            'placeholder' => 'إلى تاريخ',
                            'autocomplete' => 'off'
                            ]) !!}
                        </div>
                    </div>

                    <div class="col-md-2">
                        <div class="form-group">
                            <button class="btn btn-primary btn-block" type="submit">
                                <i class="fa fa-search"></i>
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="ibox-content">
            @if(!empty($records) && count($records)>0)
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <th>#</th>
                        <th>العنوان</th>
                        <th class="text-center">الصورة</th>
                        <th class="text-center">الحالة</th>
                        <th class="text-center">التفاصيل</th>
                        <th class="text-center">تعديل</th>
                        <th class="text-center">حذف</th>
                        </thead>
                        <tbody>
                        @php $count = 1; @endphp
                        @foreach($records as $record)
                            <tr id="removable{{$record->id}}">
                                <td>{{$count}}</td>
                                <td>{{optional($record)->title}}</td>

                                <td class="text-center">
                                    <a href="{{asset($record->photo)}}" data-lity>
                                        <img src="{{asset($record->photo)}}" alt="image" class="img-circle" width="50"
                                             height="50">
                                    </a>
                                </td>


                                <td class="text-center">
                                    {!! \App\MyHelper\Helper::toggleBooleanView($record , url('admin/posts/toggle-boolean/'.$record->id.'/is_active/')) !!}
                                </td>

                                <td class="text-center">

                                    <button style="background-color: white;border: none"
                                            data-toggle="modal" data-target="#client{{$record->id}}">
                                        <i class="btn btn-xs btn-info fa fa-eye" style="padding: 4px 6px"></i>
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="client{{$record->id}}" tabindex="-1" role="dialog"
                                         aria-labelledby="myModalLabel">

                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close"><span aria-hidden="true">&times;</span>
                                                    </button>


                                                    <h2 style="font-weight: bold" class="modal-title"
                                                        id="myModalLabel">{{$record->title}}</h2>
                                                </div>
                                                <br><br>
                                                <div style="    margin-right: 25%;    margin-left: 30%;">
                                                    <a href="{{$record->photo}}"
                                                       data-lity>
                                                        <img src="{{$record->photo}}"
                                                             alt="image" width="300" style="border-radius: 7px">
                                                    </a>
                                                </div>
                                                <br><br>
                                                <hr>
                                                <div class="modal-body">
                                                    <p><strong>الوصف
                                                            : </strong> {{$record->short_description}}
                                                    </p>
                                                    <p><strong>الوصف التفصيلي
                                                            : </strong> {{$record->long_description}}</p>

                                                </div>
                                                <div class="modal-footer">
                                                    <p><strong>تاريخ الاضافة
                                                            : </strong> {{$record->created_at->locale('ar')->isoFormat('dddd  , MMMM  ,  Do / YYYY  ,  الساعة h:mm')}}
                                                    </p>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal"
                                                            style="background-color: #00c0ef;color: white">
                                                        إغلاق
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="text-center"><a href="{{url('admin/posts/' . $record->id .'/edit')}}"
                                                           class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a>
                                </td>
                                <td class="text-center">
                                    <button id="{{$record->id}}"
                                            data-token="{{ csrf_token() }}"
                                            data-route="{{URL::route('posts.destroy',$record->id)}}"
                                            type="button" class="destroy btn btn-danger btn-xs"><i
                                                class="fa fa-trash-o"></i></button>
                                </td>
                            </tr>
                            @php $count ++; @endphp
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {!! $records->render() !!}
            @else
                <div>
                    <h3 class="text-info" style="text-align: center"> لا توجد بيانات للعرض </h3>
                </div>
            @endif


        </div>
    </div>
@stop

@section('script')
    <script>
        lightbox.option({
            'resizeDuration': 200,
            'wrapAround': true,
            'showImageNumberLabel': false,

        })
    </script>
@stop
