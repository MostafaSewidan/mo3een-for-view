<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration {

	public function up()
	{
		Schema::create('posts', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->text('short_description');
			$table->text('long_description');
			$table->tinyInteger('is_active')->default('1');
			$table->integer('order')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('posts');
	}
}