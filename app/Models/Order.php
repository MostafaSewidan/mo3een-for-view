<?php

namespace App\Models;

use App\MyHelper\Helper;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    protected $dates =
        [
            'start_trip',
            'end_trip',
            'asked_at',
            'schedule_date',
        ];

    protected $table = 'orders';
    public $timestamps = true;
    protected $fillable = array(
        'pharmacy_id',
        'client_id',
        'delivery_id',
        'description',
        'delivery_id',
        'service_id',
        'car_type_id',
        'price',
        'start_price_range',
        'end_price_range',
        'amount_paid',
        'real_price',
        'remaining_amount',
        'start_address_id',
        'payment_method_id',
        'phone',
        'end_address_id',
        'reason',
        'status', 'notes',
        'address_id',
        'reason', 'start_trip',
        'end_trip',
        'schedule_date',
        'asked_at',
        'arrival_Expected_time',
        'distance',
    );

    protected $appends = [
        'client_status_text',
        'provider_status_text',
        'type',
        'expected_time_text',
        'real_time',
        'real_time_text',
    ];

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

    public function carType()
    {
        return $this->belongsTo(CarType::class);
    }

    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }

    public function eventDeliveries()
    {
        return $this->belongsToMany(Delivery::class , 'delivery_order');
    }

    public function requestEventDeliveries()
    {
        return $this->belongsToMany(Delivery::class , 'delivery_order_requests');
    }

    public function paymentMethod()
    {
        return $this->belongsTo(PaymentMethod::class);
    }

    public function photo()
    {
        return $this->morphOne(Attachment::class, 'attachmentable');
    }

    public function startAddress()
    {
        return $this->belongsTo(Address::class, 'start_address_id');
    }

    public function endAddress()
    {
        return $this->belongsTo(Address::class, 'end_address_id');
    }

    public function hospital()
    {
        return $this->belongsTo(Hospital::class, 'hospital_id');
    }

    public function pharmacy()
    {
        return $this->belongsTo(Pharmacy::class);
    }

    public function delivery()
    {
        return $this->belongsTo(Delivery::class);
    }

    public function coupon()
    {
        return $this->belongsTo('App\Models\Coupon');
    }

    public function walletTransaction()
    {
        return $this->hasOne('App\Models\WalletTransaction');
    }

    public function hospitals()
    {
        return $this->belongsToMany(Hospital::class , 'event_hospital_requests');
    }

    public function doctorsAndNurses()
    {
        return $this->belongsToMany(Hospital::class)->withPivot('type','number');
    }

    public function hospitalDoctors()
    {
        return $this->belongsToMany(Hospital::class)->where('type','doctor')->withPivot('type','number');
    }

    public function hospitalNurses()
    {
        return $this->belongsToMany(Hospital::class)->where('type','nurse')->withPivot('type','number');
    }

    public function requestHospitals()
    {
        return $this->belongsToMany(Hospital::class,'hospital_order_requests')->withPivot('type','number');
    }

    public function requestHospitalDoctors()
    {
        return $this->belongsToMany(Hospital::class,'hospital_order_requests')->where('type','doctor')->withPivot('type','number');
    }

    public function requestHospitalNurses()
    {
        return $this->belongsToMany(Hospital::class,'hospital_order_requests')->where('type','nurse')->withPivot('type','number');
    }

    public function deliveryRejectReasons()
    {
        return $this->hasMany(DeliveryRejectReasons::class);
    }

    public function review()
    {
        return $this->morphOne(Review::class, 'reviewable');
    }

    public function pointsTransaction()
    {
        return $this->hasOne('App\Models\PointsTransaction');
    }

    public function patient()
    {
        return $this->hasOne('App\Models\Patient');
    }

    public function notifications()
    {
        return $this->morphMany('App\Models\Notification', 'notifiable');
    }

    public function getClientStatusTextAttribute()
    {
        $status_text = '';

        switch ($this->status) {
            case 'pending':
                $status_text = 'رحلة جديد';
                break;
            case 'accepted':
                $status_text = 'تم قبول الرحلة';
                break;
            case 'arrives_to_patient':
                $status_text = 'وصول للحالة';
                break;
            case 'move_with_patient':
                $status_text = 'تحرك بالحالة';
                break;
            case 'arrived_to_hospital':
                $status_text = 'وصول للمستشفي';
                break;
            case 'End_journey':
                $status_text = 'رحلة ناجحة';
                break;
            case 'cancel':
                $status_text = 'رحلة ملغية';
                break;
            case 'rejected':
                $status_text = 'رحلة مرفوضة';
                break;

            // events status
            case 'check_out':
                $status_text = 'إتمام الدفع';
                break;
            case 'scheduled':
                $status_text = 'طلب مقبول';
                break;

            // written orders status
            case 'arrives_to_pharmacy':
                $status_text = 'وصل للصيدلية';
                break;
            case 'delivery_loaded':
                $status_text = 'إستلم الطلب';
                break;
            case 'delivery_go':
                $status_text = 'بدأ بالتحرك تجاهك';
                break;
            case 'arrived_to_client':
                $status_text = 'الطلب وصل';
                break;
        }

        return $status_text;
    }

    public function getProviderStatusTextAttribute()
    {
        $status_text = '';

        switch ($this->status) {
            case 'pending':
                $status_text = 'رحلة جديد';
                break;
            case 'accepted':
                $status_text = 'قبول الرحلة';
                break;
            case 'arrives_to_patient':
                $status_text = 'وصول للحالة';
                break;
            case 'move_with_patient':
                $status_text = 'تحرك بالحالة';
                break;
            case 'arrived_to_hospital':
                $status_text = 'وصول للمستشفي';
                break;
            case 'End_journey':
                $status_text = 'إنهاء الرحلة';
                break;
            case 'cancel':
                $status_text = 'رحلة ملغية';
                break;
            case 'rejected':
                $status_text = 'رحلة مرفوضة';
                break;

            // written orders status
            case 'arrives_to_pharmacy':
                $status_text = 'وصول للصيدلية';
                break;
            case 'delivery_loaded':
                $status_text = 'إستلام الطلب';
                break;
            case 'delivery_go':
                $status_text = 'التحرك للعميل';
                break;
            case 'arrived_to_client':
                $status_text = 'وصلت للعميل';
                break;
        }

        return $status_text;
    }

    public function getTypeAttribute()
    {
        $type = $this->service ? $this->service->type : null;
        return $type;
    }

    public function getExpectedTimeTextAttribute()
    {
        $time = $this->minutesTOString($this->arrival_Expected_time);
        return $time; // 01:00
    }

    public function getRealTimeAttribute()
    {
        $startTrip = $this->start_trip;
        $endTrip = $this->end_trip;

        if ($startTrip && $endTrip) {
            $diff_in_minutes = $endTrip->diffInMinutes($startTrip);

            return $diff_in_minutes; // 01:00
        }

        return null;
    }

    public function getRealTimeTextAttribute()
    {
        $startTrip = $this->start_trip;
        $endTrip = $this->end_trip;

        if ($startTrip && $endTrip) {
            $diff_in_minutes = $endTrip->diffInMinutes($startTrip);

            $time = $this->minutesTOString($diff_in_minutes);

            return $time; // 01:00
        }

        return null;
    }

    /**
     * @param $time
     * @return string
     */
    private function minutesTOString($time , $hours = 0): string
    {
        if ($time) {

            if($time >= 60)
            {
                $hours += 1;
                $time -= 60;
                $this->minutesTOString($time);
            }

            $minute = $time;

            $time = ($hours == 0 ? '00' : ($hours < 10 ? '0' . $hours : $hours)) . ':' . ($minute < 10 ? '0' . $minute : $minute);
            return $time;
        }
        return '';
    }

    public function scopeCanUpdateStatus($query)
    {
        return $query->whereDate('schedule_date', '<=', Helper::convertDateTime(Carbon::now()));
    }
}
