@inject(countries , App\Models\Country)
@inject(services , App\Models\Service)
@inject(car_types , App\Models\CarType)
@inject(cities , App\Models\City)
@inject(regions , App\Models\Region)

<div class="row">
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-6">
                {!! \App\MyHelper\Field::fileWithPreview('photo','صورة السائق') !!}
            </div>
            <div class="col-sm-6 text-left">
                @if($model->photo)
                    <div class="col-md-4">
                        <img src="{{url($model->photo)}}" alt="" class="img-responsive thumbnail">
                    </div>
                    <div class="clearfix"></div>
                @endif
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="row">
            <div class="col-sm-6">
                {!! \App\MyHelper\Field::fileWithPreview('transport_photo','صورة السيارة') !!}
            </div>
            <div class="col-sm-6 text-left">
                @if($model->transport_photo)
                    <div class="col-md-4">
                        <img src="{{url($model->transport_photo)}}" alt="" class="img-responsive thumbnail">
                    </div>
                    <div class="clearfix"></div>
                @endif
            </div>
        </div>
    </div>
</div>

{!! \App\MyHelper\Field::multiSelect('services' , 'الخدمات' , $services->whereIn('type' , ['care','pharmacy','provider'])->pluck('name','id')->toArray())!!}
{!! \App\MyHelper\Field::select('car_type_id' , 'نوع السيارة' , $car_types->pluck('name','id')->toArray())!!}
{!! \App\MyHelper\Field::select('country' , 'منطقة' , $countries->pluck('name','id')->toArray() , $model->region ?optional($model->region->city->country)->id : null)!!}


{!! \App\MyHelper\Field::select(
'city' ,
 'مدينة' ,
 $model->region ?
 $cities->where('country_id' , optional($model->region->city->country)->id)->pluck('name','id')->toArray()
  :
  [] ,
  $model->region ?optional($model->region->city)->id : null
  )!!}

{!! \App\MyHelper\Field::select('region_id' , 'الحي' ,
$model->region ? $regions->where('city_id' , optional($model->region->city)->id)->pluck('name','id')->toArray() :  []
, $model->region ?optional($model->region)->id : null)!!}

{!! \App\MyHelper\Field::text('name' , 'الاسم')!!}
{!! \App\MyHelper\Field::text('user_name' , 'إسم دخول المستخدم')!!}
{!! \App\MyHelper\Field::email('email' , 'البريد الإلكتروني')!!}
{!! \App\MyHelper\Field::number('phone' , 'رقم الهاتف')!!}
{!! \App\MyHelper\Field::date('d_o_b' , 'تاريخ الميلاد' ,
null,
 \Carbon\Carbon::now()->subYears(3)->toDateString() ,
  \Carbon\Carbon::now()->subYears(200)->toDateString())
  !!}
{!! \App\MyHelper\Field::number('car_number' , 'رقم السيارة')!!}
{!! \App\MyHelper\Field::text('nationality' , 'جنسية السائق')!!}


@push('scripts')

    <script>
        $(document).ready(function () {

            $('#myForm').submit(function (e) {
                e.preventDefault();
                var url = $('#myForm').attr('action');
                var form_data = new FormData(this);


                $(".help-block").hide();
                $(".has-error").removeClass('has-error');

                $('#myBtn').attr('disabled', 'disabled');


                $.ajax({
                    url: url,
                    type: 'post',
                    data: form_data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {

                        var url = data.url;

                        if (url) {

                            window.location = url;
                        }
                    },
                    error: function (data) {
                        var error = data.responseJSON.errors;

                        $('#myBtn').removeAttr('disabled');

                        $.each(error, function (key, value) {

                            $('#' + key + '_error').text('').append(value);
                            $('#' + key + '_error').parent().show();
                            $('#' + key + '_error').parent().parent().addClass('has-error');

                        });
                    }
                });
            });

            $('#country').change(function () {

                var url = '{!! url('admin/country/cities') !!}/' + $('#country').val();

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function (data) {
                        console.log(data);
                        var option = '<option>' + 'إختر المدينة' + '</option>';
                        $.each(data.data, function (index, item) {
                            option += '<option value="' + item.id + '">' + item.name + '</option>';
                        });

                        $('#city').text('').append(option);
                    },
                    error: function (data) {
                        $('#city').text('').append('<option value="">لا يوجد مدن في هذه المنطقة</option>');
                    }
                });
            });

            $('#city').change(function () {

                var url = '{!! url('admin/city/regions') !!}/' + $('#city').val();

                $.ajax({
                    url: url,
                    type: 'get',
                    success: function (data) {
                        var option = '<option>' + 'إختر الحي' + '</option>';
                        $.each(data.data, function (index, item) {
                            option += '<option value="' + item.id + '">' + item.name + '</option>';
                        });

                        $('#region_id').text('').append(option);
                    },
                    error: function (data) {
                        $('#region_id').text('').append('<option value="">لا يوجد أحياء في هذه المنطقة</option>');
                    }
                });
            });
        });
    </script>
@endpush