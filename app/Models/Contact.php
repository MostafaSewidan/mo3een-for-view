<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model 
{

    protected $table = 'contacts';
    public $timestamps = true;
    protected $fillable = array('name', 'phone', 'contact','contact_reason_id');

    public function contactable()
    {
        return $this->morphTo();
    }

    public function reason()
    {
        return $this->belongsTo('App\Models\ContactReason');
    }

}