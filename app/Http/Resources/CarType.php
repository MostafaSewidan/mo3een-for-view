<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CarType extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {


        return [
            'id' => $this->id,
            'minute_price' => number_format($this->minute_price, 2),
            'km_price' => number_format($this->km_price, 2),
            'name' => $this->name,
        ];
    }
}
