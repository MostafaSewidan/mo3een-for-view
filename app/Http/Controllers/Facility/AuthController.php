<?php
namespace App\Http\Controllers\Facility;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function viewLogin()
    {
        return view('facilities.auth.login');
    }


    public function login(Request $request)
    {

        $rules = [
            'email' => 'required|email|exists:hospital_users,email',
            'password' => 'required'
        ];

        $error_sms =
            [
                'email.required'=>'البريد الإلكتروني خطأ ',
                'email.email'=>'الرجاء إدخال البريد الإلكتروني بالشكل الصحيح ',
                'email.exists'=>'البريد الإلكتروني خطأ ',
                'password.required'=>'الرجاء ادخال كلمة المرور ',

            ];

        $data = validator()->make($request->all(), $rules,$error_sms);

        if ($data->fails()) {

            return back()->withInput()->withErrors($data->errors());

        }else{

            $remember = $request->input('remember') && $request->remember == 1 ? $request->remember : 0;

            if(auth()->guard('facilities')->attempt(['email' => $request->email , 'password' => $request->password],$remember))
            {
                return redirect('facilities/home');
            }else{
                return back()->withInput()->withErrors(['password' => 'كلمة المرور غير صحيحة ']);
            }
        }

    }

    //reset password


    public function ResetPasswordView()
    {
        return view('facilities.reset_password.index');
    }

    public function ResetPassword(Request $request)
    {
        $rules =
            [

                'old_password' => 'required',
                'password' => 'required|confirmed|min:8',

            ];

        $error_sms =
            [
                'old_password.required'=>'الرجاء ادخال كلمة المرور الحالية ',
                'password.required'=>'الرجاء ادخال كلمة المرور الجديدة',
                'password.min'=>'الرجاء ادخال كلمة المرور الجديدة اكثر من ثمان حروف',
                'password.confirmed'=>'الرجاء التاكد من كلمة المرور الجديدة',

            ];

        $validator = validator()->make($request->all() , $rules , $error_sms);

        if ($validator->fails())
        {
            return back()->withInput()->withErrors($validator->errors());
        }

        if(Hash::check($request->old_password , auth('facilities')->user()->password))
        {
            auth('facilities')->user()->update(['password'=>Hash::make($request->password)]);


            session()->flash('success' , 'تمت تحديث بنجاح');
            return redirect('facilities/reset-password');

        }else{
            $errors = ['old_password' => 'الرجاء ادخال كلمة المرور الحالية '];
            return redirect('facilities/reset-password')->withInput()->withErrors($errors);
        }

    }

}
