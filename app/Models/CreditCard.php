<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditCard extends Model 
{

    protected $table = 'credit_cards';
    public $timestamps = true;
    protected $fillable = array('client_id', 'name', 'number', 'cvv_code', 'expired_year', 'expired_month');

    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }

}