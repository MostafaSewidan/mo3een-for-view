<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOrdersTable extends Migration {

	public function up()
	{
		Schema::create('orders', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('pharmacy_id');
			$table->integer('client_id');
			$table->integer('delivery_id')->nullable();
			$table->mediumInteger('address_id')->nullable();
			$table->float('price');
			$table->float('total_price');
			$table->float('delivery_cost')->nullable();
			$table->float('offer_price')->nullable();
			$table->enum('payment_method', array(''));
			$table->string('phone')->nullable();
			$table->enum('status', array(''));
			$table->enum('type', array(''));
			$table->text('note')->nullable();
			$table->text('reason')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('orders');
	}
}