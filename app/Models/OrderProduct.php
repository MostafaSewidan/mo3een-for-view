<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderProduct extends Model 
{

    protected $table = 'order_product';
    public $timestamps = true;
    protected $fillable = array('product_id', 'order_id', 'price', 'total_price', 'quantity','id','offer_price');

    public function additions()
    {
        return $this->belongsToMany('App\Models\Addition')->withPivot('quantity','price');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

}