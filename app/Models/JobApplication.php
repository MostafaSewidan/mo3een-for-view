<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobApplication extends Model
{
    protected $fillable = ['f_name', 'l_name', 'job_id', 'phone', 'email'];


    public function job()
    {
        return $this->belongsTo(Job::class);
    }

    public function photo()
    {
        return $this->morphOne(Attachment::class, 'attachmentable');
    }

    public function getPhotoAttribute()
    {
        return $this->photos()->where('usage', null)->first() ? $this->photos()->where('usage', null)->first()->path : null;
    }

}
