<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    protected $responseTo = 'client';
    public function responseTo($responseTo = 'client'){
        $this->responseTo = $responseTo;
        return $this;
    }
    /**
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        $defultArray = [];

        if($this->responseTo == 'delivery')
        {
            $reason =  $this->deliveryRejectReasons()->where([
                'status' => 'accepted',
                'order_id' => $this->id,
                'delivery_id' => auth('api_delivery')->user()->id
            ])->first();

            $defultArray += [

                'status' => $reason ? 'rejected' : $this->status,
                'status_text' => $reason ? 'رحلة مرفوضة' : $this->provider_status_text,
                'reason' => $reason ? $reason->description : $this->reason,
                'client' => new OrderClient($this->client)
            ];

        }elseif ($this->responseTo == 'client')
        {
            $defultArray += [

                'status' => $this->status,
                'status_text' =>  $this->client_status_text,
                'reason' => $this->reason,
                'delivery' => new Delivery($this->delivery),
            ];
        }

        $defultArray += [
            'id' => $this->id,
            'type' => $this->type,
            'price' => $this->real_price ? number_format($this->real_price , 2) : ((int)$this->end_price_range) .'-'. ((int)$this->start_price_range),
            'description' => $this->description,
            'rate' => $this->review ? $this->review->rate : 0,
            'photo' => $this->photo ? asset($this->photo->path) : null,
            'date' => $this->asked_at != null ? Carbon::parse($this->asked_at)->format('d/m/Y') : null,
            'distance' => number_format($this->distance , 2),
            'arrival_Expected_time' => $this->arrival_Expected_time,
            'schedule_date' => Carbon::parse($this->schedule_date)->toDateTimeString(),
            'arrival_Expected_date' => Carbon::parse($this->schedule_date)->addMinutes($this->arrival_Expected_time)->toDateTimeString(),

            'form_data' => [
                'phone' => $this->phone,
                'type' => optional($this->patient)->type,
                'facility_name' => optional($this->patient)->facility_name,
                'name' => optional($this->patient)->name,
                'dead_name' => optional($this->patient)->dead_name,
                'national_id' => optional($this->patient)->national_id,
                'dead_national_id' => optional($this->patient)->dead_national_id,
                'diseases' => optional($this->patient)->diseases,
                'clinic_status' => optional($this->patient)->clinic_status,
                'length' => optional($this->patient)->length,
                'weight' => optional($this->patient)->weight,
                'blood_type' => optional($this->patient)->bloodType,
                'floor' => optional($this->patient)->floor,
                'elevator' => optional($this->patient)->elevator,
                'cases_num' => optional($this->patient)->cases_num,
                'notes' => optional($this->patient)->notes,
                'insurance_number' => optional($this->patient)->insurance_number,
                'death_date' => optional($this->patient)->death_date,
                'burial_permit_number' => optional($this->patient)->burial_permit_number,
                'relative_relation' => optional($this->patient)->relative_relation,
                'event_type' => optional($this->patient)->event_type,
                'cars_number' => optional($this->patient)->cars_number,
                'event_start_date' =>  Carbon::parse(optional($this->patient)->event_start_date)->format('Y-m-d'),
                'event_end_date' =>  Carbon::parse(optional($this->patient)->event_end_date)->format('Y-m-d'),
                'event_start_time' => Carbon::parse(optional($this->patient)->event_start_date)->format('H:m'),
                'event_end_time' => Carbon::parse(optional($this->patient)->event_end_date)->format('H:m'),

                'event_start' => Carbon::parse(optional($this->patient)->event_start_date)->format('d/m/Y | g:i ')
                 . (Carbon::parse(optional($this->patient)->event_start_date)->format('a') == 'am' ? ' ص ' : ' م '),
                
                'event_end' => Carbon::parse(optional($this->patient)->event_end_date)->format('d/m/Y | g:i ')
                    . (Carbon::parse(optional($this->patient)->event_start_date)->format('a') == 'am' ? ' ص ' : ' م '),

                'days_number' => optional($this->patient)->days_number,
                'nurses' => optional($this->patient)->nurses,
                'doctors' => optional($this->patient)->doctors,
            ],
            'start_address' => new Address($this->startAddress),
            'end_address' => $this->endAddress ? new Address($this->endAddress) : new NearestMedicalFacility(''),
            'check_end_address' => $this->endAddress ? new Address($this->endAddress) : new NearestMedicalFacility(''),
            'service' => new Service($this->service),
            'payment_method' => new PaymentMethod($this->paymentMethod),
            'pharmacy' => new Pharmacy($this->pharmacy),
            'hospital' => new Hospital($this->hospital),
            'facilities' => Facility::collection($this->hospitals),

        ];

        return $defultArray;
    }

    public static function collection($resource){
        return new OrderCollection($resource);
    }
}
