@if(!$delivery)

    <div class="alert alert-danger">
        <p>تعذر الحصول على البيانات</p>
    </div>

@else

    <table class="table">
        <tr>
            <th>الحالة</th>
            <td>{!! $delivery->case_label !!}</td>
        </tr>
        <tr>
            <th>الاسم</th>
            <td>{{$delivery->name}}</td>
        </tr>
        <tr>
            <th>التليفون</th>
            <td>{{$delivery->phone}}</td>
        </tr>
        <tr>
            <th>المنطقة</th>
            <td>
                @if($delivery->region)
                    {{$delivery->region->name}}
                    @if($delivery->region->city)
                        - {{$delivery->region->city->name}}
                        @if($delivery->region->city->country)
                            - {{$delivery->region->city->country->name}}
                        @endif
                    @endif
                @endif
            </td>
        </tr>
        @if($delivery->latitude && $delivery->longitude)
            <tr>
                <th>اللوكيشن</th>
                <td>
                    <a href="{{url('http://maps.google.com/?q='.$delivery->latitude.','.$delivery->longitude)}}"
                       target="_blank" class="btn btn-danger">
                        <i class="fa fa-map-marker"></i>
                    </a>
                </td>
            </tr>
        @endif
    </table>

@endif