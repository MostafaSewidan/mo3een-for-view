<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Api\ParentApi;
use App\Models\Client;
use App\MyHelper\Helper;
use App\MyHelper\Photo;
use Hash;
use Illuminate\Http\Request;

class AuthController extends ParentApi
{
//////////////////////////////
    public function __construct()
    {
        $this->helper = new Helper();
        $this->guard = 'api_client';
        $this->model = new Client();
        $this->apiResource = '\App\Http\Resources\Client';
        $this->table = 'clients';
        $this->uniqueRow = 'phone';
        $this->sendPinCodeErrorMessage = 'رقم الهاتف غير صحيح';
    }

    //confirm account after sending pin code with expired time after 2 minutes
    public function confirmAccount(Request $request)
    {
        // if user update phone number in app ( in auth )

        if (auth('api_client')->check()) {
            $client = $request->user('api_client');

            $rules =
                [
                    'phone'    => 'required|exists:clients,phone',
                    'pin_code' => 'required|numeric',
                ];

            $data = validator()->make($request->all(), $rules);

            if ($data->fails()) {

                return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
            }

            if ($client->pin_code == $request->pin_code) {

                //check pin code date time expired
                if ($this->checkPinCodeExpiredDate($client->pin_code_date_expired)) {

                    //update pin code and confirm user if not confirmed
                    $client->pin_code = $this->getPinCode();

                    $client->phone = $request->phone;
                    $client->save();


                    return $this->helper->responseJson(1, 'success', ['token' => $request->bearerToken(), 'user' => new \App\Http\Resources\Client($client)]);
                }

                return $this->helper->responseJson(0, 'كود التاكيد غير صالح , حاول مرة اخري');
            }

            return $this->helper->responseJson(0, 'كود التاكيد خطأ');
        }

        // if user register or login ( out auth )
        $rules =
            [
                'phone'    => 'required|exists:clients,phone',
                'pin_code' => 'required|numeric',
                'token'    => 'required',
                'serial_number'    => 'required', //TODO
                'os'       => 'required|in:android,ios',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }


        $client = $this->model->where('phone', $request->phone)->first();

        if ($client) {
            if ($client->pin_code == $request->pin_code) {

                //check pin code date time expired
                if ($this->checkPinCodeExpiredDate($client->pin_code_date_expired)) {

                    //update pin code and confirm user if not confirmed
                    $client->pin_code = $this->getPinCode();
                    $client->save();

                    if ($client->status == 'not_confirmed')
                        $client->status = 'active';

                    $client->save();

                    //create passport token
                    $token = $client->createToken('android')->accessToken;

                    $client->tokens()->create(['token' => $request->token, 'phone_type' => $request->os]);

                    return $this->helper->responseJson(1, 'success', ['token' => $token, 'user' => new \App\Http\Resources\Client($client)]);
                }

                return $this->helper->responseJson(0, 'كود التاكيد غير صالح , حاول مرة اخري');
            }

            return $this->helper->responseJson(0, 'كود التاكيد خطأ');
        }
        return $this->helper->responseJson(0, 'رقم الهاتف غير صحيح');

    }

    //this login method can use in login and register in same time
    public function login(Request $request)
    {
        $rules =
            [
                'phone' => 'required|regex:/(05)[0-9]{8}/',
            ];


        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $user = $this->model->where('phone', $request->phone)->first();

        //check if user exists
        if (!$user) {
            //where user not exists we can create one
            $this->model->create(['phone' => $request->phone]);
        }

        // send pin code to confirm phone
        return $this->sendPinCode($request);
    }

    public function corporateLogin(Request $request)
    {
        $rules =
        [
            'username'   => 'required',
            'password'   => 'required',
            'token'    => 'required',
            'serial_number'    => 'required',
            'os'       => 'required|in:android,ios',
        ];


        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $user = $this->model->where('username', $request->username)->where('type', 'special')->first();

        //check if user exists
        if ($user) {
            if (Hash::check($request->password, $user->password)) {

                //create passport token
                $token = $user->createToken('android')->accessToken;

                $user->tokens()->create(['token' => $request->token, 'phone_type' => $request->os]);

                return $this->helper->responseJson(1, 'success', ['token' => $token, 'user' => new \App\Http\Resources\Client($user)]);

            } else {
                return $this->helper->responseJson(0, ' كلمة المرور غير صحيحة');
            }
        }

        return $this->helper->responseJson(0, 'إسم المستخدم أو كلمة المرور خطأ');
    }

    public function acceptCondition(Request $request)
    {

        $client = $request->user('api_client');

        //check if user exists
        if ($client->accept == 0) {

            //where user not exists we can create one
            $client->accept = 1;
            $client->save();
            $client->refresh();

            return $this->helper->responseJson(1, 'SUCCESS', ['token' => $request->bearerToken(), 'user' => new \App\Http\Resources\Client($client)]);
        } else {
            return $this->helper->responseJson(1, 'success', ['token' => $request->bearerToken(), 'user' => new \App\Http\Resources\Client($client)]);
        }

    }

    public function updateProfile(Request $request)
    {

        $client = $request->user('api_client');

        $rules =
            [
                'region_id'  => 'nullable|exists:regions,id',
                'blood_type' => 'nullable|in:A+,A-,B+,B-,O+,O-,AB+,AB-',
                'gender' => 'nullable|in:male,female',
                'photo' => 'nullable|image|mimes:jpeg,jpg,png,gif,svg',
            ];

        $validator = validator()->make($request->all(), $rules);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first());
        }

        //update user data if not try to update phone
        $client->update([
            'region_id' => $request->input('region_id') ? $request->region_id : $client->region_id,
            'blood_type' => $request->input('blood_type') ? $request->blood_type : $client->blood_type,
            'gender' => $request->input('gender') ? $request->gender : $client->gender,
        ]);

        if ($request->hasFile('photo')) {

            Photo::updatePhoto($request->file('photo'), $client->photo , $client, 'clients');
        }

        $client->refresh();

        return $this->helper->responseJson(1, 'تم التحديث بنجاح', ['token' => $request->bearerToken(), 'user' => new \App\Http\Resources\Client($client)]);

    }

    public function clientSendPinCode(Request $request)
    {
        $rules = ['phone' => 'required'];

        $sms =
            [
                'phone.required' => 'الرقم مطلوب',
            ];

        $data = validator()->make($request->all(), $rules, $sms);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $record = Client::where(['phone' => $request->phone ,'type' => 'special'])->first();

        if ($record) {
            return $this->sendPinCode($request, $scape_rules = true, $sendTo = 'phone' , $record);
        }

        return $this->helper->responseJson(0, 'اسم المستخدم غير صحيح');
    }

    public function resetPasswordOutAuth(Request $request)
    {
        $rules =
            [
                'phone' => 'required',
                'pin_code' => 'required|numeric',
                'password' => 'required|confirmed|min:6',
            ];


        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $record = $this->model->where(['phone' => $request->phone ,'type' => 'special'])->first();

        if ($record) {

            if ($record->pin_code == $request->pin_code) {

                //check pin code date time expired
                if ($this->checkPinCodeExpiredDate($record->pin_code_date_expired)) {

                    $record->pin_code = $this->getPinCode();
                    $record->password = Hash::make($request->password);
                    $record->save();

                    return $this->helper->responseJson(1, 'تم تغيير كلمة المرور بنجاح');
                }

                return $this->helper->responseJson(0, 'انتهت صلاحية كود التفعيل , حاول مرة اخري');
            }

            return $this->helper->responseJson(0, 'كود التاكيد خطأ');
        }

        return $this->helper->responseJson(0, 'رقم الهاتف غير صحيح');
    }

}
