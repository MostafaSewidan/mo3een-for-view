<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReviewsTable extends Migration {

	public function up()
	{
		Schema::create('reviews', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('rate')->default('0');
			$table->text('comment');
			$table->string('reviewable_type');
			$table->integer('reviewable_id');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('reviews');
	}
}