<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClientsTable extends Migration
{

    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('region_id')->nullable();
            $table->string('name')->nullable();
            $table->string('phone');
            $table->string('pin_code')->nullable();
            $table->datetime('pin_code_date_expired')->nullable();
            $table->enum('status', array(''));
            $table->string('national_ID')->nullable();
            $table->text('chronic_diseases')->nullable();
            $table->float('Length')->nullable();
            $table->float('the_weight')->nullable();
            $table->enum('blood_type', array('A+', 'A-', 'B+', 'B-', 'O+', 'O-', 'AB+', 'AB-'))->nullable();
            $table->string('insurance_number')->nullable();
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('clients');
    }
}
