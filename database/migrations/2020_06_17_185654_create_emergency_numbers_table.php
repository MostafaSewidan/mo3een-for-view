<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmergencyNumbersTable extends Migration {

	public function up()
	{
		Schema::create('emergency_numbers', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->string('number');
			$table->tinyInteger('is_active')->default('1');
			$table->integer('order')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('emergency_numbers');
	}
}