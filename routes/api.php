<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api\Client', 'prefix' => 'v1'], function () {
////////////////////////////////
// confirm Auth

    Route::post('login', 'AuthController@login');
    Route::post('corporate-login', 'AuthController@corporateLogin');
    Route::post('send-pin-code', 'AuthController@clientSendPinCode');
    Route::post('reset-password', 'AuthController@resetPassword');
    Route::post('confirm-account', 'AuthController@confirmAccount');
    Route::post('resend-pin-code', 'AuthController@sendPinCode');

    Route::group(['middleware' => ['auth:api_client', 'client-check']], function () {

        ////////////////////////////////
        //  Auth transactions
        Route::post('accept-condition', 'AuthController@acceptCondition');
        Route::get('client-profile', 'AuthController@showProfile');
        Route::get('client-wallet-balance', 'AuthController@showWalletBalance');
        Route::post('update-profile', 'AuthController@updateProfile');
        Route::post('logout', 'AuthController@logOut');
        //Route::post('make-service', 'AuthController@makeService');
        ////////////////////////////////

        ////////////////////////////////
        //  client transactions

        Route::post('add-review', 'TransactionController@addReviews');
        Route::post('add-address', 'TransactionController@addAddress');
        Route::post('remove-address', 'TransactionController@removeAddress');
        Route::get('show-addresses', 'TransactionController@showAddresses');

        ////////////////////////////////

        ////////////////////////////////
        ///
        // notifications

        Route::get('notifications', 'TransactionController@notifications');
        Route::post('delete-notification', 'TransactionController@deleteNotification');
        Route::post('read-notification', 'TransactionController@readNotification');

        ////////////////////////////////
        ///
        // Reservations

        Route::post('make-reservation', 'HomeController@makeReservation');
        ////////////////////

        // Write order
        Route::get('my-tours', 'HomeController@myTours');
        ///////////////////////////////

        ////////////////////////////////

        // order transactions

        Route::post('nearest-facility', 'OrderController@nearestFacilityResponse');
        Route::post('calculate-distance-price', 'OrderController@CalculateDistancePriceResponse');
        Route::post('make-order', 'OrderController@parentOrder');
        Route::post('make-transferring-body', 'OrderController@makeTransferingBody');
        Route::get('orders', 'OrderController@showOrders');

        ////////////////////////////////

        // wallet transactions

        Route::post('recharge-wallet', 'TransactionController@changePoints');

        ////////////////////////////////


    });
////////////////////////////////


////////////////////////////////
// general apis
    Route::get('countries', 'HomeController@countries');
    Route::get('profile-selects', 'HomeController@profileSelects');
    Route::get('car-types', 'HomeController@carTypes');
    Route::get('settings', 'HomeController@settings');
    Route::get('blood-types', 'HomeController@bloodTypes');
    Route::get('cities', 'HomeController@cities');
    Route::get('regions', 'HomeController@regions');
    Route::get('posts', 'HomeController@posts');
    Route::get('emergency-numbers', 'HomeController@emergencyNumbers');
    Route::get('home', 'HomeController@home');
    Route::get('hospitals', 'HomeController@hospitals');
    Route::get('payment-methods', 'HomeController@paymentMethods');
    Route::get('pharmacies', 'HomeController@pharmacies');
    Route::get('deliveries', 'HomeController@deliveries');
    Route::get('reviews', 'HomeController@reviews');
    Route::get('offers', 'HomeController@offers');
    Route::get('contact-reasons', 'HomeController@contactReasons');
    Route::post('contact-us', 'HomeController@contactUs');
    Route::post('add-delivery', 'HomeController@addDelivery');
    Route::get('charitable-actors', 'HomeController@charitableActors');

////////////////////////////////
});


Route::group(['namespace' => 'Api\Provider', 'prefix' => 'v1/provider'], function () {


    Route::get('test-order-notification', 'OrderController@testSendNotification');
    ////////////////////////////////
    // Auth

    Route::post('login', 'AuthController@login');
    Route::post('send-pin-code', 'AuthController@ProviderSendPinCode');
    Route::post('reset-password', 'AuthController@resetPassword');
    Route::get('reject-order-reasons', 'GeneralTransactionController@rejectOrderReasons');
    ////////////////////////////////
    ///

    Route::post('contact-us', 'GeneralTransactionController@contactUs');

    Route::group(['middleware' => ['auth:api_delivery', 'delivery-check']], function () {
        //  Auth transactions

        Route::post('accept-privacy', 'AuthController@acceptPrivacy');
        Route::get('profile', 'AuthController@showProfile');
        Route::post('update-profile', 'AuthController@updateProfile');
        Route::post('logout', 'AuthController@logOut');
        Route::post('update-status', 'AuthController@updateStatus');
        Route::post('update-location', 'GeneralTransactionController@updateLocation');

        ////////////////////////////////
        ///
        // notifications

        Route::get('notifications', 'GeneralTransactionController@notifications');
        Route::post('delete-notification', 'GeneralTransactionController@deleteNotification');
        Route::post('read-notification', 'GeneralTransactionController@readNotification');

        ////////////////////////////////

        // wallet transactions

        Route::post('recharge-wallet', 'GeneralTransactionController@changePoints');
        Route::get('points-report', 'GeneralTransactionController@pointsReport');

        ////////////////////////////////

        // order transactions

        Route::get('notification-order', 'GeneralTransactionController@listOrder');
        Route::get('orders', 'OrderController@listOrders');
        Route::post('accept-order', 'OrderController@acceptOrder');
        Route::post('reject-order', 'OrderController@rejectOrder');
        Route::post('cancel-order', 'OrderController@cancelOrder');
        Route::post('arrives-to-patient', 'OrderController@arrivesToPatient');
        Route::post('move-with-patient', 'OrderController@moveWithPatient');
        Route::post('arrived-to-hospital', 'OrderController@arrivedToHospital');
        Route::post('end-journey', 'OrderController@EndJourney');
        Route::get('last-notification-order', 'GeneralTransactionController@lastOrderWithNotification');

        //pharmacy order (written order)

        Route::post('arrived-to-pharmacy', 'OrderController@arrivesToPharmacy');
        Route::post('delivery-loaded', 'OrderController@deliveryLoaded');
        Route::post('delivery-go', 'OrderController@deliveryGo');
        Route::post('arrived-to-client', 'OrderController@arrivedToClient');

        ////////////////////////////////

        Route::get('reviews', 'GeneralTransactionController@reviews');
    });
});
























