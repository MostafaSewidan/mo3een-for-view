<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Api\ParentApi;
use App\MyHelper\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentMethodController extends ParentApi
{

    public function __construct()
    {

        $this->helper = new Helper();
    }

    public function wallet($user , $order)
    {
        $wallet = $user->walletTransactions()->latest()->first();

        if($wallet && $wallet->balance_after >= $order->total_price)
        {
            $user->walletTransactions()->create(
                [
                    'order_id' => $order->id,
                    'balance_before' => $wallet->balance_after,
                    'amount' => - $order->total_price,
                    'balance_after' => $wallet->balance_after - $order->total_price,
                    'action' => 'order',
                ]
            );
            return 'true';
        }

        return 'false';
    }

    public function creditCard($user , $order)
    {
        //TODO خصم من visa العميل
    }
}
