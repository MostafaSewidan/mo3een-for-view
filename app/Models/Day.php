<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Day extends Model
{

    protected $table = 'days';
    public $timestamps = true;
    protected $fillable = array('day', 'daieable_type', 'daieable_id');

    public function dates()
    {
        return $this->hasMany(Date::class);
    }

    public function daieable()
    {
        return $this->morphTo();
    }

}