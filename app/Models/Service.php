<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{

    protected $table      = 'services';
    public    $timestamps = true;
    protected $fillable   = array('name', 'type');
    protected $appends   = array('facility_title');

    public function pharmacies()
    {
        return $this->hasMany('App\Models\Pharmacy');
    }


    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function attachment()
    {
        return $this->morphOne('App\Models\Attachment', 'attachmentable');
    }

    public function hospitals()
    {
        return $this->hasMany('App\Models\Hospital');
    }

    public function emergency_numbers()
    {
        return $this->hasMany('App\Models\EmergencyNumber');
    }

    public function deliveries()
    {
        return $this->belongsToMany('App\Models\Delivery');
    }

    public function workingAmbulances()
    {
        return $this->belongsToMany('App\Models\Delivery')
            ->whereIn('deliveries.status',['active']);
    }

    public function getFacilityTitleAttribute()
    {
        $text = '';

        switch ($this->type)
        {
            case 'pharmacy':
                $text = "طلب مكتوب من صيدلية";
                break;
            default:
                $text = $this->name;
                break;
        }
        return $text;
    }


}
