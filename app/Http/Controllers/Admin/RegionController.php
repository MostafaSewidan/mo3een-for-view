<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Region;
use App\MyHelper\Helper;
use Illuminate\Http\Request;
use Response;

class RegionController extends Controller
{
    protected $model;
    protected $viewsDomain = 'admin/regions.';

    public function __construct()
    {
        $this->model = new Region();
    }

    /**
     * @param $view
     * @param array $params
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $records = $this->model->where(function ($q) use ($request) {
            if ($request->name) {
                $q->where(function ($q) use ($request) {

                    $q->where('name', 'LIKE', '%' . $request->name . '%');
                });
            }

            if ($request->country) {
                $q->whereHas('city', function ($q) use ($request) {
                    $q->where('country_id', $request->country);
                });
            }

            if ($request->city) {
                $q->where('city_id', $request->city);
            }

            if ($request->from) {
                $q->whereDate('created_at', '>=', Helper::convertDateTime($request->from));
            }

            if ($request->to) {
                $q->whereDate('created_at', '<=', Helper::convertDateTime($request->to));
            }


        })->latest()->paginate(20);

        return $this->view('index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = $this->model;
        return $this->view('create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =
            [
                'name' => 'required',
                'city_id' => 'required',
            ];

        $error_sms =
            [
                'name.required' => 'الرجاء ادخال الاسم ',
                'city_id.required' => 'الرجاء ادخال المنطقة ',
            ];

        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return back()->withInput()->withErrors($data->errors());
        }

        $record = $this->model->create($request->all());

        session()->flash('success', 'تمت الاضافة بنجاح');
        return redirect('admin/regions');
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->model->findOrFail($id);
        return $this->view('edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =
            [
                'name' => 'required',
                'city_id' => 'required',
            ];

        $error_sms =
            [
                'name.required' => 'الرجاء ادخال الاسم ',
                'city_id.required' => 'الرجاء ادخال المنطقة ',
            ];


        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return back()->withInput()->withErrors($data->errors());
        }

        $record = $this->model->findOrFail($id);

        $record->update($request->all());

        session()->flash('success', 'تمت تحديث بنجاح');
        return redirect('admin/regions/' . $id . '/edit');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = $this->model->findOrFail($id);

        if ($record->deliveries()->count()) {
            $data = [
                'status' => 0,
                'message' => 'لا يمكن الحذف , يوجد سيارات إسعاف مرتبطة به',
                'id' => $id
            ];
            return Response::json($data, 200);
        }
        $record->delete();

        $data = [
            'status' => 1,
            'message' => 'تم الحذف بنجاح',
            'id' => $id
        ];
        return Response::json($data, 200);
    }

    public function getRegion($id)
    {

        $record = City::findOrFail($id)->regions()->get();

        if ($record->count()) {

            $data = [
                'status' => 1,
                'data' => $record,
                'id' => $id
            ];

            return Response::json($data, 200);
        } else {
            $data = [
                'status' => 0,
                'data' => $record,
                'id' => $id
            ];

            return Response::json($data, 400);
        }

    }
}
