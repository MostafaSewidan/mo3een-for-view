<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\MyHelper\Helper;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use Response;
use Hash;
use Auth;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    //
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    public function changePassword()
    {
        return view('admin.reset-password');
    }

    public function changePasswordSave(Request $request)
    {
        $this->validate($request, [
            'old-password' => 'required',
            'password'     => 'required|confirmed',
        ]);

        $user = Auth::user();

        if (Hash::check($request->input('old-password'), $user->password)) {
            // The passwords match...
            $user->password = bcrypt($request->input('password'));
            $user->save();
            session()->flash('success', 'تم تحديث كلمة المرور');
            return view('admin.reset-password');
        } else {
            session()->flash('fail', 'كلمة المرور غير صحيحة');
            return view('admin.reset-password');
        }

    }

    public function index(Request $request)
    {
        $users = User::where(function ($q) use ($request) {
            if ($request->name) {
                $q->where(function ($q) use ($request) {

                    $q->where('name', 'LIKE', '%' . $request->name . '%')
                      ->orWhere('email', 'LIKE', '%' . $request->name . '%');
                });
            }
            if ($request->role_name) {

                $q->whereHas('roles', function ($q) use ($request) {

                    $q->where('display_name', 'LIKE', '%' . $request->role_name . '%');
                });
            }

            if ($request->from) {
                $q->whereDate('created_at', '>=', Helper::convertDateTime($request->from));
            }

            if ($request->to) {
                $q->whereDate('created_at', '<=', Helper::convertDateTime($request->to));
            }


        })->latest()->paginate(20);
        return view('admin.users.index', compact('users'));
    }

    public function create(User $model)
    {
        $model = new User();
        $roles = Role::all();

        return view('admin.users.create', compact('model', 'roles'));
    }

    public function store(Request $request)
    {
        $rules =
            [
                'name'     => 'required',
                'email'    => 'required|email|unique:users',
                'password' => 'required|confirmed',
                'roles.*'  => 'required|exists:roles,id',
            ];

        $error_sms =
            [
                'name.required'      => 'الرجاء ادخال الاسم ',
                'email.unique'       => ' البريد الالكتروني موجود بالفعل',
                'email.required'     => 'الرجاء ادخال البريد الالكتروني',
                'password.required'  => 'الرجاء ادخال كلمة المرور',
                'password.confirmed' => 'الرجاء التاكد من كلمة المرور ',

            ];

        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return back()->withInput()->withErrors($data->errors());
        }
        $user = User::create(request()->all());

        $user->update(['password' => Hash::make($request->password)]);

        $user->assignRole($request->roles);
        session()->flash('success', 'تمت الاضافة بنجاح');
        return redirect('/admin/user');
    }

    public function show($id)
    {
        /* $user = User::with('addresses')->findOrFail($id);
         $orders = $user->orders()->latest()->paginate(5);
         return view('admin.sushi.user',compact('user','orders'));*/
    }

    public function edit($id)
    {
        $model = User::findOrFail($id);
        $roles = Role::all();

        return view('admin.users.edit', compact('model', 'roles'));
    }

    public function update(Request $request, $id)
    {
        $record = User::findOrFail($id);

        $rules =
            [
                'name'     => 'required',
                'email'    => 'required|email|unique:users,email,' . $record->id . '',
                'password' => 'required|confirmed',
                'roles.*'  => 'required|exists:roles,id',
            ];

        $error_sms =
            [
                'name.required'      => 'الرجاء ادخال الاسم ',
                'email.required'     => 'الرجاء ادخال البريد الالكتروني',
                'email.unique'       => ' البريد الالكتروني موجود بالفعل',
                'password.required'  => 'الرجاء ادخال كلمة المرور ',
                'password.confirmed' => 'الرجاء التاكد من كلمة المرور ',

            ];
        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return redirect('/admin/user/' . $id . '/edit')->withInput()->withErrors($data->errors());
        }


        $record->update($request->except('password'));

        if ($request->has('password')) {
            $record->update(['password' => Hash::make($request->password)]);
        }

        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $record->assignRole($request->roles);

        session()->flash('success', 'تم التعديل بنجاح');
        return back();

    }

    public function destroy($id)
    {
        $record = User::findOrFail($id);

        if (auth('web')->user()->id == $record->id) {
            session()->flash('fail', 'This email, you cannot deactivate it');
            return redirect('admin/users');
        }

        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $record->delete();
        $data = [
            'status' => 1,
            'msg'    => 'Deleted successfully',
            'id'     => $id
        ];

        return Response::json($data, 200);
    }

    public function activation($id)
    {
        $record = User::findOrFail($id);

        if (auth('web')->user()->id == $record->id) {
            session()->flash('fail', 'This email, you cannot deactivate it');
            return redirect('admin/user');
        }
        $activate = Helper::activation($record);

        if ($activate) {
            session()->flash('success', 'success');
            return redirect('admin/user');
        }


        session()->flash('fail', 'Something went wrong please try again');
        return redirect('admin/user');
    }

    public function home()
    {
        return view('admin.layouts.home');
    }
}
