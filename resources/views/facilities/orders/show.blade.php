@extends('facilities.layouts.main',[
                                'page_header'       => 'عرض الرحلات ',
                                'page_description'  => 'طلب رقم #'.$order->id,
                                'link' => url('facilities/orders')
                                ])
@section('content')


    <div class="box box-primary">
        <div class="box-body">

            @include('flash::message')
            @if(!empty($order))
                <section class="invoice">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                                <i class="fa fa-globe"></i> تفاصيل طلب # {{$order->id}}
                                <small class="pull-left"><i class="fa fa-calendar-o"></i> {{$order->created_at}}
                                </small>
                            </h2>
                        </div><!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
                            <b>تفاصيل الطلب</b>
                            <address>
                                <i class="fa fa-angle-left" aria-hidden="true"></i><b> رقم الطلب </b>#{{$order->id}}<br>
                                <i class="fa fa-angle-left" aria-hidden="true"></i><b> نوع
                                    الرحلة </b>{{optional($order->service)->facility_title}}<br>
                                <i class="fa fa-angle-left" aria-hidden="true"></i><b> حالةالطلب:</b>
                                <span class="label xs label-primary">{{$order->client_status_text}}</span>
                                <br>
                                <br>

                                <i class="fa fa-angle-left" aria-hidden="true"></i><b> تاريخ :
                                    الطلب </b>{{$order->asked_at}}<br>

                                @if($order->status == 'move_with_patient')
                                    <i class="fa fa-angle-left" aria-hidden="true"></i><b> تاريخ :
                                        الإنطلاق </b>{{$order->start_trip}}<br>

                                @endif

                                @if($order->status == 'End_journey')
                                    <i class="fa fa-angle-left" aria-hidden="true"></i><b> تاريخ
                                        الوصول : </b>{{$order->end_trip}}<br><br>
                                @endif

                                <i class="fa fa-angle-left" aria-hidden="true"></i><b> الوقت المتوقع
                                    للوصول : </b>{{$order->arrival_Expected_time}}<br>

                                @if($order->status == 'End_journey')

                                    <i class="fa fa-angle-left" aria-hidden="true"></i><b> وقت الوصول
                                        الفعلي
                                        : </b>{{gmdate('H:i:s', $order->end_trip->diffInSeconds($order->start_trip))}}
                                    <br><br>

                                @endif

                                <i class="fa fa-angle-left" aria-hidden="true"></i><b> يتراوح
                                    السعر : </b>{{$order->start_price_range}}<b> من </b>{{$order->end_price_range}}<br>

                                <i class="fa fa-angle-left" aria-hidden="true"></i><b> تكلفة الرحلة
                                    الفعلية : </b>{{$order->real_price}}<br>

                                <br>

                                <i class="fa fa-angle-left" aria-hidden="true"></i><b> عنوان الإنطلاق :</b>
                                <a href="{{url('http://maps.google.com/?q='.optional($order->startAddress)->latitude.','.optional($order->startAddress)->longitude)}}"
                                   target="_blank">
                                    <i class="fa fa-map-marker"></i>

                                    {{optional($order->startAddress)->title}}
                                </a>
                                <br>
                                <i class="fa fa-angle-left" aria-hidden="true"></i><b> عنوان الوصول :</b>
                                <a href="{{url('http://maps.google.com/?q='.optional($order->endAddress)->latitude.','.optional($order->endAddress)->longitude)}}"
                                   target="_blank">
                                    <i class="fa fa-map-marker"></i>

                                    {{optional($order->endAddress)->title}}
                                </a>
                                <br>

                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                            <b>العميل</b>
                            <address>

                                <i class="fa fa-angle-left"
                                   aria-hidden="true"></i><b>الاسم</b>: @if($order->client){{$order->client->name}}@endif
                                <br>
                                <i class="fa fa-angle-left"
                                   aria-hidden="true"></i><b>الهاتف</b>: @if($order->client){{$order->client->phone}}@endif
                                <br>

                            </address>
                        </div><!-- /.col -->
                        @if($order->service_id != 7)
                            <div class="col-sm-3 invoice-col">
                                <b>بيانات الحالة</b>
                                <address>

                                    @if($order->service_id != 7)
                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>الاسم</b>: {{optional($order->patient)->name}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>رقم الهوية</b>: {{optional($order->patient)->national_id}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>رقم الهاتف</b>: {{optional($order)->phone}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>الأمراض المزمنة</b>: {{optional($order->patient)->diseases}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>الحالة الإسعافية</b>: {{optional($order->patient)->clinic_status}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>الطول</b>: {{optional($order->patient)->length}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>فصيلة الدم</b>: {{optional(optional($order->patient)->bloodType)->name}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>الدور</b>: {{optional($order->patient)->floor}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>المصعد</b>: {{optional($order->patient)->elevator}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>عدد الحالات</b>: {{optional($order->patient)->cases_num}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>رقم التأمين</b>: {{optional($order->patient)->insurance_number}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>النوع</b>: {{optional($order->patient)->type == 'injured' ? 'مصاب' : 'مبلغ'}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>ملاحظة</b>: {{optional($order->patient)->note}}
                                        <br>

                                    @elseif($order->service_id == 7)

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>إسم المبلغ بالكامل</b>: {{optional($order->patient)->name}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>رقم هوية المبلغ</b>: {{optional($order->patient)->national_id}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>رقم الهاتف المحمول</b>: {{optional($order)->phone}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>صلة القرابة</b>: {{optional($order->patient)->relative_relation}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>اسم المتوفي</b>: {{optional($order->patient)->dead_name}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>رقم هوية المتوفي</b>: {{optional($order->patient)->dead_national_id}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>تاريخ الوفاة</b>: {{optional($order->patient)->death_date}}
                                        <br>

                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>رقم تصريح الدفن</b>: {{optional($order->patient)->burial_permit_number}}
                                        <br>


                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>ملاحظة</b>: {{optional($order->patient)->note}}
                                        <br>
                                    @endif
                                </address>
                            </div><!-- /.col -->
                        @else

                            <div class="col-sm-3 invoice-col">
                                <b>بيانات الطلب</b>
                                <address>
                                    <i class="fa fa-angle-left" aria-hidden="true"></i>
                                    <b>الوصف</b>: {{optional($order)->description}}
                                    <br>
                                    @if($order->photo)
                                        <i class="fa fa-angle-left" aria-hidden="true"></i>
                                        <b>الصورة</b>:
                                        <br>
                                        <br>
                                        <a href="{{asset(optional($order->photo)->path)}}" data-lity>
                                            <img src="{{asset(optional($order->photo)->path)}}" alt="image"
                                                 width="100" height="100">
                                        </a>
                                        <br>
                                    @endif
                                </address>
                            </div><!-- /.col -->

                        @endif
                        <div class="col-sm-3 invoice-col">
                            <b>السائق</b>
                            <address>
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                <b>الاسم</b>:
                                <a href="{{url(route('ambulances.show',optional($order->delivery)->id))}}">
                                    {{optional($order->delivery)->name}}</a>

                                <br>
                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                <b>التليفون</b>:
                                {{optional($order->delivery)->phone}}
                                <br>
                                {{--<i class="fa fa-angle-left" aria-hidden="true"></i>--}}
                                {{--<span>الهاتف</span>: @if($order->client){{$order->client->phone}}@endif<br>--}}
                            </address>
                        </div>
                    </div><!-- /.row -->
                    <!-- Table row -->
                    <!-- this row will not appear when printing -->
                    <div class="row no-print">
                        <div class="col-xs-12">
                            <a href="" class="btn btn-success pull-left" id="print-all">
                                <i class="fa fa-print"></i> طباعة </a>

                            @if(in_array($order->status,['pending','accepted']))

                                @if($order->status == 'pending')

                                    <button class="btn btn-primary" data-toggle="modal" data-target="#choose-delivery">
                                        <i class="fa fa-check"></i> قبول الطلب
                                    </button>

                                @elseif($order->status == 'accepted')

                                    <button class="btn btn-warning" data-toggle="modal" data-target="#choose-delivery">
                                        <i class="fa fa-bicycle"></i> إختيار الإسعاف
                                    </button>

                                @endif


                            <!-- Modal -->
                                <div class="modal fade" id="choose-delivery" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">اختيار الإسعاف</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            {!! Form::open([
                                                'url' => url('facilities/select-delivery-orders/' . $order->id),
                                                'method' => 'post'
                                            ]) !!}
                                            <div class="modal-body">
                                                <div class="row">
                                                    <div class="col-xs-10">
                                                        <div class="form-group">
                                                            {!! Form::text('delivery_id',$order->delivery_id,[
                                                                'class' => 'form-control',
                                                                'id' => 'delivery'
                                                            ]) !!}
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-2">
                                                        <div class="form-group">
                                                            <button type="button" class="btn btn-info btn-block"
                                                                    id="show-delivery">
                                                                <i class="fa fa-search"></i> عرض
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="delivery-data">
                                                    @if(!$order->delivery)

                                                        <div class="alert alert-danger">
                                                            <p>تعذر الحصول على البيانات</p>
                                                        </div>

                                                    @else

                                                        <table class="table">

                                                            <tr>
                                                                <th>الحالة</th>
                                                                <td>{!! $order->delivery->case_label !!}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>الاسم</th>
                                                                <td>{{$order->delivery->name}}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>التليفون</th>
                                                                <td>{{$order->delivery->phone}}</td>
                                                            </tr>
                                                            <tr>
                                                                <th>المنطقة</th>
                                                                <td>
                                                                    @if($order->delivery->region)
                                                                        {{$order->delivery->region->name}}
                                                                        @if($order->delivery->region->city)
                                                                            - {{$order->delivery->region->city->name}}
                                                                            @if($order->delivery->region->city->country)
                                                                                - {{$order->delivery->region->city->country->name}}
                                                                            @endif
                                                                        @endif
                                                                    @endif
                                                                </td>
                                                            </tr>
                                                            @if($order->delivery->latitude && $order->delivery->longitude)
                                                                <tr>
                                                                    <th>اللوكيشن</th>
                                                                    <td>
                                                                        <a href="{{url('http://maps.google.com/?q='.$order->delivery->latitude.','.$order->delivery->longitude)}}"
                                                                           target="_blank" class="btn btn-danger">
                                                                            <i class="fa fa-map-marker"></i>
                                                                        </a>
                                                                    </td>
                                                                </tr>
                                                            @endif
                                                        </table>

                                                    @endif
                                                </div>
                                                @push('scripts')
                                                    <script>
                                                        var showDeliveryBtn = $("#show-delivery");
                                                        var deliveryInput = $("#delivery");
                                                        var deliveryData = $("#delivery-data");
                                                        showDeliveryBtn.click(function (e) {
                                                            var deliveryId = deliveryInput.val();
                                                            if (!deliveryId) {
                                                                swal({
                                                                    title: "خطأ!",
                                                                    text: 'اكتب كود الإسعاف',
                                                                    type: "error",
                                                                    showConfirmButton: true,
                                                                    timer: 1500
                                                                });
                                                            }
                                                            $.ajax({
                                                                url: "{{url('facilities/get-ambulance?ambulance_id=')}}" + deliveryId,
                                                                type: 'get',
                                                                dataType: 'json',
                                                                success: function (data) {
                                                                    console.log(data.view);
                                                                    deliveryData.html(data.view);
                                                                }
                                                            });
                                                        });
                                                    </script>
                                                @endpush
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary">تأكيد</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    إغلاق
                                                </button>
                                            </div>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            @endif

                            @if(in_array($order->status,['arrives_to_patient','accepted']))


                                <button class="btn btn-danger" data-toggle="modal" data-target="#cancel-order">
                                    <i class="fa fa-times-circle-o"></i> إلغاء الطلب
                                </button>

                                <!-- Modal -->
                                <div class="modal fade" id="cancel-order" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">إلغاء
                                                    طلب {{ $order->id }}#</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">

                                                {!! Form::open([
                                                            'url' => url('facilities/cancel-orders-request'),
                                                            'method' => 'post',
                                                            'id' => 'cancelForm',
                                                        ]) !!}
                                                @inject('reasons' , App\Models\RejectOrderReason)

                                                {!! Form::hidden('order_id',$order->id) !!}
                                                {!! \App\MyHelper\Field::select('reject_order_reason_id', 'سبب الرفض',$reasons->where('is_active',1)->pluck('name','id')->toArray()) !!}

                                                {!! \App\MyHelper\Field::textarea('description','تفاصيل سبب الرفض') !!}

                                            </div>

                                            <div class="modal-footer">
                                                <button type="submit" id="submit-button" class="btn btn-primary">تأكيد</button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    إغلاق
                                                </button>
                                            </div>
                                        </div>

                                        {!! Form::close() !!}

                                        @push('scripts')
                                            <script>
                                                $('#cancelForm').submit(function (e) {
                                                    e.preventDefault();
                                                    var url = $('#cancelForm').attr('action');
                                                    var form_data = new FormData(this);
                                                    $(".help-block").hide();
                                                    $(".has-error").removeClass('has-error');
                                                    // $('#submit-button').attr('disabled', 'disabled');
                                                    $.ajax({
                                                        url: url,
                                                        type: 'post',
                                                        data: form_data,
                                                        cache: false,
                                                        contentType: false,
                                                        processData: false,
                                                        success: function (data) {
                                                            var url = data.url;
                                                            if (url) {
                                                                window.location = url;
                                                            }
                                                        },
                                                        error: function (data) {
                                                            var error = data.responseJSON.errors;

                                                            $('#submit-button').removeAttr('disabled');
                                                            $.each(error, function (key, value) {
                                                                $('#' + key + '_error').text('').append(value);
                                                                $('#' + key + '_error').parent().show();
                                                                $('#' + key + '_error').parent().parent().addClass('has-error');
                                                            });
                                                        }
                                                    });
                                                });
                                            </script>
                                        @endpush
                                    </div>
                                </div>
                        </div>
                        @endif
                        <script>
                            //                                $('#myModal').on('shown.bs.modal', function () {
                            //                                    $('#myInput').focus()
                            //                                })
                        </script>
                    </div>

        </section><!-- /.content -->
        @push('scripts')
            <script>
                $("#print-all").click(function () {
                    window.print();
                });
            </script>
        @endpush
        <div class="clearfix"></div>
        @endif

    </div>
    </div>

@endsection
