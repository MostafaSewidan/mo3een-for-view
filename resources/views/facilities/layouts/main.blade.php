@include('facilities.layouts.head')

<div id="wrapper">

    @include('facilities.layouts.sidebar')

    <div id="page-wrapper" class="gray-bg" style="background-image: url('{{asset('photos/more_background.png')}}')">
        @include('facilities.layouts.header')
        <section class="content-header">
            <h1>
                <a href="{{$link}}">

                    {{$page_header}}
                </a>
                <small>{!! $page_description !!}</small>
            </h1>
        </section>
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    @yield('content')
                </div>
            </div>
        </div>

        @include('facilities.layouts.footer')
    </div>

</div>
@include('facilities.layouts.foot')