<?php

namespace App\Http\Controllers\Facility;

use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationOrder;
use App\Models\City;
use App\Models\Delivery;
use App\Models\Governorate;
use App\Models\Order;
use App\Models\Review;
use App\MyHelper\Helper;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected $model;
    protected $helper;
    protected $guard = 'facilities';
    protected $url = 'facilities/orders';
    protected $viewsDomain = 'facilities.orders.';

    public function __construct()
    {
        $this->helper = new Helper;
        $this->guard = 'facilities';
    }

    /**
     * @param $view
     * @param array $params
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $facility = auth($this->guard)->user()->hospital;

        $records = Order::where('service_id', '!=', 3)->whereHas('delivery', function ($q) use ($facility) {
            $q->where('hospital_id', $facility->id);
        })->where(function ($q) use ($request) {

            if ($request->ambulance_id) {
                $q->where('delivery_id', $request->ambulance_id);
            }

            if ($request->status) {
                $q->where('status', $request->status);
            }

            if ($request->service) {
                $q->where('service_id', $request->service);
            }

            if ($request->order_id) {
                $q->where('id', $request->order_id);
            }


            if ($request->from) {
                $q->whereDate('created_at', '>=', Helper::convertDateTime($request->from));
            }

            if ($request->to) {
                $q->whereDate('created_at', '<=', Helper::convertDateTime($request->to));
            }

        })->orderBy('asked_at')->paginate(15);

        return $this->view('index', compact('records'));
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $facility = auth($this->guard)->user()->hospital;
        $order = Order::where('service_id', '!=', 3)->whereHas('delivery', function ($q) use ($facility) {
            $q->where('hospital_id', $facility->id);
        })->findOrFail($id);

        return $this->view('show', compact('order'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $record = $this->model->find($id);
        //dd($record);
        if (!$record) {
            return response()->json([
                'status' => 0,
                'message' => 'تعذر الحصول على البيانات'
            ]);
        }

        if ($record->cities()->count()) {
            return response()->json([
                'status' => 0,
                'message' => 'يوجد مدن مرتبطة بهذه المحافظة',
            ]);
        }
        $record->delete();
        return response()->json([
            'status' => 1,
            'message' => 'تم الحذف بنجاح',
            'id' => $id
        ]);
    }


    public function selectDelivery($id, Request $request)
    {
        $facility = auth($this->guard)->user()->hospital;
        $user = $facility->ambulances()->where('types', 'open')->find($request->delivery_id);

        $order = Order::where('service_id', '!=', 3)->whereHas('delivery' , function ($q) use ($facility) {
            $q->where('hospital_id', $facility->id);
        })->find($id);

        if(!in_array($order->status, ['accepted']))
        {
            return flash()->error($order->client_status_text.' لا يمكن تغيير الإسعاف في حالة ');
        }

        $oldUser = $order->delivery;

        if ($order) {

            $order->update(['status' => 'accepted', 'delivery_id' => $user->id]);

            // send notifications to store and client
            $this->helper->sendNotification($order, [$user->id], 'deliveries', 'لديك رحلة جديدة', '#'.$order->id .' قامت منشأتك بختيارك للقيام برحلة رقم ', 'order', new NotificationOrder($order));

            if($oldUser)
            {
                $this->helper->sendNotification($order, [$oldUser->id], 'deliveries', 'تم تغير سائق الرحلة الخاصة بك ', ' لسائق اخر '. '#'. $order->id .' قامت منشأتك بتغيير رحلتك برحلة رقم ', 'order', new NotificationOrder($order));
            }

            flash()->success('تم اختيار الدليفري');
            return back();
        }
        flash()->error('حدث خطأ أثناء اختيار الدليفري');
        return back();
    }


    public function requestCancel( Request $request)
    {
        $rules =
            [
                'order_id' => 'required|exists:orders,id',
                'description' => 'required',
                'reject_order_reason_id' => 'required|exists:reject_order_reasons,id',
            ];

        $messages = [
            'reject_order_reason_id.required' => 'سبب الإلغاء غير محدد',
            'reject_order_reason_id.exists' => 'سبب الإلغاء غير محدد',
            'description.required' => 'تفاصيل سبب الإلغاء مطلوبة',
        ];


        $validator = validator()->make($request->all(), $rules, $messages);


        if ($validator->fails()) {
            return response()->json([
                'status' => 0,
                'message' => 'يوجد مدن مرتبطة بهذه المحافظة',
                'errors' => $validator->errors()
            ],400);
        }

        $facility = auth($this->guard)->user()->hospital;

        $order = Order::where('service_id', '!=', 3)->whereHas('delivery' , function ($q) use ($facility) {
            $q->where('hospital_id', $facility->id);
        })->find($request->order_id);

        if(!in_array($order->status, ['accepted', 'arrives_to_patient']))
        {
            return flash()->error($order->client_status_text.' لا يمكن إلغاء الرحلة في حالة ');
        }

        $user = $order->delivery;

        if ($order) {

            $facility->orderRejectReasons()->create(
               array_merge($request->all() , [
                   'delivery_id' => $user->id,
                   'type' => 'cancel',
               ])
            );

            //TODO notifi admin

            flash()->success('تم التقدم بطلب الإلغاء بنجاح , الرجاء انتظار موافقة الإدارة');
            return response()->json([
                'status' => 1,
                'message' => '',
                'url' => url($this->url)
            ]);
        }

        flash()->error('حدث خطأ حاول في وقت لاحق');
        return response()->json([
            'status' => 1,
            'message' => '',
            'url' => url($this->url)
        ]);
    }
}
