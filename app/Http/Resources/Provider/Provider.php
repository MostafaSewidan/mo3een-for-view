<?php

namespace App\Http\Resources\Provider;

use App\Models\Setting;
use App\MyHelper\Helper;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Provider extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id' => $this->id,
            'name' => $this->name,
            'user_name' => $this->user_name,
            'phone' => $this->phone,
            'email' => $this->email,
            'nationality' => $this->nationality,
            'd_o_b' => $this->d_o_b,
            'gender' => $this->gender,
            'status' => $this->types,
            'rate' => number_format($this->rate ,1),
            'transport_photo' => $this->transport_photo != null ? asset($this->transport_photo) : null,
            'photo' => $this->photo != null ? asset($this->photo) : null,
            'points' => (int)$this->points,
            'points_in_pounds' => $this->points * (new Helper())->settingValue('provider_points_cost' , 1),
            'last_date_to_change_points' => Carbon::now()->addMonth()->startOfMonth()->format('d/m/Y'),
            'wallet_balance' => (int)$this->walletBalance,
            'accept_privacy' => $this->accept_privacy,
            'orders' => [
                'canceled_count' => (string) $this->orders()->where('status', 'cancel')->count(),
                'rejected_count' => (string) $this->deliveryRejectReasons()->accepted()->type('reject')->count(),
                'success_count' => (string) $this->orders()->where('status', 'End_journey')->count(),
            ],
            'last_order' => null,
            'region' => $this->region,
            'points_to_egp' => (new Helper())->settingValue('provider_points_cost' , 1),
        ];
    }
}
