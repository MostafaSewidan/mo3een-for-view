<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWalletTransactionsTable extends Migration {

	public function up()
	{
		Schema::create('wallet_transactions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('client_id');
			$table->integer('order_id')->nullable();
			$table->float('balance_before');
			$table->float('amount');
			$table->float('balance_after');
			$table->enum('action', array('visa', 'order', 'points'));
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('wallet_transactions');
	}
}