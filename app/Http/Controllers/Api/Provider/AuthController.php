<?php

namespace App\Http\Controllers\Api\Provider;

use App\Http\Controllers\Api\ParentApi;
use App\Http\Resources\Provider\Provider;
use App\Models\Delivery;
use App\Models\Store;
use App\Models\Token;
use App\MyHelper\Helper;
use App\MyHelper\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends ParentApi
{
    public function __construct()
    {
        $this->helper = new Helper();
        $this->guard = 'api_delivery';
        $this->model = new Delivery();
        $this->table = 'deliveries';
        $this->apiResource = '\App\Http\Resources\Provider\Provider';
        $this->uniqueRow = 'user_name';
    }

    //this login method can use in login and register in same time
    public function login(Request $request)
    {
        $rules =
            [
                'user_name' => 'required',
                'password' => 'required|min:6',
                'token' => 'required',
                'serial_number' => 'required',
                'os' => 'required|in:android,ios',
            ];


        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $provider = Delivery::where('user_name', $request->user_name)->first();

        if ($provider) {
            if (Hash::check($request->password, $provider->password)) {

                // check user confirmation and activation
                ///
                if ($provider->status == 'pending') {

                    return $this->helper->responseJson(0, 'لم يتم تاكيد الحساب بعد الرجاء إنتظار موافقة الإدارة');

                } elseif ($provider->status == 'rejected') {
                    return $this->helper->responseJson(0, 'تم رفض طلبك للانضمام كمقدم خدمة');

                } elseif ($provider->status == 'deactivate') {

                    return $this->helper->responseJson(0, 'تم حظرك من الاستخدام يمكنك التواصل مع الادارة');
                }
                ///
                ///////////////////

                //create passport token
                $token = $provider->createToken('android')->accessToken;

                if (!$provider->tokens()->where('token', $request->token)->count())
                    $provider->tokens()->create(['token' => $request->token, 'phone_type' => $request->os]);

                $ProviderData = new Provider($provider);

                return $this->helper->responseJson(1, 'تم تسجيل الدخول بنجاح', ['token' => $token, 'user' => $ProviderData]);

            } else {

                return $this->helper->responseJson(0, 'كلمة المرور غير صحيحة');
            }

        }

        return $this->helper->responseJson(0, 'اسم المستخدم غير صحيح');
    }

    public function ProviderSendPinCode(Request $request)
    {
        $rules = ['user_name' => 'required'];

        $sms =
            [
                'user_name.required' => 'الاسم مطلوب',
            ];

        $data = validator()->make($request->all(), $rules, $sms);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $record = Delivery::where('user_name', $request->user_name)->first();

        if ($record) {
            return $this->sendPinCode($request, $scape_rules = true, $sendTo = 'phone', $record);
        }

        return $this->helper->responseJson(0, 'اسم المستخدم غير صحيح');
    }

    public function updateProfile(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'region_id' => 'exists:regions,id',
                'user_name' => 'min:1|unique:deliveries,user_name,' . $user->id,
                'name' => 'min:1',
                'email' => 'email|min:1',
                'gender' => 'in:male,female',
                'd_o_b' => 'date',
                'phone' => 'unique:deliveries,phone,' . $user->id,
                'password' => 'confirmed|min:6',
                'photo' => 'image|mimes:jpeg,jpg,png,gif,svg',
                'transport_photo' => 'image|mimes:jpeg,jpg,png,gif,svg',
            ];
        $sms =
            [
                'user_name.unique' => 'الاسم مستخدم بالفعل حاول باستخدام اسم اخر',
                'user_name.min' => 'الرجاء ادخال اسم المستخدم',
                'name.min' => 'الرجاء ادخال اسم المحل',
                'phone.unique' => 'رقم الهاتف مستخدم بالفعل',
                'password.confirmed' => 'كلمة السر غير متطابقة',
                'password.min' => 'اقل قيمة لكلمة المرور هي 6 حروف',
                'photo.image' => 'الرجاء اختبار صورة',
                'photo.mimes' => 'الرجاء اختبار صورة',
                'transport_photo.image' => 'الرجاء اختبار صورة',
                'transport_photo.mimes' => 'الرجاء اختبار صورة',
            ];

        $validator = validator()->make($request->all(), $rules, $sms);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
        }

        if ($request->input('password')) {
            $this->resetPasswordInAuth($request);
        }

        if ($request->hasFile('photo')) {
            $old = $user->photos()->where('usage', null)->first() ? $user->photos()->where('usage', null)->first() : null;

            Photo::updatePhoto($request->file('photo'), $old, $user, 'deliveries', 'photos');
        }

        if ($request->hasFile('transport_photo')) {
            $old = $user->photos()->where('usage', 'transport')->first() ? $user->photos()->where('usage', 'transport')->first() : null;

            Photo::updatePhoto($request->file('transport_photo'), $old, $user, 'deliveries', 'photos', 'transport');
        }

        if ($request->input('phone') && $request->phone != $user->phone)
        {
            $user->phone = $request->phone;
            $user->save();
        }

        $user->update($request->except('password', 'phone'));

        return $this->helper->responseJson(1, 'تم التحديث بنجاح');
    }

    public function updateStatus(Request $request)
    {

        $user = $request->user($this->guard);

        $rules =
            [
                'status' => 'required|in:busy,closed,open'
            ];
        $sms =
            [
                'status.required' => 'الرجاء اختيار الحالة',
                'status.in' => 'الرجاء اختيار الحالة',
            ];

        $validator = validator()->make($request->all(), $rules, $sms);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
        }

        $user->types = $request->status;
        $user->save();

        return $this->helper->responseJson(1, 'تم التحديث بنجاح' , $user->types);
    }
}
