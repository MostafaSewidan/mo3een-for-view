<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Pharmacy extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'latitude' => $this->latitude,
            'longitude' => $this->longitude,
            'description' => $this->description,
            'status' => $this->case,
            'rate' => $this->rate,
            'delivery_start_time' => $this->delivery_start_time,
            'delivery_end_time' => $this->delivery_end_time,
            'delivery_time_unit' => 'دقيقة',
            'delivery_type' => $this->delivery_type == 'fast' ? 'توصيل سريع' : '',
            'distance' => number_format($this->distance , 1),
            'delivery_cost' => $this->delivery_cost,
            'photo' => $this->photo != null ? asset($this->photo) : asset('photos/logo.png'),
            'cover_photo' => $this->cover != null ? asset($this->cover) : asset('photos/logo.png'),
            // 'region'              => new Region($this->region),
        ];
    }
}
