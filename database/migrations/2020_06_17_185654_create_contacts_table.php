<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContactsTable extends Migration {

	public function up()
	{
		Schema::create('contacts', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('contact_reason_id')->nullable();
			$table->string('name')->nullable();
			$table->string('phone')->nullable();
			$table->text('contact');
			$table->tinyInteger('is_read')->default('0');
			$table->string('contactable_type')->nullable();
			$table->integer('contactable_id')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('contacts');
	}
}