$('.main-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    dots:false,
    items:1,
    rtl:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true
});

$('.categories-carousel').owlCarousel({
    loop:true,
    margin:10,
    nav:false,
    dots:false,
    items:5,
    rtl:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    responsiveClass:true,
    responsive:{
        0:{
            items:2
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
});


$("#job-apply-reset").click(function (e) {
    e.preventDefault();
    $('#job-apply-form').trigger('reset');
    // document.getElementById("job-apply-form").reset();

});

var loadFile = function(event,imageId) {
    var image = document.getElementById(imageId);
    image.src = URL.createObjectURL(event.target.files[0]);
};