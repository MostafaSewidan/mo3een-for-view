<?php

namespace App\Http\Resources;

use App\Models\Setting;
use App\MyHelper\Helper;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderClient extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'name'             => $this->name,
            'phone'            => $this->phone,
            "national_ID"      => $this->national_ID,
            "photo"           => $this->photo ? asset($this->photo->path) : asset('photos/cartoon.png'),
        ];
    }
}
