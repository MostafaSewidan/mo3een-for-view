@extends('facilities.layouts.main',[
								'page_header'		=> 'سيارات الإسعاف',
								'page_description'	=> 'عرض السيارات',
                                'link' => url('facilities/ambulances')

								])
@section('content')
    {{--    @include('layouts.partials.validation-errors')--}}
    {{--    @include('flash::message')--}}
    @push('styles')
        <style>
            @media print {
                a[href]:after {
                    content: none !important;
                }
            }
        </style>
    @endpush
    <button type="button" class="btn btn-sm btn-default pull-left hidden-print" onclick="window.print()"><i
                class="fa fa-print"></i></button>
    <section id="printArea">
        <div class="row m-b-lg m-t-lg">
            <div class="col-md-6 mb-5">

                <div class="profile-image">
                    <img src="{{asset($record->photo ? $record->photo : 'inspina/img/p3.jpg')}}"
                         class="rounded-circle circle-border m-b-md" alt="profile">
                </div>
                <div class="profile-info">
                    <div class="">
                        <div>
                            <h2 class="no-margins">
                                {{optional($record->hospital)->name}}
                            </h2>
                            <h4>{{$record->name}}</h4>
                            <h4>{{$record->user_name}}</h4>
                            <h4>{{$record->phone}}</h4>
                            <h4>{{optional($record->region)->name}} - {{optional($record->region->city)->name}} - {{optional($record->region->city->country)->name}}</h4>
                            <h4>
                                @if($record->latitude && $record->longitude)
                                    <a href="{{url('http://maps.google.com/?q='.$record->latitude.','.$record->longitude)}}"
                                       target="_blank" class="btn btn-warning">
                                        <i class="fa fa-map-marker"></i>
                                    </a>
                                @endif
                            </h4>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <h3> {!! \App\MyHelper\Helper::ratStars($reviews['provider_rate']) !!} <strong style="padding-right: 3%">{{$reviews['provider_rate']}}</strong> </h3>
                <ul class="stat-list" style="width: 57%;">
                    @php $colors = ['#FF0000','#FF6363','#FEDD12','#4BEF8C','#00A542'] @endphp
                    @for($i = 1; $i <= 5; $i++)
                        <li>
                            <div class="stat-percent" style="margin-left: 7px;margin-top: -6px;">{{$i}}</div>
                            <div class="progress progress-mini">
                                <div style="width: 100%;background-color: #707070;" class="progress-bar">
                                    <span style="width: {{$reviews['counts'][$i]}}%; background-color: {{$colors[$i - 1]}}" class="progress-bar"></span>
                                </div>
                            </div>
                        </li>
                    @endfor
                </ul>
            </div>

            <div class="clearfix"></div>
            <br>

            <div class="col-md-6">

                <table class="table m-b-xs">
                    <tbody>

                    <tr>
                        <td>
                            رصيد المحفظة : <strong>{{$record->wallet_balance}}</strong>
                        </td>
                        <td>
                            رصيد النقاط : <strong>{{$record->points}}</strong>
                        </td>
                    </tr>

                    <tr>

                        <td>
                            عدد الطلبات: <strong>{{$record->orders()->count()}}</strong>
                        </td>
                        <td>
                             الخدمات :
                            <strong>
                                @foreach($record->services()->get() as $service)
                                    <span class="label label-primary"
                                          style="display: inline-block;margin-bottom: 5px;">{{$service->name}}</span>
                                @endforeach
                            </strong>
                        </td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>
        <div class="ibox ibox-primary">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-4" style="margin-bottom: 15px">
                        <a href="{{url('facilities/ambulances/'.$record->id.'/edit')}}" class="btn btn-success btn-block btn-lg p-5">تحديث البيانات وتعديلها</a>
                    </div>

                    <div class="col-sm-4" style="margin-bottom: 15px">
                        <a href="{{url('facilities/orders').'?ambulance_id='.$record->id}}" class="btn btn-success btn-block btn-lg p-5">الطلبات</a>
                    </div>

                    <div class="col-sm-4" style="margin-bottom: 15px">
                        <a href="#" class="btn btn-success btn-block btn-lg p-5">التقارير المالية</a>
                    </div>

                    <div class="col-sm-4" style="margin-bottom: 15px">
                        <a href="#" class="btn btn-success btn-block btn-lg p-5">التقارير</a>
                    </div>
                </div>

            </div>
        </div>
    </section>

@endsection