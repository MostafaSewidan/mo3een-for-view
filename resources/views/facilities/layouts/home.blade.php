@extends('facilities.layouts.main',[
                                    'page_header'       => 'الصفحة الرئيسية',
                                    'page_description'  => '',
                                    'link' => url('facilities')
                                ])
@section('content')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>


    <div class="row">

        <div class="col-md-12">
            <div class="ibox ">
                <div class="ibox-title">
                    <div class="ibox-tools">
                        <strong class="label label-success"
                                style="font-size: 1pc;">{{$reviews['provider_rate']}}</strong>

                    </div>
                    <h5>تقييمات العملاء</h5>
                </div>
                <div class="ibox-content">
                    <h3> {!! \App\MyHelper\Helper::ratStars($reviews['provider_rate'] ,'font-size: 2.5pc;',' ')!!} </h3>
                    <ul class="stat-list" style="width: 80%;">
                        @php $colors = ['#FF0000','#FF6363','#FEDD12','#4BEF8C','#00A542'] @endphp
                        @for($i = 1; $i <= 5; $i++)
                            <li>
                                <div class="stat-percent" style="margin-left: 7px;margin-top: -6px;">{{$i}}</div>
                                <div class="progress progress-mini">
                                    <div style="width: 100%;background-color: #707070;" class="progress-bar">
                                        <span style="width: {{$reviews['counts'][$i]}}%; background-color: {{$colors[$i - 1]}}"
                                              class="progress-bar"></span>
                                    </div>
                                </div>
                            </li>
                        @endfor
                    </ul>
                </div>
            </div>

        </div>

        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>رسم بياني الطلبات</h5>
                </div>
                <div class="ibox-content">
                    @push('scripts')
                        {!! $pie->script() !!}
                    @endpush
                    {!!$pie->container() !!}
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>رسم بياني لسيارات الأسعاف</h5>
                </div>
                <div class="ibox-content">
                    @push('scripts')
                        {!! $amb->script() !!}
                    @endpush
                    {{$amb->container()}}
                </div>
            </div>
        </div>
    </div>

    <div class="row">

        @foreach($services as $service)
            <div class="col-md-4">
                <div class="ibox ">
                    <div class="ibox-title">
                        <div class="ibox-tools">
                            <a class="label label-danger float-right"
                               href="{{url(($service->id != 3 ? 'facilities/orders?service='.$service->id : 'facilities/events?') . '&status=pending')}}"> الطلبات الجديدة :
                                {{$service->id != 3 ? $service->orders_count : $eventsCount}}

                            </a>
                        </div>
                        <h5>{{$service->name}}</h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">

                            <div class="col-md-6">

                                <h5>الطلبات</h5>

                                <div class="ibox-content">
                                @php $pieService = (new \App\Http\Controllers\Facility\HomeController())->buildPieChart(request() , $service->id);@endphp
                                @push('scripts')
                                    {!! $pieService->script() !!}
                                @endpush

                                {!!$pieService->container() !!}
                                </div>
                            </div>

                            <div class="col-md-6">

                                <h5>الإسعاف</h5>
                                <div class="ibox-content">
                                @php $pieService = (new \App\Http\Controllers\Facility\HomeController())->buildAmbChart(request() , $service->id);@endphp
                                @push('scripts')
                                    {!! $pieService->script() !!}
                                @endpush

                                {!!$pieService->container() !!}
                                </div>
                            </div>

{{--                            <div class="col-md-6">--}}
{{--                                @push('scripts')--}}
{{--                                    {!! $pie->script() !!}--}}
{{--                                @endpush--}}
{{--                                {!!$pie->container() !!}--}}
{{--                            </div>--}}

                        </div>
                    </div>
                </div>
            </div>
{{--            <li role="presentation" class="bg-gray" style="margin-bottom: 10px;min-width:200px;">--}}
{{--                <a href="{{url('facilities/ambulances?service_id='.$service->id.'&country='.request()->input('country_id'))}}">{{$service->name}}--}}
{{--                    <span class="mr-3 badge badge-info">{{$service->working_ambulances_count}}</span></a>--}}
{{--            </li>--}}
        @endforeach
    </div>

@endsection

