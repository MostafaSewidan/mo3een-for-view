<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class OrderCollection extends ResourceCollection
{
    protected $responseTo = 'client';

    public function responseTo($responseTo = 'client')
    {
        $this->responseTo = $responseTo;
        return $this;
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->map(function (Order $resource) use ($request) {
            return $resource->responseTo($this->responseTo)->toArray($request);
        })->all();
    }

}
