<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CharitableActor extends Model
{

    protected $table = 'charitable_actors';
    public $timestamps = true;
    protected $fillable = array('name');


}