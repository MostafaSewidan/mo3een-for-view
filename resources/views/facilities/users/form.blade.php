
{!! \App\MyHelper\Field::text('name' , 'الاسم')!!}
{!! \App\MyHelper\Field::email('email' , 'البريد الالكترونى') !!}
{!! \App\MyHelper\Field::password('password' , 'كلمة المرور') !!}
{!! \App\MyHelper\Field::password('password_confirmation' , 'تاكيد كلمة المرور') !!}
<br>
<div class="form-group" id="permissions_wrap">
    <label for="permissions">الرتب</label>
    <div class="">
        <br>

        @foreach( $roles as $role)

            @if($model->hasRole($role->name))

                {!! \App\MyHelper\Field::checkBox('roles', $role  , 'checked') !!}
            @else
                {!! \App\MyHelper\Field::checkBox('roles', $role ) !!}
            @endif

        @endforeach
    </div>
    <br>
    <br>

</div>


