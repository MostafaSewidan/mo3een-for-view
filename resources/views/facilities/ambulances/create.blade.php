@extends('facilities.layouts.main',[
                                'page_header'       => 'سيارات الإسعاف',
                                'page_description'  => 'اضافة سيارة',
                                'link' => url('facilities/ambulances')
                                ])


@section('content')
    <!-- general form elements -->
    <div class="ibox ibox-primary">
        <!-- form start -->
        {!! Form::model($model,[
                                'action'=>'Facility\AmbulanceController@store',
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'POST',
                                'files' => true
                                ])!!}

        <div class="ibox-content">

            @include('facilities.ambulances.form')

            <div class="ibox-footer">
                <button type="submit" class="btn btn-primary" id="myBtn">إرسال للإدارة</button>
            </div>

        </div>
        {!! Form::close()!!}

    </div><!-- /.box -->

@endsection
