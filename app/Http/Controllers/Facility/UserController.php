<?php

namespace App\Http\Controllers\Facility;

use App\Http\Controllers\Controller;
use App\Models\HospitalUser;
use App\MyHelper\Helper;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Response;
use Hash;
use Auth;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    protected $model;
    protected $guard = 'facilities';
    protected $url = 'facilities/users/';
    protected $viewsDomain = 'facilities.users.';

    public function __construct()
    {
        $this->model = new Role();
        $this->guard = 'facilities';
    }

    /**
     * @param $view
     * @param array $params
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function changePassword()
    {
        return view('facilities.reset-password');
    }

    public function changePasswordSave(Request $request)
    {
        $this->validate($request, [
            'old-password' => 'required',
            'password'     => 'required|confirmed',
        ]);

        $user = Auth::user();

        if (Hash::check($request->input('old-password'), $user->password)) {
            // The passwords match...
            $user->password = bcrypt($request->input('password'));
            $user->save();
            session()->flash('success', 'تم تحديث كلمة المرور');
            return view('facilities.reset-password');
        } else {
            session()->flash('fail', 'كلمة المرور غير صحيحة');
            return view('facilities.reset-password');
        }

    }

    public function index(Request $request)
    {
        $facility = auth($this->guard)->user()->hospital;

        $users = $facility->users()->where(function ($q) use ($request) {
            if ($request->name) {
                $q->where(function ($q) use ($request) {

                    $q->where('name', 'LIKE', '%' . $request->name . '%')
                      ->orWhere('email', 'LIKE', '%' . $request->name . '%');
                });
            }
            if ($request->role_name) {

                $q->whereHas('roles', function ($q) use ($request) {

                    $q->where('display_name', 'LIKE', '%' . $request->role_name . '%');
                });
            }

            if ($request->from) {
                $q->whereDate('created_at', '>=', Helper::convertDateTime($request->from));
            }

            if ($request->to) {
                $q->whereDate('created_at', '<=', Helper::convertDateTime($request->to));
            }


        })->latest()->paginate(20);
        return $this->view('index', compact('users'));
    }

    public function create(User $model)
    {
        $facility = auth($this->guard)->user()->hospital;
        $model = new HospitalUser();
        $roles = $facility->roles()->get();

        return $this->view('create', compact('model', 'roles'));
    }

    public function store(Request $request)
    {

        $facility = auth($this->guard)->user()->hospital;

        $rules =
            [
                'name'     => 'required',
                'email'    => 'required|email|unique:hospital_users',
                'password' => 'required|confirmed',
                'roles'  => 'required',
                'roles.*'  => 'required|exists:roles,id',
            ];

        $error_sms =
            [
                'name.required'      => 'الرجاء ادخال الاسم ',
                'email.unique'       => ' البريد الالكتروني موجود بالفعل',
                'email.required'     => 'الرجاء ادخال البريد الالكتروني',
                'password.required'  => 'الرجاء ادخال كلمة المرور',
                'password.confirmed' => 'الرجاء التاكد من كلمة المرور ',

            ];

        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return back()->withInput()->withErrors($data->errors());
        }
        $user = $facility->users()->create(request()->all());

        $user->update(['password' => Hash::make($request->password)]);

        $user->assignRole($request->roles);

        session()->flash('success', 'تمت الاضافة بنجاح');
        return redirect($this->url);
    }

    public function show($id)
    {
        /* $user = User::with('addresses')->findOrFail($id);
         $orders = $user->orders()->latest()->paginate(5);
         return view('facilities.sushi.user',compact('user','orders'));*/
    }

    public function edit($id)
    {
        $facility = auth($this->guard)->user()->hospital;
        $model = HospitalUser::findOrFail($id);
        $roles =  $facility->roles()->get();

        return $this->view('edit', compact('model', 'roles'));
    }

    public function update(Request $request, $id)
    {
        $facility = auth($this->guard)->user()->hospital;
        $record = $facility->users()->findOrFail($id);

        $rules =
            [
                'name'     => 'required',
                'email'    => 'required|email|unique:hospital_users,email,' . $record->id . '',
                'password' => 'required|confirmed',
                'roles.*'  => 'required|exists:roles,id',
            ];

        $error_sms =
            [
                'name.required'      => 'الرجاء ادخال الاسم ',
                'email.required'     => 'الرجاء ادخال البريد الالكتروني',
                'email.unique'       => ' البريد الالكتروني موجود بالفعل',
                'password.required'  => 'الرجاء ادخال كلمة المرور ',
                'password.confirmed' => 'الرجاء التاكد من كلمة المرور ',

            ];
        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return redirect($this->url . $id . '/edit')->withInput()->withErrors($data->errors());
        }


        $record->update($request->except('password'));

        if ($request->has('password') && $request->password != null) {
            $record->update(['password' => Hash::make($request->password)]);
        }

        DB::table('model_has_roles')->where('model_id', $id)->delete();

        $record->assignRole($request->roles);

        session()->flash('success', 'تم التعديل بنجاح');
        return back();

    }

    public function destroy($id)
    {
        $facility = auth($this->guard)->user()->hospital;
        $record = $facility->users()->findOrFail($id);

        if (auth($this->guard)->user()->id == $record->id) {
            $data = [
                'status' => 0,
                'msg'    => 'This email, you cannot deactivate it',
                'id'     => $id
            ];

            return Response::json($data, 200);

        }

        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $record->delete();

        $data = [
            'status' => 1,
            'msg'    => 'Deleted successfully',
            'id'     => $id
        ];

        return Response::json($data, 200);
    }

    public function toggleBoolean($id , $action)
    {
        $facility = auth($this->guard)->user()->hospital;
        $record = $facility->users()->findOrFail($id);

        if(auth($this->guard)->user()->id == $id )
        {
            return Helper::responseJson(0,'لا يمكنك إلغاء تفعيل حسابك');
        }

        $activate = Helper::toggleBoolean($record , $action);

        if ($activate) {
            return Helper::responseJson(1,'تمت العملية بنجاح');
        }

        return Helper::responseJson(0,'حدث خطأ');
    }

    public function home()
    {
        return view('facilities.layouts.home');
    }
}
