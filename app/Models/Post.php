<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{

    protected $table      = 'posts';
    public    $timestamps = true;
    protected $fillable   = array('title', 'short_description', 'long_description', 'order');


    public function attachment()
    {
        return $this->morphOne('App\Models\Attachment', 'attachmentable');
    }

    public function getPhotoAttribute()
    {
        return $this->attachment ? asset($this->attachment->path) : null;
    }
}
