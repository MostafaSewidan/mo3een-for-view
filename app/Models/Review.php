<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{

    protected $table = 'reviews';
    public $timestamps = true;
    protected $fillable = array('client_id','rate', 'comment', 'reviewable_type', 'reviewable_id','client_id');

    public function reviewable()
    {
        return $this->morphTo();
    }

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function hospital()
    {
        return $this->belongsTo(Hospital::class);
    }

}
