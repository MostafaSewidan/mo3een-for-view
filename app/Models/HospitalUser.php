<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

class HospitalUser extends Authenticatable
{

    use HasRoles , Notifiable;

    protected $table = 'hospital_users';
    public $timestamps = true;

    protected $fillable = array(
        'name',
        'email',
        'password',
        'remember_token',
        'hospital_id'
    );

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function hospital()
    {
        return $this->belongsTo(Hospital::class);
    }


}
