<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Api\ParentApi;
use App\Http\Resources\Address;
use App\Http\Resources\PointsReport;
use App\Http\Resources\Review;
use App\Models\Setting;
use App\MyHelper\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TransactionController extends ParentApi
{
    public function __construct()
    {

        $this->helper = new Helper();
        $this->guard = 'api_client';
    }

    public function addReviews(Request $request)
    {
        //TODO edit and add hospital id
        $rules =
            [
                'type'        => 'required|in:Pharmacy,Delivery',
                'store_id'    => 'required_if:type,Pharmacy|exists:stores,id',
                'delivery_id' => 'required_if:type,Delivery|exists:deliveries,id',
                'comment'     => 'required',
                'rate'        => 'required|numeric|max:5|min:0'
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $id = $request->type == 'Pharmacy' ? $request->store_id : $request->delivery_id;
        $className = 'App\Models\\' . $request->type;

        $model = new $className();
        $reviewable = $model->find($id);

        $review = !$reviewable->reviews()->where('client_id', $request->user()->id)->first()
            ?

            $reviewable->reviews()->create(array_merge($request->all(), ['client_id' => $request->user('api_client')->id]))
            :
            $reviewable->reviews()->where('client_id', $request->user()->id)->first()->update($request->all());

        $reviewable->rate = $reviewable->reviews()->avg('rate');
        $reviewable->save();

        return $this->helper->responseJson(1, 'success', new Review($reviewable->reviews()->where('client_id', $request->user()->id)->first()));

    }


    /******************************************/
    //          address cycle
    /******************************************/

    // add address
    public function addAddress(Request $request)
    {
        $user = auth('api_client')->user();
        $rules =
            [
                'longitude' => 'required',
                'latitude'  => 'required',
                'is_saved'  => 'required|in:1,0',
                'title'     => 'required_if:is_saved,1',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $address = $user->addresses()->create($request->all());

        return $this->helper->responseJson(1, 'تمت الاضافة بنجاح', new Address($address));

    }


    //show addresses
    public function showAddresses()
    {
        return (Address::collection(auth('api_client')->user()->addresses()->where('is_saved' , 1)->latest()->get()))->additional([
                                                                                                               "status"  => 1,
                                                                                                               "massage" => "SUCCESS",
                                                                                                           ]);
    }

    //remove address
    public function removeAddress(Request $request)
    {
        $user = auth('api_client')->user();
        $rules =
            [
                'address_id' => 'required|exists:addresses,id',
            ];

        $sms =
            [
                'address_id.required' => 'العنوان مطلوب',
                'address_id.exists' => 'العنوان مطلوب',
            ];

        $data = validator()->make($request->all(), $rules, $sms);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $address = $user->addresses()->find($request->address_id);

        if ($address) {
            $address->is_saved = 0;
            $address->save();

            return $this->helper->responseJson(1, 'تمت الحذف بنجاح');
        }
        return $this->helper->responseJson(0, 'لم يتم العثور علي العنوان');
    }

    /******************************************/


    /******************************************/
    //          wallet cycle
    /******************************************/

    public function RechargeWallet(Request $request)
    {
        $user = auth('api_client')->user();
        $rules =
            [
                'type'   => 'required|in:points,credit_card',
                'amount' => 'required|min:1',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        if ($request->type == 'points') {
            $oldPoints = $user->pointsTransactions()->latest()->first();
            $oldWallet = $user->walletTransactions()->latest()->first();

            if ($oldPoints && $oldPoints->points_after > $request->amount) {
                $pointsCost = Setting::where(['key' => 'client_points_cost'])->first();

                if ($pointsCost) {

                    DB::beginTransaction();

                    $rechargeAmount = $pointsCost->value * $request->amount;

                    $user->pointsTransactions()->create(
                        [
                            'points_before' => $oldPoints->points_after,
                            'quantity'      => -$request->amount,
                            'points_after'  => $oldPoints->points_after - $request->amount,
                            'factor'        => $pointsCost->value,
                            'type'          => 'change',
                        ]
                    );

                    $user->walletTransactions()->create(
                        [
                            'balance_before' => $oldWallet ? $oldWallet->balance_after : 0,
                            'amount'         => $rechargeAmount,
                            'balance_after'  => $oldWallet ? $oldWallet->balance_after + $rechargeAmount : $rechargeAmount,
                            'action'         => 'points',
                        ]
                    );

                    DB::commit();

                    return $this->helper->responseJson(1, 'تمت العملية بنجاح' , new PointsReport($user->pointsTransactions()->latest()->first()));
                } else {
                    return $this->helper->responseJson(0, ' شحن المحفظة متوقف حاليا');
                }

            } else {

                return $this->helper->responseJson(0, 'رصيدك من النقاط لا يكفي لاتمام العملية');
            }

        } elseif ($request->type == 'credit_card') {
            //TODO
        }

        return $this->helper->responseJson(0, 'حدث مشكلة ما حاول في وقت لاحق');
    }


    /******************************************/
}
