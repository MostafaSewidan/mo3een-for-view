<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Date extends Model
{

    protected $table = 'dates';
    public $timestamps = true;
    protected $fillable = array('day_id', 'start', 'end');

    public function day()
    {
        return $this->belongsTo('App\Models\Day');
    }

}