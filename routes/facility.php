<?php

/*
|--------------------------------------------------------------------------
| facilities Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Facility'], function () {

    Route::group(['middleware' => ['guest:facilities']], function () {
        Route::get('/login', 'AuthController@viewLogin')->name('facilities-login');
        Route::post('/login', 'AuthController@login');
    });

//'auto-check-permission' TODO, 'facility_activation'
    Route::group(['middleware' => ['auth:facilities']], function () {

        Route::get('/', 'HomeController@index');

        Route::get('home', 'HomeController@index');

        Route::resource('users', 'UserController');
        Route::get('users/toggle-boolean/{id}/{action}', 'UserController@toggleBoolean')->name('facilities.users.toggleBoolean');

        Route::resource('roles', 'RoleController');

        Route::resource('reviews', 'ReviewController');

        Route::resource('events', 'EventController');
        Route::post('accept-events-request', 'EventController@acceptEvent');
        Route::post('cancel-events-request', 'EventController@requestCancel');

        Route::resource('orders', 'OrderController');
        Route::post('select-delivery-orders/{id}', 'OrderController@selectDelivery');
        Route::post('cancel-orders-request', 'OrderController@requestCancel');

        Route::resource('ambulances', 'AmbulanceController');
        Route::get('get-ambulance','AmbulanceController@getAmbulance')->name('facilities.get-ambulance');
        Route::get('ambulance/stats', 'AmbulanceController@stats')->name('facilities.ambulance.stats');
        Route::get('ambulance/toggle-boolean/{id}/{action}', 'AmbulanceController@toggleBoolean')->name('facilities.ambulances.toggleBoolean');

    });
});
