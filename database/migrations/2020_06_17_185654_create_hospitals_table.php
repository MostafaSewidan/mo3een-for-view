<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHospitalsTable extends Migration {

	public function up()
	{
		Schema::create('hospitals', function(Blueprint $table) {
			$table->increments('id');
			$table->decimal('latitude', 10,8);
			$table->decimal('longitude', 10,8);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('hospitals');
	}
}