@extends('facilities.layouts.main',[
                                'page_header'       => 'عرض الرحلات ',
                                'page_description'  => 'طلب رقم #'. $order->id,
                                'link' => url('facilities/events')
                                ])
@section('content')


    <div class="box box-primary">
        <div class="box-body">

            @include('flash::message')
            @if(!empty($order))
                <section class="invoice">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-xs-12">
                            <h2 class="page-header">
                                <i class="fa fa-globe"></i> تفاصيل طلب # {{$order->id}}
                                <small class="pull-left"><i class="fa fa-calendar-o"></i> {{$order->asked_at}}
                                </small>
                            </h2>
                        </div><!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <div class="col-sm-3 invoice-col">
                            <b>تفاصيل الطلب</b>
                            <address>
                                <i class="fa fa-angle-left" aria-hidden="true"></i><b> رقم الطلب </b>#{{$order->id}}<br>
                                <i class="fa fa-angle-left" aria-hidden="true"></i><b> حالةالطلب:</b>
                                <span class="label xs label-primary">{{$order->client_status_text}}</span>
                                <br>
                                <br>

                                <i class="fa fa-angle-left" aria-hidden="true"></i><b>
                                    تاريخ بدء الرحلة :</b>

                                {{\App\MyHelper\Helper::convertDateTimeNotString($order->schedule_date)->locale('ar')->isoFormat('dddd  , MMMM  ,  Do / YYYY ')}}

                                <br>


                                {{--                                <i class="fa fa-angle-left" aria-hidden="true"></i><b> يتراوح--}}
                                {{--                                    السعر : </b>{{$order->start_price_range}}<b> من </b>{{$order->end_price_range}}<br>--}}

                                {{--                                <i class="fa fa-angle-left" aria-hidden="true"></i><b> تكلفة الرحلة--}}
                                {{--                                    الفعلية : </b>{{$order->real_price}}<br>--}}

                                {{--                                <br>--}}

                                <i class="fa fa-angle-left" aria-hidden="true"></i><b> العنوان :</b>
                                <a href="{{url('http://maps.google.com/?q='.optional($order->startAddress)->latitude.','.optional($order->startAddress)->longitude)}}"
                                   target="_blank">
                                    <i class="fa fa-map-marker"></i>

                                    {{optional($order->startAddress)->title}}
                                </a>
                                <br>

                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                            <b>العميل</b>
                            <address>

                                <i class="fa fa-angle-left"
                                   aria-hidden="true"></i><b>الاسم</b>: @if($order->client){{$order->client->name}}@endif
                                <br>
                                <i class="fa fa-angle-left"
                                   aria-hidden="true"></i><b>الهاتف</b>: @if($order->client){{$order->client->phone}}@endif
                                <br>

                            </address>
                        </div><!-- /.col -->
                        <div class="col-sm-3 invoice-col">
                            <b>بيانات الحدث</b>
                            <address>

                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                <b>اسم المنشأة المقيمة للحدث</b>: {{optional($order->patient)->facility_name}}
                                <br>

                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                <b>نوع الحدث</b>: {{optional($order->patient)->event_type}}
                                <br>

                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                <b>رقم الهاتف</b>: {{optional($order)->phone}}
                                <br>

                                <i class="fa fa-angle-left" aria-hidden="true"></i><b>
                                    تاريخ بدء الحدث :</b>

                                {{\App\MyHelper\Helper::convertDateTimeNotString($order->event_start_date)->locale('ar')->isoFormat('dddd  , MMMM  ,  Do / YYYY ')}}

                                <br>

                                <i class="fa fa-angle-left" aria-hidden="true"></i><b>
                                    تاريخ إنتهاء الحدث :</b>
                                {{\App\MyHelper\Helper::convertDateTimeNotString($order->event_end_date)->locale('ar')->isoFormat('dddd  , MMMM  ,  Do / YYYY ')}}

                                <br>

                                <i class="fa fa-angle-left" aria-hidden="true"></i>
                                <b>ملاحظة</b>: {{optional($order->patient)->note}}
                                <br>

                            </address>
                        </div>

                        <div class="col-sm-3 invoice-col">
                            <b>سيارتك في الرحلة</b>
                            <br><br>
                            <address>
                                @if($order->eventDeliveries()->where('hospital_id', $facility->id)->count())
                                    @foreach($order->eventDeliveries()->get() as $delivery)
                                        <a href="{{url(route('ambulances.show',optional($delivery)->id))}}"
                                           class="label label-primary"
                                           style="display: inline-block;margin-bottom: 5px;"
                                        >
                                            {{optional($delivery)->name}}
                                        </a>
                                    @endforeach
                                @endif
                            </address>
                        </div>


                    </div><!-- /.row -->


                    <!-- Table row -->
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <tr>
                                <th>الأعداد</th>
                                <th class="text-center"> المطلوب</th>
                                <th class="text-center"> المسجل</th>
                                <th class="text-center"> المسجل من منشأتك</th>
                                <th class="text-center"> المتاح</th>
                            </tr>
                            <tr>

                                <td>سيارات الإسعاف</td>
                                <td class="text-center">{{optional($order->patient)->cars_number}}</td>
                                <td class="text-center">{{$order->eventDeliveries()->count()}}</td>
                                <td class="text-center">{{$order->eventDeliveries()->where('hospital_id', $facility->id)->count()}}</td>
                                <td class="text-center">{{ optional($order->patient)->cars_number - $order->eventDeliveries()->count()}}</td>
                            </tr>
                            <tr>

                                <td>الأطباء</td>
                                <td class="text-center">{{optional($order->patient)->doctors}}</td>
                                <td class="text-center">{{$order->hospitalDoctors()->sum('number')}}</td>
                                <td class="text-center">
                                    {{
                                    $order->hospitalDoctors()->where('hospital_id' , $facility->id)->first() ?
                                                        optional(optional($order->hospitalDoctors()->where('hospital_id' , $facility->id)->first())->pivot)->number
                                                        : 0
                                    }}
                                </td>
                                <td class="text-center">{{$order->patient->doctors - $order->hospitalDoctors()->sum('number')}}</td>
                            </tr>
                            <tr>

                                <td>الممرضين</td>
                                <td class="text-center">{{optional($order->patient)->nurses}}</td>
                                <td class="text-center">{{$order->hospitalNurses()->sum('number')}}</td>
                                <td class="text-center">
                                    {{
                                         $order->hospitalNurses()->where('hospital_id' , $facility->id)->first() ?
                                                        optional(optional($order->hospitalNurses()->where('hospital_id' , $facility->id)->first())->pivot)->number
                                                        : 0
                                    }}
                                </td>
                                <td class="text-center">{{$order->patient->nurses - $order->hospitalNurses()->sum('number')}}</td>
                            </tr>
                        </table>
                    </div>

                    <!-- this row will not appear when printing -->
                    @if($order->status != 'End_journey')
                        <div class="row no-print">
                            <div class="col-xs-12">
                                <a  class="btn btn-success pull-left" id="print-all">
                                    <i class="fa fa-print"></i> طباعة </a>

                                @if(in_array($order->status,['pending']))

                                    <button class="btn btn-primary" data-toggle="modal" data-target="#choose-delivery">
                                        <i class="fa fa-check"></i> تسجيل
                                    </button>

                                @else

                                    <button class="btn btn-warning" data-toggle="modal" data-target="#choose-delivery">
                                        <i class="fa fa-edit"></i> تعديل
                                    </button>

                                @endif
                            <!-- Modal -->
                                <div class="modal fade" id="choose-delivery" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">الإشتراك في الحدث</h5>
                                                <button type="button" class="close" data-dismiss="modal"
                                                        aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>

                                            <div class="modal-body">

                                                {!! Form::open([
                                                            'url' => url('facilities/accept-events-request'),
                                                            'method' => 'post',
                                                            'id' => 'addForm',
                                                        ]) !!}

                                                {!! Form::hidden('order_id',$order->id) !!}

                                                {!! \App\MyHelper\Field::multiSelect(
                                                    'ambulances',
                                                     'السيارات',
                                                      $ambulances->where('status','active')->pluck('name','id')->toArray() ,
                                                      $order->eventDeliveries()->where('hospital_id', $facility->id)->count() ?
                                                      $order->eventDeliveries->where('hospital_id', $facility->id)->pluck('id')->toArray() : null
                                                      )!!}

                                                @if($order->patient->doctors > 0)

                                                    {!!
                                                        \App\MyHelper\Field::number( 'doctors', 'الأطباء',
                                                        $order->hospitalDoctors()->where('hospital_id' , $facility->id)->first() ?
                                                        optional(optional($order->hospitalDoctors()->where('hospital_id' , $facility->id)->first())->pivot)->number
                                                        : null
                                                    )!!}

                                                @endif

                                                @if($order->patient->nurses > 0)

                                                    {!!
                                                        \App\MyHelper\Field::number( 'nurses', 'الممرضين',
                                                        $order->hospitalNurses()->where('hospital_id' , $facility->id)->first() ?
                                                        optional(optional($order->hospitalNurses()->where('hospital_id' , $facility->id)->first())->pivot)->number
                                                        : null
                                                    )!!}

                                                @endif

                                            </div>

                                            <div class="modal-footer">
                                                <button type="submit" id="add-btn" class="btn btn-primary">تأكيد
                                                </button>
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                    إغلاق
                                                </button>
                                            </div>
                                            {!! Form::close() !!}
                                            @push('scripts')
                                                <script>
                                                    $('#addForm').submit(function (e) {
                                                        e.preventDefault();
                                                        var url = $('#addForm').attr('action');
                                                        var form_data = new FormData(this);
                                                        $(".help-block").hide();
                                                        $(".has-error").removeClass('has-error');
                                                        $('#add-btn').attr('disabled', 'disabled');
                                                        $.ajax({
                                                            url: url,
                                                            type: 'post',
                                                            data: form_data,
                                                            cache: false,
                                                            contentType: false,
                                                            processData: false,
                                                            success: function (data) {
                                                                var url = data.url;
                                                                if (url) {
                                                                    window.location = url;
                                                                }
                                                            },
                                                            error: function (data) {
                                                                var error = data.responseJSON.errors;

                                                                $('#add-btn').removeAttr('disabled');
                                                                $.each(error, function (key, value) {
                                                                    $('#' + key + '_error').text('').append(value);
                                                                    $('#' + key + '_error').parent().show();
                                                                    $('#' + key + '_error').parent().parent().addClass('has-error');
                                                                });
                                                            }
                                                        });
                                                    });
                                                </script>
                                            @endpush
                                        </div>
                                    </div>
                                </div>

                                @if(in_array($order->status,['arrives_to_patient','accepted']))


                                    <button class="btn btn-danger" data-toggle="modal" data-target="#cancel-order">
                                        <i class="fa fa-times-circle-o"></i> إلغاء الطلب
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="cancel-order" tabindex="-1" role="dialog"
                                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">إلغاء
                                                        طلب {{ $order->id }}#</h5>
                                                    <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">

                                                    {!! Form::open([
                                                                'url' => url('facilities/cancel-events-request'),
                                                                'method' => 'post',
                                                                'id' => 'cancelForm',
                                                            ]) !!}
                                                    @inject('reasons' , App\Models\RejectOrderReason)

                                                    {!! Form::hidden('order_id',$order->id) !!}
                                                    {!! \App\MyHelper\Field::select('reject_order_reason_id', 'سبب الإلغاء',$reasons->where('is_active',1)->pluck('name','id')->toArray()) !!}

                                                    {!! \App\MyHelper\Field::textarea('description','تفاصيل سبب الإلغاء') !!}

                                                </div>

                                                <div class="modal-footer">
                                                    <button type="submit" id="submit-button" class="btn btn-primary">
                                                        تأكيد
                                                    </button>
                                                    <button type="button" class="btn btn-secondary"
                                                            data-dismiss="modal">
                                                        إغلاق
                                                    </button>
                                                </div>
                                            </div>

                                            {!! Form::close() !!}

                                            @push('scripts')
                                                <script>
                                                    $('#cancelForm').submit(function (e) {
                                                        e.preventDefault();
                                                        var url = $('#cancelForm').attr('action');
                                                        var form_data = new FormData(this);
                                                        $(".help-block").hide();
                                                        $(".has-error").removeClass('has-error');
                                                        // $('#submit-button').attr('disabled', 'disabled');
                                                        $.ajax({
                                                            url: url,
                                                            type: 'post',
                                                            data: form_data,
                                                            cache: false,
                                                            contentType: false,
                                                            processData: false,
                                                            success: function (data) {
                                                                var url = data.url;
                                                                if (url) {
                                                                    window.location = url;
                                                                }
                                                            },
                                                            error: function (data) {
                                                                var error = data.responseJSON.errors;

                                                                $('#submit-button').removeAttr('disabled');
                                                                $.each(error, function (key, value) {
                                                                    $('#' + key + '_error').text('').append(value);
                                                                    $('#' + key + '_error').parent().show();
                                                                    $('#' + key + '_error').parent().parent().addClass('has-error');
                                                                });
                                                            }
                                                        });
                                                    });
                                                </script>
                                            @endpush
                                        </div>
                                    </div>
                            </div>
                            @endif
                            <script>
                                //                                $('#myModal').on('shown.bs.modal', function () {
                                //                                    $('#myInput').focus()
                                //                                })
                            </script>
                        </div>
                    @endif
                </section><!-- /.content -->
                @push('scripts')
                    <script>
                        $("#print-all").click(function () {
                            window.print();
                        });
                    </script>
                @endpush
                <div class="clearfix"></div>
            @endif

        </div>
    </div>

@endsection
