<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\ParentApi;
use App\Models\Contact;
use App\Models\Country;
use App\Models\Coupon;
use App\Models\Delivery;
use App\Models\Gift;
use App\Models\Job;
use App\Models\JobApplication;
use App\Models\Product;
use App\Models\Service;
use App\Models\Store;
use App\MyHelper\Photo;
use Carbon\Carbon;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class FrontController extends Controller
{
    public function index()
    {
        $countries = Country::all();
        return view('front.index', compact('countries'));
    }

    public function about()
    {
        return view('front.about');
    }

    public function job()
    {
        return view('front.jobs');
    }

    public function policies()
    {
        return view('front.policies');
    }

    public function terms()
    {
        return view('front.terms');
    }


    public function contact()
    {
        return view('front.contact');
    }

    public function promoCheck(Request $request)
    {
        $gift = Gift::where('code', $request->promo)->first();
        if ($gift) {
            if ($gift->expires_at <= Carbon::now()->toDateString()) {
                return [
                    'status'  => 0,
                    'message' => 'كود الهدية منتهي الصلاحية'
                ];
            }
            if ($gift->is_used == 1) {
                return [
                    'status'  => 0,
                    'message' => 'كود الهدية مستخدم من قبل'
                ];
            }
            if ($request->use_gift == 1) {
                $gift->update(['is_used' => 1]);
                return [
                    'status'  => 1,
                    'message' => "تم استخدام الهدية"
                ];
            }
            return [
                'status'  => 1,
                'message' => $gift->message
            ];
        } else {
            return [
                'status'  => 0,
                'message' => 'الكود غير صحيح'
            ];
        }
    }


    public function becomePartner()
    {
        return view('front.become-a-partner');
    }

    public function becomeDelivery()
    {
        return view('front.become-a-delivery');
    }

    public function storeSignup()
    {
        $services = Service::all();
        return view('front.store-signup', compact('services'));
    }

    public function deliverySignup()
    {
        return view('front.delivery-signup');
    }

    /**
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storeSignupSubmit(Request $request)
    {
        $rules =
            [
                'region_id'           => 'required|exists:regions,id',
                'countries'           => 'required|exists:countries,id',
                'city'                => 'required|exists:cities,id',
                'name'                => 'required',
                'commercial_register' => 'required',
                'cars_num'            => 'required|numeric',
                'have_ambulance'      => 'required|in:1,0',
                'car_model'           => 'required',

                //TODO 'address' => 'required',
            ];

        $this->validate($request, $rules);
        //dd($request->all());
        $provider = new Delivery();

        $record = $provider->create($request->all());
        $record->password = Hash::make(Str::random(6));
        $record->pin_code = (new ParentApi())->getPinCode();
        $record->pin_code_date_expired = (new ParentApi())->getPinCodeExpiredDate();
        $record->save();
        if ($request->has('services')) {
            $record->services()->attach($request->services);
        }
        flash()->success('تم التسجيل بنجاح سيتم مراجعة طلبكم في خلال 24 ساعه');
        return back();
    }

    /**
     * @param Request $request
     * @throws \Illuminate\Validation\ValidationException
     */
    public function deliverySignupSubmit(Request $request)
    {
        $rules =
            [
                'region_id'         => 'required|exists:regions,id',
                'name'              => 'required',
                'user_name'         => 'required|unique:stores|unique:deliveries',
                'email'             => 'email',
                'phone'             => 'required|unique:deliveries|regex:/(01)[0-9]{9}/',
                'd_o_b'             => 'required|date',
                'photo'             => 'required|image|mimes:jpeg,jpg,png,gif,svg',
                'national_id_photo' => 'required|image|mimes:jpeg,jpg,png,gif,svg',
                'transport_photo'   => 'required|image|mimes:jpeg,jpg,png,gif,svg',
                'transport_photo2'  => 'required|image|mimes:jpeg,jpg,png,gif,svg',
                'license_photo'     => 'required|image|mimes:jpeg,jpg,png,gif,svg',
            ];
        $this->validate($request, $rules);

        $provider = new Delivery();

        $record = $provider->create($request->all());

        $record->password = Hash::make(Str::random(6));
        $record->pin_code = (new ParentApi)->getPinCode();
        $record->pin_code_date_expired = (new ParentApi)->getPinCodeExpiredDate();
        $record->save();

        Photo::addOriginalPhoto($request->file('photo'), $record, 'deliveries', 'photos');
        Photo::addOriginalPhoto($request->file('transport_photo'), $record, 'transports', 'photos', 'transport');
        Photo::addOriginalPhoto($request->file('transport_photo2'), $record, 'transports', 'photos', 'transport2');
        Photo::addOriginalPhoto($request->file('national_id_photo'), $record, 'deliveries', 'photos', 'national_id_photo');
        Photo::addOriginalPhoto($request->file('license_photo'), $record, 'transports', 'photos', 'license_photo');

        flash()->success('تم التسجيل بنجاح سيتم مراجعة طلبكم في خلال 24 ساعه');
        return back();
    }


    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function contactSubmit(Request $request)
    {
        $rules =
            [
                'name'              => 'required',
                'phone'             => 'required',
                'email'             => 'required',
                'contact'           => 'required',
                'contact_reason_id' => 'required|exists:contact_reasons,id',
            ];
        $messages = [
            'name.required'              => 'الاسم مطلوب',
            'phone.required'             => ' التليفون مطلوب',
            'contact.required'           => 'حقل الرسالة مطلوب',
            'email.required'             => 'الايميل مطلوب',
            'contact_reason_id.required' => 'سبب التواصل مطلوب',
            'contact_reason_id.exists'   => 'سبب التواصل غير محدد',
        ];
        $this->validate($request, $rules, $messages);

        $contact = Contact::create($request->all());
        session()->flash('success', 'تم الارسال بنجاح');
        return back();
    }

    public function offers(Request $request)
    {
        $records = Product::CheckActive()->Offer()->where(function ($q) use ($request) {

            if ($request->store_id) {
                $q->Store($request->store_id);
            }

            if ($request->latitude && $request->longitude) {
                $q->whereHas('categories', function ($q) use ($request) {

                    $q->whereHas('store', function ($q) use ($request) {

                        $q->Check()->CheckArea(['latitude' => $request->latitude, 'longitude' => $request->longitude]);
                    });
                });
            }

            if ($request->category_id) {
                $q->whereHas('categories', function ($q) use ($request) {

                    $q->whereHas('store', function ($q) use ($request) {

                        $q->whereHas('categories', function ($q) use ($request) {

                            $q->where('category_id', $request->category_id)->orWhere('parent_id', $request->category_id);
                        });
                    });
                });
            }

            if ($request->name) {
                $q->where('name', 'LIKE', ' % ' . $request->name . ' % ')
                  ->orWhere('description', 'LIKE', ' % ' . $request->name . ' % ');
            }
        })->orderBy('order')->paginate(12);

        return view('front . offers', compact('records'));
    }

    public function jobs(Request $request)
    {
        $jobs = Job::where(function ($query) use ($request) {
            if ($request->input('country_id')) {
                $query->where('country_id', $request->country_id);
            }
            if ($request->input('department_id')) {
                $query->where('department_id', $request->department_id);
            }
        })->isOpen()->paginate(6);
        return view('front.jobs', compact('jobs'));
    }

    public function jobApply($id)
    {
        $job = Job::findOrFail($id);
        return view('front.job-apply', compact('job'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function jobApplySubmit(Request $request, $id)
    {
        $rules =
            [
                'f_name' => 'required',
                'l_name' => 'required',
                'phone'  => 'required',
                'email'  => 'required | email',
                'cv'     => 'required | mimes:png,doc,docx,pdf',
            ];
        $messages = [
            'f_name.required' => 'الاسم الاول مطلوب',
            'l_name.required' => 'الاسم الاخير مطلوب',
            'phone.required'  => ' التليفون مطلوب',
            'email.required'  => ' الايميل مطلوب',
            'email.email'     => ' صيغة الايميل غير صحيحة',
            'cv.required'     => 'السيرة الذاتية مطلوبة',
            'cv.mimes'        => 'صيغة الملف المرفق غير سليمة',
        ];
        $this->validate($request, $rules, $messages);
        $job = Job::findOrFail($id);
        $record = $job->applications()->create($request->all());

        Photo::addOriginalPhoto($request->file('cv'), $record, 'cvs', 'photos');

        flash()->success('تم التقدم للوظيفة بنجاح');
        return back();
    }
}
