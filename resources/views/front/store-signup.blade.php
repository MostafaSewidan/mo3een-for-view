@extends('front.master')
@section('content')
    <!-- Top Navbar -->
    <!-- jobs -->
    <section class="jobs pt-5 pb-5">
        <div class="container">
            <div class="page-head">
                <h3 class="text-primary">انضم إلى شركاء النجاح</h3>
            </div>
            @include('layouts.partials.validation-errors')
            @include('flash::message')
            <section class="wizard">
                <ul class="nav nav-tabs flex-nowrap nav-justified" role="tablist">
                    <li role="presentation" class="nav-item">
                        <a href="#step1" class="nav-link active" data-toggle="tab" aria-controls="step1" role="tab"
                           title="الاسم التجاري">
                            <i class="fas fa-hospital fa-2x"></i>
                        </a>
                    </li>
                    <li role="presentation" class="nav-item">
                        <a href="#step2" class="nav-link disabled" data-toggle="tab" aria-controls="step2" role="tab"
                           title="بيانات المحل">
                            <i class="fas fa-info-circle fa-2x"></i>
                        </a>
                    </li>
                    <li role="presentation" class="nav-item">
                        <a href="#step3" class="nav-link disabled" data-toggle="tab" aria-controls="step3" role="tab"
                           title="التوصيل">
                            <i class="fas fa-ambulance fa-2x"></i>
                        </a>
                    </li>
                </ul>
                {!! Form::open([
                   'action' => 'FrontController@storeSignupSubmit',
                   'method' => 'post',
                   'id' => 'job-apply-form',
                   'files' => true
               ]) !!}
                <div class="tab-content py-5">
                    <div class="tab-pane active" role="tabpanel" id="step1">
                        <h3 class="text-primary">الاسم التجاري</h3>
                        <div class="form-group">
                            <input type="text" name="name" class="form-control" placeholder="اسم المؤسسة">
                        </div>
                        <div class="form-group">
                            <input type="text" name="commercial_register" class="form-control"
                                   placeholder="السجل التجاري">
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                @inject('countries','App\Models\Country')
                                <div class="form-group">
                                    {!! Form::select('countries',$countries->pluck('name','id')->toArray(),null,[
                                        'class' => 'form-control',
                                        'id' => 'countries',
                                        'placeholder' => 'اختر المنطقة'
                                    ]) !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                @inject('cities','App\Models\City')
                                <div class="form-group">
                                    {!! Form::select('city',[],null,[
                                        'class' => 'form-control',
                                        'id' => 'city',
                                        'placeholder' => 'اختر المدينة'
                                    ]) !!}
                                </div>
                            </div>
                            <div class="col-sm-4">
                                @inject('regions','App\Models\Region')
                                <div class="form-group">
                                    {!! Form::select('region_id',[],null,[
                                        'class' => 'form-control',
                                        'id' => 'region',
                                        'placeholder' => 'اختر الحى'
                                    ]) !!}
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-primary bg-primary next-step float-right">
                            التالي
                        </button>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step2">
                        <h3 class="text-primary">بيانات المؤسسة</h3>
                        <div class="categories-tabs">
                            <h4 class="text-primary">اختر التخصصات</h4>
                            <div class="main-category mb-4">
                                <div class="row">
                                    @foreach($services as $service)
                                        <div class="col-sm-3 col-6">
                                            <div class="form-check">
                                                <label>
                                                    <input type="checkbox" name="services[]" value="{{$service->id}}"

                                                    >
                                                    {{$service->name}}
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <hr>
                        </div>
                        <ul class="float-right">
                            <li class="list-inline-item">
                                <button type="button" class="btn btn-outline-primary prev-step">السابق</button>
                            </li>
                            <li class="list-inline-item">
                                <button type="button" class="btn btn-primary bg-primary btn-info-full next-step">
                                    التالي
                                </button>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-pane" role="tabpanel" id="step3">
                        <h3 class="text-primary">السيارات</h3>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="">هل تمتلك سيارات إسعاف؟؟</label>
                                </div>
                                <div class="col-sm-2 col-6">

                                    <label>
                                        <input type="radio" id="have_ambulance" name="have_ambulance"
                                               value="1">نعم
                                        <br>
                                        <input type="radio" id="have_ambulance" name="have_ambulance"
                                               value="0">لا
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="number" step="1" class="form-control" name="cars_num"
                                   placeholder="عدد السيارات">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" name="car_model"
                                   placeholder="أنواع وموديلات السيارات">
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-sm-3">
                                    <label for="">هل تتعامل مع تطبيقات أخرى مماثلة؟؟</label>
                                </div>
                                <div class="col-sm-2 col-6">
                                    <div class="form-check">
                                        <input class="form-check-input" name="other_apps" type="radio" value="1">
                                        <label class="form-check-label" for="defaultCheck1">
                                            نعم
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-2 col-6">
                                    <div class="form-check">
                                        <input class="form-check-input" name="other_apps" type="radio" value="0">
                                        <label class="form-check-label" for="defaultCheck1">
                                            لا
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="accept_conditions" value="1"
                                   id="defaultCheck1">
                            <label class="form-check-label" for="defaultCheck1">
                                أقر أني قرأت الشروط والطلبات المذكورة وأن البيانات المرفقة صحيحة
                            </label>
                        </div>
                        <ul class="float-right">
                            <li class="list-inline-item">
                                <button type="button" class="btn btn-outline-primary prev-step">السابق</button>
                            </li>
                            <li class="list-inline-item">
                                <button type="submit" class="btn btn-primary bg-primary btn-info-full next-step">
                                    أرسل البيانات للإدارة للتسجيل
                                </button>
                            </li>
                        </ul>
                    </div>
                    <div class="clearfix"></div>
                </div>
                {!! Form::close() !!}
            </section>
        </div>
    </section>
    <!-- jobs -->

@endsection
