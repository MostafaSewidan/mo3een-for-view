<?php

namespace App\Http\Middleware;

use Closure;

class AdminActivation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->user()->activation == 0)
        {

            auth()->logout();
            return abort(404);
        }
        return $next($request);
    }
}
