<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class PointsReport extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_id' => optional($this->order)->id,
            'quantity' => $this->quantity,
            'date' => Carbon::parse($this->created_at)->format('d/m/Y')
        ];
    }
}
