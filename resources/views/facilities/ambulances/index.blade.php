@extends('facilities.layouts.main',[
								'page_header'		=> 'سيارات الإسعاف',
								'page_description'	=> 'عرض السيارات',
                                'link' => url('facilities/ambulances')

								])
@section('content')
    <div class="ibox ibox-primary">
        <div class="ibox-title">
            <div class="pull-right">
                <a href="{{url('facilities/ambulances/create')}}" class="btn btn-primary">
                    <i class="fa fa-plus"></i>اضافة سيارة جديد
                </a>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="">
            {!! Form::open([
                'method' => 'GET'
            ]) !!}
            <div class="col-md-3">
                <div class="">
                    <label for="">&nbsp;</label>
                    {!! Form::text('name',old('name'),[
                        'class' => 'form-control',
                        'placeholder' => 'الاسم'
                    ]) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::select('service_id',
                              [
                                  '1' => 'الحالات الطارئة',
                                  '2' =>'الحالات المستقرة',
                                  '5' => 'نقل الجثمان',
                                  '6' => 'كبار السن وذوى الاحتياجات',
                                  '7' => 'أقرب صيدلية',
                              ],\Request::input('service_id'),[
                                  'class' => 'form-control',
                                  'placeholder' => 'كل الخدمات'
                          ]) !!}
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::text('from',old('from'),[
                        'class' => 'form-control datepicker',
                        'placeholder' => 'بداية تاريخ الاضافة',
                                'autocomplete' => 'off',
                    ]) !!}
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::text('to',old('to'),[
                        'class' => 'form-control datepicker',
                        'placeholder' => 'انتهاء تاريخ الاضافة',
                                'autocomplete' => 'off',
                    ]) !!}
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    <button class="btn btn-flat btn-block btn-primary">بحث</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="ibox-content">
            @if(!empty($records) && count($records)>0)
                <div class="table-responsive">
                    <table class="data-table table table-bordered">
                        <thead>
                        <th>كود السيارة</th>
                        <th>اسم السائق</th>
                        <th>التليفون</th>
                        <th>الخدمات</th>
                        <th class="text-center">الحالة</th>
                        <th class="text-center">حالة العمل</th>
                        <th class="text-center">عدد الطلبات</th>
                        <th class="text-center">عرض</th>
                        </thead>
                        <tbody>

                        @foreach($records as $record)
                            <tr id="removable{{$record->id}}">
                                <td>{{$record->id}}</td>
                                <td>{{$record->name}}</td>
                                <td>{{$record->phone}}</td>
                                <td>
                                    @foreach($record->services()->get() as $service)
                                        <span class="label label-primary"
                                              style="display: inline-block;margin-bottom: 5px;">{{$service->name}}</span>
                                    @endforeach
                                </td>

                                <td class="text-center">
                                    @if($record->status=='pending')
                                        <strong class="label label-success">في إنتظار الموافقة</strong>
                                    @elseif($record->status == 'active')
                                        <strong class="label label-warning">
                                            <i class="fa fa-check"></i>مفعل
                                        </strong>
                                    @elseif($record->status == 'deactivate')
                                        <strong class="label label-danger">
                                            <i class="fa fa-close"></i>محظور</strong>
                                    @endif
                                </td>

                                <td class="text-center">
                                    {!! \App\MyHelper\Helper::toggleBooleanView($record , url('facilities/ambulance/toggle-boolean/'.$record->id.'/types/') ,'types' ,'open','close') !!}
                                </td>

                                <td class="text-center">
                                    {{$record->orders()->count()}}
                                </td>
                                <td>
                                    <a href="{{url(route('ambulances.show',$record->id))}}" class="btn btn-info btn-sm">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                                {{--<td class="text-center">--}}
                                {{--<button id="{{$record->id}}" data-token="{{ csrf_token() }}"--}}
                                {{--data-route="{{URL::route('ambulances.destroy',$record->id)}}"--}}
                                {{--type="button" class="destroy btn btn-danger btn-xs"><i--}}
                                {{--class="fa fa-trash-o"></i>--}}
                                {{--</button>--}}
                                {{--</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {!! $records->render() !!}
            @else
                <div>
                    <h3 class="text-info" style="text-align: center">No data to show </h3>
                </div>
            @endif


        </div>
    </div>
@stop

@section('script')
    <script>
        lightbox.option({
            'resizeDuration': 200,
            'wrapAround': true,
            'showImageNumberLabel': false,

        })
    </script>
@stop
