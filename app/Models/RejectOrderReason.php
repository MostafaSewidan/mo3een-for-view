<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RejectOrderReason extends Model
{

    protected $table = 'reject_order_reasons';
    public $timestamps = true;
    protected $fillable = array('name','is_active');

    public function deliveryRejectReasons()
    {
        return $this->hasMany(Order::class);
    }
}