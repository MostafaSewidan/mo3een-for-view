<?php

namespace App\Http\Middleware;


use App\MyHelper\Helper;
use Closure;


class ClientAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $helper = new Helper();
        if(auth('api_client')->user()->status == 'deactivate')
        {
            return $helper->responseJson(0, 'تم حظر هذا المستخدم');

        }elseif(auth('api_client')->user()->status == 'not_confirmed')
        {
            return $helper->responseJson(0, 'لم تم تاكيد الحساب بعد ,الرجاء تاكيد الحساب الخاص بك');
        }
        return $next($request);
    }
}
