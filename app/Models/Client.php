<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class Client extends Authenticatable
{

    use HasApiTokens, Notifiable;

    protected $dates      = ['d_o_b'];
    protected $table      = 'clients';
    public    $timestamps = true;
    protected $fillable   = array(
        'name',
        'username',
        'password',
        'phone',
        'region_id',
        'national_ID',
        'chronic_diseases',
        'Length',
        'the_weight',
        'blood_type_id',
        'insurance_number',
        'notes',
        'accept',
        'gender',
        'd_o_b',
        'nationality',
    );
    protected $hidden     = array('pin_code', 'pin_code_date_expired', 'status', 'password');
    protected $appends     = array('wallet_balance', 'points','age');

    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    public function bloodType()
    {
        return $this->belongsTo(BloodType::class);
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function addresses()
    {
        return $this->morphMany('App\Models\Address', 'addressable');
    }

    public function AuthAccessToken()
    {
        return $this->hasMany('\App\Models\OauthAccessToken', 'user_id');
    }

    public function tokens()
    {
        return $this->morphMany('App\Models\Token', 'tokenable');
    }

    public function contacts()
    {
        return $this->morphMany('App\Models\Contact', 'contactable');
    }

    public function notifications()
    {
        return $this->morphToMany('App\Models\Notification', 'notifiable')->withPivot('is_read');
    }

    public function pointsTransactions()
    {
        return $this->morphMany('App\Models\PointsTransaction', 'pointable');
    }

    public function getPointsAttribute()
    {
        return $this->pointsTransactions()->latest()->first() ? $this->pointsTransactions()->latest()->first()->points_after : 0;
    }

    public function getWalletBalanceAttribute()
    {
        return $this->walletTransactions()->latest()->first() ? $this->walletTransactions()->latest()->first()->balance_after : 0;
    }

    public function getAgeAttribute()
    {
        return $this->d_o_b ? Carbon::now()->diffInYears($this->d_o_b) : 0;
    }

    public function walletTransactions()
    {
        return $this->morphMany('App\Models\WalletTransaction' , 'walletable');
    }

    public function reservations()
    {
        return $this->hasMany('App\Models\Reservation');
    }

    public function credit_cards()
    {
        return $this->hasMany('App\Models\CreditCard');
    }

    public function photo()
    {
        return $this->morphOne(Attachment::class , 'attachmentable');
    }
}
