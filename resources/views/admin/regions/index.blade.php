@extends('admin.layouts.main',[
								'page_header'		=> 'الأحياء',
								'page_description'	=> 'عرض ',
								'link' => url('admin/regions')
								])
@section('content')

    <div class="ibox box-primary">
        <div class="ibox-title">
            <div class="pull-right">


                <a href="{{url('admin/regions/create')}}" class="btn btn-primary">
                    <i class="fa fa-plus"></i> إضافة جديد
                </a>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="row">
            {!! Form::open([
                'method' => 'GET'
            ]) !!}
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::text('name',old('name'),[
                        'class' => 'form-control',
                        'placeholder' => 'الاسم'
                    ]) !!}
                </div>
            </div>
            <div class="col-sm-3">
                @inject('countries','App\Models\Country')
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::select('country',$countries->pluck('name','id')->toArray(),request('country'),[
                        'class' => 'form-control',
                        'id' => 'country',
                        'placeholder' => 'كل المحافظات'
                    ]) !!}
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    @inject('cities' , App\Models\City)
                    {!! Form::select('city', $cities->pluck('name','id')->toArray() ,request('city'),[
                        'class' => 'form-control',
                        'id' => 'city',
                        'placeholder' => 'كل المدن'
                    ]) !!}
                </div>
            </div>


            @push('scripts')
                <script>
                    $('#country').change(function (){

                        var url = '{!! url('admin/country/cities') !!}/' + $('#country').val();

                        $.ajax({
                            url: url,
                            type: 'get',
                            success:function(data){

                                var builtSelectCategory ='<option value="" selected>قم بالاخيتار</option>';
                                $.each(data.data, function(index, item) {
                                    var option = '<option value="'+item.id+'">'+item.name+'</option>';
                                    builtSelectCategory+= option;
                                });

                                $('#city').text('').append(builtSelectCategory);
                            },
                            error: function (data) {

                                $('#city').text('').append('<option value="" selected>لا يوجد مدن تابعة لهذه المحافظة</option>');
                            }
                        });
                    });
                </script>
            @endpush

            <div class="col-md-3">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::text('from',old('from'),[
                        'autocomplete' => 'off',
                        'class' => 'form-control datepicker',
                        'placeholder' => 'بداية تاريخ الاضافة'
                    ]) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    {!! Form::text('to',old('to'),[
                        'autocomplete' => 'off',
                        'class' => 'form-control datepicker',
                        'placeholder' => 'انتهاء تاريخ الاضافة'
                    ]) !!}
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="">&nbsp;</label>
                    <button class="btn btn-primary btn-block" type="submit">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>

        <div class="ibox-content">
            @if(!empty($records) && count($records)>0)
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <th>#</th>
                        <th>الاسم</th>
                        <th class="text-center">تعديل</th>
                        <th class="text-center">حذف</th>
                        </thead>
                        <tbody>
                        @php $count = 1; @endphp
                        @foreach($records as $record)
                            <tr id="removable{{$record->id}}">
                                <td>{{$count}}</td>
                                <td>{{optional($record)->name}}</td>

                                <td class="text-center"><a href="{{url('admin/regions/' . $record->id .'/edit')}}" class="btn btn-xs btn-success"><i class="fa fa-edit"></i></a></td>
                                <td class="text-center">
                                    <button
                                            id="{{$record->id}}"
                                            data-token="{{ csrf_token() }}"
                                            data-route="{{url('admin/regions/'.$record->id)}}"
                                            type="button"
                                            class="destroy btn btn-danger btn-xs">
                                        <i class="fa fa-trash-o"></i>
                                    </button>
                                </td>
                            </tr>
                            @php $count ++; @endphp
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {!! $records->render() !!}
            @else
                <div>
                    <h3 class="text-info" style="text-align: center"> لا توجد بيانات للعرض </h3>
                </div>
            @endif


        </div>
    </div>
@stop

@section('script')
    <script>
        lightbox.option({
            'resizeDuration': 200,
            'wrapAround': true,
            'showImageNumberLabel':false,

        })
    </script>
@stop
