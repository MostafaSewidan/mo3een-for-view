@extends('facilities.layouts.main',[
                                'page_header'       => 'التقييمات ',
                                'page_description'  => '  ',
                                'link' => url('facilities/reviews')
                                ])

@section('content')
    <div class="ibox-content">

                    <div class="ibox-title">
                        <div class="ibox-tools">
                            <strong class="label label-success"
                                    style="font-size: 1pc;">{{$rateData['provider_rate']}}</strong>

                        </div>
                        <h5>اجمالي التقييم</h5>
                    </div>
                    <div class="ibox-content">
                        <h3> {!! \App\MyHelper\Helper::ratStars($rateData['provider_rate'] ,'font-size: 2.5pc;',' ')!!} </h3>
                        <ul class="stat-list" style="width: 80%;">
                            @php $colors = ['#FF0000','#FF6363','#FEDD12','#4BEF8C','#00A542'] @endphp
                            @for($i = 1; $i <= 5; $i++)
                                <li>
                                    <div class="stat-percent" style="margin-left: 7px;margin-top: -6px;">{{$i}}</div>
                                    <div class="progress progress-mini">
                                        <div style="width: 100%;background-color: #707070;" class="progress-bar">
                                        <span style="width: {{$rateData['counts'][$i]}}%; background-color: {{$colors[$i - 1]}}"
                                              class="progress-bar"></span>
                                        </div>
                                    </div>
                                </li>
                            @endfor
                        </ul>
                    </div>



        <div class="box-body">

            @include('flash::message')
            @if(count($records))
                <div class="table-responsive">
                    <table class="data-table table table-bordered">
                        <thead>
                        <th>#</th>
                        <th class="text-center">العميل</th>
                        <th class="text-center">الرحلة</th>
                        <th class="text-center">التقييم</th>
                        <th class="text-center">التعليق</th>
                        </thead>
                        <tbody>

                        @foreach($records as $record)
                            <tr id="removable{{$record->id}}">
                                <td>{{$loop->iteration}}</td>
                                <td class="text-center">
                                    #{{optional($record->client)->id}} <br>
                                    {{optional($record->client)->phone}} <br>
                                    {{optional($record->client)->name}}
                                </td>
                                <td class="text-center">

                                    <a target="_blank" href="{{url('facilities/orders?order_id='. optional($record->reviewable)->id)}}">#
                                        {{optional($record->reviewable)->id}}
                                    </a>
                                </td>
                                <td class="text-center">  {!! \App\MyHelper\Helper::ratStars($record->rate) !!} </td>
                                <td class="text-center">{{$record->comment}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

        </div>
        <div class="text-center">
            {!! $records->render() !!}
        </div>
        @else
            <div>
                <h3 class="text-info" style="text-align: center"> لا توجد بيانات للعرض </h3>
            </div>
        @endif
    </div>
    </div>
@endsection
