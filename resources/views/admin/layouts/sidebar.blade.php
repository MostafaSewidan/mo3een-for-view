
<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">

                    {{--// profile image and display name of user--}}

                    <span>
                            <img alt="image"  style="max-width: 183px;"
                                 src="{{asset('photos/logo-png.png')}}"/>
                             </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="block m-t-xs">
                                    <strong class="font-bold">{{Auth::user()->name}}</strong>
                                    {{--@foreach(auth()->user()->roles as $role)--}}
                                    {{--<span class="label label-success">{{$role->display_name}}</span>--}}
                                    {{--@endforeach--}}
                                </span>
                            </span>
                    </a>


                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        {{--<!-- <li><a href="">حسابي</a></li>--}}
                        {{--<li class="divider"></li> -->--}}

                        <li>

                            <a href="{{url('facilities/reset-password')}}" > <i class="fa fa-key" style="margin-left: 5px;"></i> تسجيل الخروج  </a>

                        </li>
                        <li>
                            <script type="">
                                function submitSidebarSignout() {
                                    document.getElementById('signoutSidebar').submit();

                                }
                            </script>
                            {!! Form::open(['method' => 'post', 'url' => url('logout'),'id'=>'signoutForm','id'=>'signoutSidebar']) !!}
                            {!! Form::hidden('guard','admin') !!}

                            {!! Form::close() !!}
                            <a href="#" onclick="submitSidebarSignout()"> <i class="fa fa-sign-out" style="margin-left: 5px;"></i> تسجيل الخروج  </a>

                        </li>
                    </ul>
                </div>

                <div class="logo-element">

                    <img src="{{asset('photos/logo_title.png')}}" style="margin-top: 20px; margin-bottom:auto;" height="20"
                         alt="logo">
                </div>
            </li>
            {{--Home--}}
            <li>
                <a href="{{url('facilities/home')}}">
                    <i class="fa fa-home"></i>
                    <span class="nav-label">الرئيسية</span>
                </a>
            </li>

            <li>
                <a href="{{url('facilities/users')}}">
                    <i class="fa fa-users" aria-hidden="true"></i>
                    <span class="nav-label"> المستخدمين</span>
                </a>
            </li>

            {{--Orders--}}
            <li>
                <a href="{{url('facilities/ambulance/stats')}}">
                    <i class="fa fa-ambulance" aria-hidden="true"></i>
                    <span class="nav-label">سيارات الإسعاف</span>
                </a>
            </li>

            {{--Orders--}}
            <li>
                <a href="#"><i class="fa fa-shopping-cart"></i> <span class="nav-label">الطلبات</span><span
                            class="fa arrow"></span></a>
                <ul class="nav nav-second-level collapse">
                    <li><a href="{{url('facilities/orders')}}">كل الطلبات</a></li>
                    <li><a href="{{url('facilities/events')}}">طلبات الأعياد والمناسبات</a></li>
                </ul>
            </li>

            <li>
                <a href="{{url('facilities/reviews')}}">
                    <i class="fa fa-star" aria-hidden="true"></i>
                    <span class="nav-label">التقييمات</span>
                </a>
            </li>

            <li>
                <a href="{{url('facilities/roles')}}">
                    <i class="fa fa-user-circle" aria-hidden="true"></i>
                    <span class="nav-label">رتب المستخدمين</span>
                </a>
            </li>

        </ul>

    </div>
</nav>