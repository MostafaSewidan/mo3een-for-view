<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $table      = 'payment_methods';
    public    $timestamps = true;
    protected $fillable   = array('name');
    protected $appends   = array('name_text');

    public function photo()
    {
        return $this->morphOne('App\Models\Attachment', 'attachmentable');
    }

    public function getNameTextAttribute()
    {
        $type = '';

        switch ($this->name){
            case 'wallet':
                $type = 'محفظتك';
                break;
            case 'mada_card':
                $type = 'بطافات مدى';
                break;
            case 'cache':
                $type = 'الدفع النقدى';
                break;
            case 'online_payment':
                $type = 'الدفع الالكترونى';
                break;
        }

        return $type;
    }
}
