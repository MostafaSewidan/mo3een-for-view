@extends('admin.layouts.main',[
                                'page_header'       => 'أرقام الطوارئ ',
                                'page_description'  => " عرض  ",
                                 'link' => url('admin/emergencies/')
                                ])
@section('content')
    @include('admin.layouts.partials.validation-errors')
    @include('flash::message')


    <div class="ibox-title">
        {!! Form::open([
               'method' => 'get'
               ]) !!}
        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::text('name',request()->input('name'),[
                    'placeholder' => 'العنوان',
                    'class' => 'form-control'
                    ]) !!}
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    {!! Form::text('number',request()->input('number'),[
                    'placeholder' => 'رقم الهاتف',
                    'class' => 'form-control'
                    ]) !!}
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
        {!! Form::close() !!}
    </div>
    <br>
    <br>
    <br>

    <div class="row">

        <div class="col-md-12">
            <div class="ibox-title" style="margin-bottom: 15px;">
                أرقام الطوارئ
                <a href="#" class="btn btn-xs btn-success pull-left"
                   data-toggle="modal" data-target="#addProduct">
                    <i class="fa fa-plus-circle"></i>
                </a>
                <div class="modal fade text-right" id="addProduct" tabindex="-1" role="dialog"
                     aria-labelledby="add_additionLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">إضافة منتج</h4>
                            </div>
                            {!! Form::open([
                                    'url' => url('admin/emergencies'),
                                    'method' => 'post',
                                    'files' => true
                                ]) !!}
                            <div class="modal-body">

                                <div class="form-group">
                                    {!! Form::text('title',null,[
                                        'class' => 'form-control',
                                        'placeholder' => 'العنوان'
                                    ]) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::number('number',null,[
                                        'class' => 'form-control',
                                        'placeholder' => 'رقم الهاتف',
                                        'step' => '0.01'
                                    ]) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::number('order',null,[
                                        'class' => 'form-control',
                                        'placeholder' => 'الترتيب',
                                        'step' => '0.01'
                                    ]) !!}
                                </div>

                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">إضافة</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق
                                </button>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>

            <div class="ibox">
                <div class="ibox-title">
                    <h5>المنتجات</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link" href="#">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                    </div>
                    {{--<i class="fa fa-angle-down pull-left" style="font-size: 17px"></i>--}}
                </div>
                <div class="ibox-content" style="padding: 0">
                    <div class="table-responsive">
                        <table class="table">
                            @foreach($records as $record)
                                <tr id="removable{{$record->id}}">
                                    <td>
                                        <span class="label label-warning">#{{$record->id}}</span>
                                        {{$record->title}}
                                    </td>
                                    <td class="text-left" style="min-width: 91px;">

                                        {!! \App\MyHelper\Helper::toggleBooleanView($record , url('admin/emergencies/toggle-boolean/'.$record->id.'/is_active/')) !!}

                                        <a href="#"
                                           class="btn  btn-info btn-xs"
                                           data-toggle="modal" data-target="#product{{$record->id}}">
                                            <i class="fa fa-info-circle"></i></a>
                                        <div class="modal fade text-right" id="product{{$record->id}}"
                                             tabindex="-1" role="dialog"
                                             aria-labelledby="categoryRequestsLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span></button>


                                                        <span class="label label-warning">#{{$record->id}}</span>
                                                    </div>
                                                    <div class="modal-body">
                                                        <div class="table-responsive">
                                                            <table class="table">
                                                                <tr>
                                                                    <th colspan="2" class="text-center">البيانات
                                                                    </th>
                                                                </tr>

                                                                <tr>
                                                                    <th class="text-center">العنوان</th>
                                                                    <td class="text-center">{{$record->title}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center">رقم الهاتف</th>
                                                                    <td class="text-center">{{$record->number}}</td>
                                                                </tr>
                                                                <tr>
                                                                    <th class="text-center">ترتريب الظهور</th>
                                                                    <td class="text-center">{{$record->order}}</td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <a href="#" class="btn btn-success btn-xs"
                                           data-toggle="modal" data-target="#editProduct{{$record->id}}"><i
                                                    class="fa fa-edit"></i></a>
                                        <div class="modal fade text-right" id="editProduct{{$record->id}}"
                                             tabindex="-1" role="dialog"
                                             aria-labelledby="editCategoryLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                            <span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="myModalLabel">تعديل منتج</h4>
                                                    </div>
                                                    {!! Form::model($record,[
                                                            'url' => url('admin/emergencies/'.$record->id),
                                                            'method'=>'PUT',
                                                            'files' => true
                                                        ]) !!}

                                                    <div class="modal-body">
                                                        <div class="form-group">
                                                            {!! Form::text('title',null,[
                                                                'class' => 'form-control',
                                                                'placeholder' => 'العنوان'
                                                            ]) !!}
                                                        </div>
                                                        <div class="form-group">
                                                            {!! Form::number('number',null,[
                                                                'class' => 'form-control',
                                                                'placeholder' => 'رقم الهاتف',
                                                                'step' => '0.01'
                                                            ]) !!}
                                                        </div>
                                                        <div class="form-group">
                                                            {!! Form::number('order',null,[
                                                                'class' => 'form-control',
                                                                'placeholder' => 'الترتيب',
                                                                'step' => '0.01'
                                                            ]) !!}
                                                        </div>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="submit" class="btn btn-primary">تعديل</button>
                                                        <button type="button" class="btn btn-default"
                                                                data-dismiss="modal">إغلاق
                                                        </button>
                                                    </div>
                                                    {!! Form::close() !!}
                                                </div>
                                            </div>
                                        </div>
                                        <button id="{{$record->id}}" data-token="{{ csrf_token() }}"
                                                data-route="{{URL::route('emergencies.destroy',$record->id)}}"
                                                type="button" class="destroy btn btn-danger btn-xs"><i
                                                    class="fa fa-trash-o"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>

        </div>

        <div class="text-center">
            {!! $records->render() !!}
        </div>
    </div>

@endsection