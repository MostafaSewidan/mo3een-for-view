<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Slider extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'        => $this->id,
            'is_active' => $this->is_active,
            'order'     => $this->order,
            'photo'     => $this->attachment()->count() ? asset(optional($this->attachment)->path) : null,
        ];
    }
}
