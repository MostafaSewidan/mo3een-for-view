<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{

    protected $table      = 'sliders';
    public    $timestamps = true;
    protected $fillable   = array('is_active', 'order');


    public function attachment()
    {
        return $this->morphOne('App\Models\Attachment', 'attachmentable');
    }
}
