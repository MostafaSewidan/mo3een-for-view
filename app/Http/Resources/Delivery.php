<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Delivery extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'              => $this->id,
            'name'            => $this->name,
            'phone'           => $this->phone,
            'd_o_b'           => $this->d_o_b,
            'gender'          => $this->gender,
            'status'          => $this->status,
            'case'            => $this->case,
            'rate'            => $this->rate,
            'distance'        => number_format($this->distance, 2). ' km',
            'latitude'        => $this->latitude,
            'longitude'        => $this->longitude,
            'transport_photo' => $this->transport_photo != null ? asset($this->transport_photo) : null,
            'photo'           => $this->photo != null ? asset($this->photo) : null,
            'region'          => $this->region
        ];
    }
}
