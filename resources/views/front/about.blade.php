@extends('front.master')
@section('content')
    <!-- Top Navbar -->
    <!-- main carousel -->
    <section class="carousel-wrapper">
        <div class="owl-carousel main-carousel">
            <div>
                <img src="{{asset('front/images/mo3een-slider.png')}}" alt="Sonqr slider 1">
            </div>
            <div>
                <img src="{{asset('front/images/mo3een-slider.png')}}" alt="Sonqr slider 2">
            </div>
            <div>
                <img src="{{asset('front/images/mo3een-slider.png')}}" alt="Sonqr slider 3">
            </div>
        </div>
        <div class="download-apps">
            <h2 class="text-center mb-5">حمل التطبيق</h2>
            <div class="row">
                <div class="col-sm-6 text-center">
                    <a href="#">
                        <img src="{{asset('front/images/google-play.png')}}" alt="">
                    </a>
                </div>
                <div class="col-sm-6 text-center">
                    <a href="#">
                        <img src="{{asset('front/images/app-store.png')}}" alt="">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- main carousel -->

    <!-- about us -->
    <section class="about-us pt-5 pb-5">
        <div class="container">
            <h2 class="text-center text-primary mt-4 mb-4">عن مُعين</h2>
            <div class="about-content">
                <p>معين شركة متخصصة في التوصيل .. لديها فريق على أعلى مستوى من الإحترافية والمهنية لإيجاد الطرق السهلة
                    والبسيطة التي توفر بشكل غير تقليدي الاحتياجات اليومية بطريقة سهلة وآمنة وسريعة ، كما توفر خدمة عملاء
                    على
                    مدار الساعة لتلبية احتياجاتكم وتوفير أفضل خدمة ممكنة.</p>
            </div>
            <div class="row mt-5">
                <div class="col-sm-4 text-center">
                    <h3 class="text-center text-primary mt-4 mb-4">الرؤية</h3>
                    <p>توصيل الاحتياجات الطبية بطريقة سهلة وسريعة ومضمونة</p>
                </div>
                <div class="col-sm-4 text-center">
                    <h3 class="text-center text-primary mt-4 mb-4">الرسالة</h3>
                    <p>توصيل الاحتياجات الطبية بطريقة سهلة وسريعة ومضمونة</p>
                </div>
                <div class="col-sm-4 text-center">
                    <h3 class="text-center text-primary mt-4 mb-4">الأهداف</h3>
                    <p>توصيل الاحتياجات الطبية بطريقة سهلة وسريعة ومضمونة</p>
                </div>
            </div>
        </div>
    </section>
    <!-- about us -->

@endsection
