
@inject('countries','App\Models\Country')
@inject('cities' , App\Models\City)

{!! \App\MyHelper\Field::text('name' , 'الاسم ' ) !!}
{!! \App\MyHelper\Field::select('country' , 'المنطقة ' , $countries->pluck('name','id')->toArray(),request('country')) !!}
{!! \App\MyHelper\Field::select('city_id' , 'كل المدن ' , $cities->pluck('name','id')->toArray()) !!}




@push('scripts')
    <script>
        $('#country').change(function (){

            var url = '{!! url('admin/country/cities') !!}/' + $('#country').val();

            $.ajax({
                url: url,
                type: 'get',
                success:function(data){

                    var builtSelectCategory ='<option value="" selected>قم بالاخيتار</option>';
                    $.each(data.data, function(index, item) {
                        var option = '<option value="'+item.id+'">'+item.name+'</option>';
                        builtSelectCategory+= option;
                    });

                    $('#city_id').text('').append(builtSelectCategory);
                },
                error: function (data) {

                    $('#city_id').text('').append('<option value="" selected>لا يوجد مدن تابعة لهذه المنطقة</option>');
                }
            });
        });
    </script>
@endpush