<?php

namespace App\Http\Controllers\Facility;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Governorate;
use App\Models\Review;
use App\MyHelper\Helper;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    protected $model;
    protected $helper;
    protected $guard = 'facilities';
    protected $url = 'facilities/reviews';
    protected $viewsDomain = 'facilities.reviews.';

    public function __construct()
    {
        $this->guard = 'facilities';
        $this->helper = new Helper();
    }

    /**
     * @param $view
     * @param array $params
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $facility = auth($this->guard)->user()->hospital;

        $records = $facility->reviews()->where(function ($query) use ($request) {
        })->paginate(10);

        $totalCount = $facility->reviews()->count();
        $rateData = [
            "status" => 1,
            "massage" => "SUCCESS",
            'provider_rate' => number_format($facility->reviews()->avg('rate'), 1),
            'total_count_reviews' => $totalCount,
            'counts' => (new Helper())->ratePresent($facility, $facility->reviews()->count())
        ];

        return $this->view('index', compact('records','rateData'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy($id)
    {
        $record = $this->model->find($id);
        //dd($record);
        if (!$record) {
            return response()->json([
                'status' => 0,
                'message' => 'تعذر الحصول على البيانات'
            ]);
        }

        if ($record->cities()->count()) {
            return response()->json([
                'status' => 0,
                'message' => 'يوجد مدن مرتبطة بهذه المحافظة',
            ]);
        }
        $record->delete();
        return response()->json([
            'status' => 1,
            'message' => 'تم الحذف بنجاح',
            'id' => $id
        ]);
    }
}
