<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NearestMedicalFacility extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id'        => 0,
            'title'     => 'أقرب منشأة طبية',
            'latitude'  => null,
            'longitude' => null,
            'is_saved'  => 0,
        ];
    }
}
