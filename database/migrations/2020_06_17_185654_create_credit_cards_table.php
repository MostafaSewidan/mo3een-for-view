<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCreditCardsTable extends Migration {

	public function up()
	{
		Schema::create('credit_cards', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('client_id');
			$table->string('name');
			$table->string('number');
			$table->string('cvv_code');
			$table->string('expired_year');
			$table->string('expired_month');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('credit_cards');
	}
}