@extends('facilities.layouts.main',[
								'page_header'		=> 'سيارات الإسعاف',
								'page_description'	=> 'عرض السيارات',
                                'link' => url('facilities/ambulances')

								])
@section('content')
    @push('styles')
        {{-- ChartStyle --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>
    @endpush
    <div class="ibox-content">
        <div class="box-body">
            <div class="box-header">
                <div class="row">
                    <div class="col-sm-3">
                        {!! Form::open([
                       'method' => 'get'
                       ]) !!}
                        <div class="form-group">
                            <label for="">اختر المنطقة</label>
                            {!! Form::select('country_id',$countries,request()->input('country_id'),[
                            'placeholder' => 'كل المناطق',
                            'class' => 'form-control',
                            'onchange' => 'submit()'
                            ]) !!}
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-sm-9">
                        <ul class="nav nav-pills navbar-left">
                            @foreach($services as $service)
                                <li role="presentation" class="bg-gray" style="margin-bottom: 10px;min-width:200px;">
                                    <a href="{{url('facilities/ambulances?service_id='.$service->id.'&country='.request()->input('country_id'))}}">{{$service->name}}
                                        <span class="mr-3 badge badge-info">{{$service->working_ambulances_count}}</span></a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <br>
                <div class="row">
                    @foreach($charts as $chart)
                        <div class="col-sm-4">
                            @push('scripts')
                                {!! $chart->script() !!}
                            @endpush
                            {{$chart->container()}}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
