<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    //

    protected $fillable = ['title', 'exp', 'education', 'country_id', 'department_id', 'is_open'];

    public function country()
    {
        return $this->belongsTo(Country::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }

    public function applications()
    {
        return $this->hasMany(JobApplication::class);
    }

    public function scopeIsOpen($query)
    {
        $query->whereIsOpen(1);
    }
}
