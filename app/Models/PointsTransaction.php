<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PointsTransaction extends Model 
{

    protected $table = 'points_transactions';
    public $timestamps = true;
    protected $fillable = array('pointable_type','pointable_id', 'order_id', 'points_before', 'quantity', 'points_after', 'factor', 'type');

    public function pointable()
    {
        return $this->morphTo();
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

}