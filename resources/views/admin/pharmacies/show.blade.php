@extends('admin.layouts.main',[
								'page_header'		=> 'الصيدليات',
								'page_description'	=> 'عرض ',
								'link' => url('admin/pharmacies/' . $record->id)
								])
@section('content')
    @include('layouts.partials.validation-errors')
    @include('flash::message')
    @push('styles')
        <style>
            @media print {
                a[href]:after {
                    content: none !important;
                }
            }
        </style>
    @endpush
    <button type="button" class="btn btn-sm btn-success pull-left hidden-print" onclick="window.print()"><i
                class="fa fa-print"></i></button><br>

    <section id="printArea">
        <div class="row m-b-lg m-t-lg">
            <div class="col-md-12">

                <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2>Profile</h2>
                    </div>
                    <div class="col-lg-12">
                        <div class="ibox-content no-padding border-left-right">
                            <img alt="image" class="img-fluid" src="{{asset('inspina/img/p3.jpg')}}" style="width: 100%; max-height: 8%">
                        </div>
                    </div>
                </div>

                <div class="profile-info">

                    <div class="">
                        <div class="profile-image">
                            <img src="{{asset($record->photo ? $record->photo : 'inspina/img/p3.jpg')}}"
                                 class="rounded-circle circle-border m-b-md" alt="profile">
                        </div>
                        <div>
{{--                            <h2 class="no-margins">--}}
{{--                                {{$record->name}}--}}
{{--                            </h2>--}}
{{--                            <h4>{{$record->owner_name}}</h4>--}}
{{--                            <h4>{{$record->phone}}</h4>--}}
{{--                            <h4>{{$record->category_text}}</h4>--}}
{{--                            <h4>{{$record->category_name}}</h4>--}}
{{--                            <h4>--}}
{{--                                @if($record->address)--}}
{{--                                    <a href="{{url('http://maps.google.com/?q='.$record->address->latitude.','.$record->address->longitude)}}"--}}
{{--                                       target="_blank" class="btn btn-danger">--}}
{{--                                        <i class="fa fa-map-marker"></i>--}}
{{--                                    </a>--}}
{{--                                @endif--}}
{{--                            </h4>--}}
{{--                            <small>--}}
{{--                                @if($record->region)--}}
{{--                                    @if($record->region->city)--}}
{{--                                        {{optional($record->region->city->governorate)->name}}--}}
{{--                                        ---}}
{{--                                    @endif--}}
{{--                                    {{optional($record->region->city)->name}}--}}
{{--                                    ---}}
{{--                                    {{optional($record->region)->name}}--}}
{{--                                @endif--}}
{{--                            </small>--}}
{{--                            <small>--}}
{{--                                {{$record->full_address}}--}}
{{--                            </small>--}}
                        </div>
                    </div>
                </div>
            </div>
{{--            <div class="col-md-6">--}}
{{--                <table class="table m-b-xs">--}}
{{--                    <tbody>--}}
{{--                    <tr>--}}
{{--                        <td>--}}
{{--                            اسم المستخدم: <strong>{{$record->user_name}}</strong>--}}
{{--                        </td>--}}
{{--                        <td>--}}
{{--                            واتساب: <strong>{{$record->whatsapp}}</strong>--}}
{{--                        </td>--}}
{{--                    </tr>--}}
{{--                    <tr>--}}
{{--                        <td>--}}
{{--                            السجل التجاري: <strong>{{$record->commercial_registry}}</strong>--}}
{{--                        </td>--}}
{{--                        <td>--}}
{{--                            عدد الفروع: <strong>{{$record->branches_num}}</strong>--}}
{{--                        </td>--}}
{{--                    </tr>--}}
{{--                    <tr>--}}
{{--                        <td>--}}
{{--                            صفحة الفيس بوك :--}}
{{--                            <strong><a href="{{$record->fb_url ?? '#'}}"--}}
{{--                                       target="{{$record->fb_url ? '_blank' : '_self'}}">{{$record->name}}</a></strong>--}}
{{--                        </td>--}}
{{--                        <td>--}}
{{--                            طريقة التقديم: <strong>{{$record->signup_platform_text}}</strong>--}}
{{--                        </td>--}}
{{--                    </tr>--}}
{{--                    <tr>--}}
{{--                        <td>--}}
{{--                            خدمة التوصيل: <strong>@if($record->have_delivery) نعم @else لا @endif</strong>--}}
{{--                        </td>--}}
{{--                        <td>--}}
{{--                            متعاقد مع تطبيقات أخري: <strong>@if($record->other_app_contracted) نعم @else--}}
{{--                                    لا @endif</strong>--}}
{{--                        </td>--}}
{{--                    </tr>--}}
{{--                    </tbody>--}}
{{--                </table>--}}
{{--            </div>--}}
        </div>
        <div class="ibox ibox-primary">
            <div class="ibox-content">
                <div class="row">
{{--                    <div class="col-sm-4" style="margin-bottom: 15px">--}}
{{--                        <a href="{{url(route('stores.edit',$record->id))}}" class="btn btn-success btn-block btn-lg p-5">تحديث البيانات وتعديلها</a>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-4"  style="margin-bottom: 15px">--}}
{{--                        <a href="{{url(route('stores.products',$record->id))}}" class="btn btn-success btn-block btn-lg p-5">منتجات المحل والإضافات</a>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-4"  style="margin-bottom: 15px">--}}
{{--                        <a href="{{url(route('stores.offers',$record->id))}}" class="btn btn-success btn-block btn-lg p-5">العروض والخصومات</a>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-4"  style="margin-bottom: 15px">--}}
{{--                        <a href="{{url(route('orders.index')).'?store_id='.$record->id}}" class="btn btn-success btn-block btn-lg p-5">الطلبات</a>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-4"  style="margin-bottom: 15px">--}}
{{--                        <a href="#" class="btn btn-success btn-block btn-lg p-5">التقارير المالية</a>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-4"  style="margin-bottom: 15px">--}}
{{--                        <a href="#" class="btn btn-success btn-block btn-lg p-5">التقارير</a>--}}
{{--                    </div>--}}
{{--                    <div class="col-sm-4"  style="margin-bottom: 15px">--}}
{{--                        <a href="{{url(route('stores.working-times',$record->id))}}" class="btn btn-success btn-block btn-lg p-5">مواعيد العمل</a>--}}
{{--                    </div>--}}
                </div>

            </div>
        </div>
    </section>

@endsection