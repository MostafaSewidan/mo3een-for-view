<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Api\ParentApi;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\CountrySelect;
use App\Http\Resources\DeliveryCollection;
use App\Http\Resources\EmergencyNumberCollection;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\PostCollection;
use App\Http\Resources\ProductCollection;
use App\Http\Resources\Review;
use App\Http\Resources\ServiceCollection;
use App\Http\Resources\Slider;
use App\Http\Resources\SliderCollection;
use App\Http\Resources\StoreCategoryCollection;
use App\Http\Resources\PharmacyCollection;
use App\Models\Address;
use App\Models\BloodType;
use App\Models\CarType;
use App\Models\Category;
use App\Models\CharitableActor;
use App\Models\City;
use App\Models\Client;
use App\Models\ContactReason;
use App\Models\Country;
use App\Models\Delivery;
use App\Models\EmergencyNumber;
use App\Models\Governorate;
use App\Models\Hospital;
use App\Models\Order;
use App\Models\PaymentMethod;
use App\Models\Pharmacy;
use App\Models\Post;
use App\Models\Product;
use App\Models\Region;
use App\Models\Reservation;
use App\Models\Service;
use App\Models\Setting;
use App\Models\Store;
use App\MyHelper\Helper;
use App\MyHelper\Photo;
use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class HomeController extends ParentApi
{

    public function __construct()
    {
        $this->helper = new Helper();
        $this->guard = 'api_client';
        $this->model = new Client();
    }

    public function settings()
    {
        return $this->helper->responseJson(1, 'SUCCESS', new \App\Http\Resources\Setting(''));
    }

    public function categories(Request $request)
    {
        $records = Category::HomeCategories()->ParentCategory()->where(function ($q) use ($request) {

            if ($request->category_id) {
                $q->where('id', $request->category_id);
            }

            if ($request->name) {
                $q->where('name', 'LIKE', '%' . $request->name . '%');
            }

        })->orderBy('order')->get();

        return $this->helper->responseJson(1, 'SUCCESS', new CategoryCollection($records));

    }


    public function home(Request $request)
    {
        $records = Service::where(function ($q) use ($request) {

        })->orderBy('level')->get();
        $sliders = \App\Models\Slider::where('is_active', 1)
            ->orderBy('order')->get();
        return (new ServiceCollection($records))->additional([
            'sliders' => new SliderCollection($sliders),
            "status" => 1,
            "massage" => "SUCCESS",
        ]);

    }

    public function countries(Request $request)
    {
        $records = Country::where(function ($q) use ($request) {
            if ($request->country_id) {
                $q->where('id', $request->country_id);
            }
        })->get();

        return (\App\Http\Resources\Country::collection($records))->additional([
            "status" => 1,
            "massage" => "SUCCESS",
        ]);
    }

    public function bloodTypes()
    {
        $bloodTypes = BloodType::all();
        return $this->helper->responseJson(1, 'success', $bloodTypes);
    }

    public function profileSelects(Request $request)
    {
        $countries = Country::where(function ($q) use ($request) {
            if ($request->country_id) {
                $q->where('id', $request->country_id);
            }
        })->get();

        $bloodTypes = BloodType::all();

        return $this->helper->responseJson(1, 'success', [
            'blood_types' => $bloodTypes,
            'countries' => CountrySelect::collection($countries),
        ]);
    }

    public function carTypes()
    {
        $records = CarType::where('is_active' , 1)->orderBy('level')->get();

        return $this->helper->responseJson(1, 'success', \App\Http\Resources\CarType::collection($records));
    }

    public function paymentMethods()
    {
        $paymentMethods = PaymentMethod::all();
        return $this->helper->responseJson(1, 'success', $paymentMethods);
    }


    public function posts()
    {
        $posts = Post::where('is_active', 1)
            ->orderBy('order')
            ->paginate(10);
        return (new PostCollection($posts))->additional([
            "status" => 1,
            "massage" => "SUCCESS",
        ]);
    }

    public function myTours(Request $request)
    {
        $user = auth('api_client')->user();

        $orders = $user->orders()->where(function ($q) use ($request) {

            if ($request->status) {
                $q->where('status', $request->status);
            }

            if ($request->order_id) {
                $q->where('id', $request->order_id);
            }

        })->latest('asked_at')->paginate(15);

        return (new OrderCollection($orders))->additional([
            "status" => 1,
            "massage" => "SUCCESS",
        ]);
    }

    public function emergencyNumbers()
    {
        $records = EmergencyNumber::where('is_active', 1)
            ->orderBy('order')
            ->paginate(10);
        return (new EmergencyNumberCollection($records))->additional([
            "status" => 1,
            "massage" => "SUCCESS",
        ]);
    }

    public function cities(Request $request)
    {
        $records = City::where(function ($q) use ($request) {
            if ($request->city_id) {
                $q->where('id', $request->city_id);
            }

            if ($request->country_id) {
                $q->where('country_id', $request->country_id);
            }
        })->get();

        return (\App\Http\Resources\City::collection($records))->additional([
            "status" => 1,
            "massage" => "SUCCESS",
        ]);
    }

    public function regions(Request $request)
    {
        $records = Region::where(function ($q) use ($request) {
            if ($request->city_id) {
                $q->where('city_id', $request->city_id);
            }

            if ($request->region_id) {
                $q->where('id', $request->region_id);
            }

        })->get();

        return (\App\Http\Resources\Region::collection($records))->additional([
            "status" => 1,
            "massage" => "SUCCESS",
        ]);
    }


    public function pharmacies(Request $request)
    {
        $rules =
            [
                'lat' => 'required',
                'lon' => 'required',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $distance = Setting::where(['key' => 'pharmacy_filter_area'])->first() ? Setting::where(['key' => 'pharmacy_filter_area'])->first()->value : 6;

        $records = Pharmacy::select(DB::raw('*, ( 6367 * acos( cos( radians(' . $request->lat . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $request->lon . ') ) + sin( radians(' . $request->lat . ') ) * sin( radians( latitude ) ) ) ) AS distance'))
            ->having("distance", "<", $distance)
            ->orderBy("distance")
            ->paginate(10);

        return (new PharmacyCollection($records))->additional([
            "status" => 1,
            "massage" => "SUCCESS",
        ]);
    }

    public function hospitals(Request $request)
    {
        $rules =
            [
                'lat' => 'required',
                'lon' => 'required',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $distance = Setting::where(['key' => 'hospital_filter_area'])->first() ? Setting::where(['key' => 'hospital_filter_area'])->first()->value : 6;

        $records = Hospital::CheckActive()->select(DB::raw('*, ( 6367 * acos( cos( radians(' . $request->lat . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $request->lon . ') ) + sin( radians(' . $request->lat . ') ) * sin( radians( latitude ) ) ) ) AS distance'))
            ->having("distance", "<", $distance)
            ->orderBy("distance")
            ->paginate(10);

        return (new PharmacyCollection($records))->additional([
            "status" => 1,
            "massage" => "SUCCESS",
        ]);
    }


    public function deliveries(Request $request)
    {
        $rules =
            [
                'lat' => 'required',
                'lon' => 'required',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $distance = $this->helper->settingValue('delivery_filter_area', 6);

        $records = Delivery::select(DB::raw($this->helper->getLocation($request->lat, $request->lon)))
            ->having("distance", "<", $distance)
            ->orderBy("distance")
            ->paginate($request->input('nearest') && $request->nearest == 1 ? 1 : 10);


        return (new DeliveryCollection($records))->additional([
            "status" => 1,
            "massage" => "SUCCESS",
        ]);
    }

    public function offers(Request $request)
    {
        $records = Product::Offer()->where(function ($q) use ($request) {

            if ($request->category_id && !$request->input('sub_category_id')) {
                $q->whereHas('categories', function ($q) use ($request) {
                    $q->where(['category_id' => $request->category_id, 'parent_id' => null]);

                });
            }

            if ($request->category_id && $request->input('sub_category_id')) {
                $q->whereHas('categories', function ($q) use ($request) {

                    $q->where(['category_id' => $request->sub_category_id, 'parent_id' => $request->category_id]);

                });
            }

            if ($request->name) {
                $q->where('name', 'LIKE', '%' . $request->name . '%')
                    ->orWhere('description', 'LIKE', '%' . $request->name . '%');
            }

        })->orderBy('order')->paginate(10);

        return (new ProductCollection($records))->additional([
            "status" => 1,
            "massage" => "SUCCESS",
        ]);

    }


    public function reviews(Request $request)
    {
        $rules =
            [
                'type' => 'required|in:Pharmacy,Delivery',
                'store_id' => 'required_if:type,Pharmacy|exists:stores,id',
                'delivery_id' => 'required_if:type,Delivery|exists:deliveries,id',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }


        $id = $request->type == 'Pharmacy' ? $request->store_id : $request->delivery_id;
        $className = 'App\Models\\' . $request->type;

        $model = new $className();
        $reviewable = $model->find($id);

        $reviews = $reviewable->reviews()->latest()->paginate(10);

        return Review::collection($reviews)->additional([
            "status" => 1,
            "massage" => "SUCCESS",
        ]);

    }

    public function contactReasons()
    {
        return $this->helper->responseJson(1, 'success', ContactReason::all());
    }


    public function charitableActors(Request $request)
    {
        $records = CharitableActor::where(function ($q) use ($request) {
            if ($request->key) {
                $q->where('name', 'LIKE', '%' . $request->key . '%');
            }

            $q->where('is_active', 1);

        })->get();

        return (\App\Http\Resources\CharitableActor::collection($records))->additional([
            "status" => 1,
            "massage" => "SUCCESS",
        ]);
    }

    public function addDelivery(Request $request)
    {
        $rules =
            [
                'region_id' => 'required|exists:regions,id',
                'name' => 'required',
                'user_name' => 'required|unique:deliveries',
                'phone' => 'required|unique:deliveries|regex:/(01)[0-9]{9}/',
                'gender' => 'required|in:male,female',
                'd_o_b' => 'required|date',
                'photo' => 'required|image|mimes:jpeg,jpg,png,gif,svg',
                'transport_photo' => 'required|image|mimes:jpeg,jpg,png,gif,svg',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $provider = new Delivery();

        $record = $provider->create($request->all());

        $record->password = Hash::make(Str::random(6));
        $record->pin_code = $this->getPinCode();
        $record->pin_code_date_expired = $this->getPinCodeExpiredDate();
        $record->save();
        Photo::addOriginalPhoto($request->file('photo'), $record, 'deliveries', 'photos');
        Photo::addOriginalPhoto($request->file('transport_photo'), $record, 'transports', 'photos', 'transport');

        return $this->helper->responseJson(1, 'تم التسجيل بنجاح سيتم مراجعة طلبكم في خلال 24 ساعه');
    }

    public function makeReservation(Request $request)
    {
        $rules =
            [
                'name' => 'required',
                'event_type' => 'required',
                'cars_num' => 'required|numeric',
                'event_from' => 'required|date_format:Y-m-d',
                'event_to' => 'required|date_format:Y-m-d',
                'time_from' => 'required',
                'time_to' => 'required',
                'days_num' => 'required|numeric',
                'doctors_num' => 'required|numeric',
                'nursing_num' => 'required|numeric',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }


        $record = $request->user('api_client')->reservations()->create($request->all());

        return $this->helper->responseJson(1, 'تم الحجز بنجاح ,قم بانتظار موافقة المنشأة');
    }
}
