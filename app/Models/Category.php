<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model 
{

    protected $table = 'categories';
    public $timestamps = true;
    protected $fillable = array('name', 'parent_id', 'order', 'store_id');

    public function products()
    {
        return $this->morphedByMany('App\Models\Product','categoriable');
    }

    public function store()
    {
        return $this->belongsTo('App\Models\Store');
    }

    public function parent()
    {
        return $this->belongsTo('App\Models\Category','parent_id');
    }

    public function children()
    {
        return $this->hasMany('App\Models\Category','parent_id');
    }

    public function stores()
    {
        return $this->morphedByMany('App\Models\Store' , 'categoriable');
    }

    public function photo()
    {
        return $this->morphOne(Attachment::class , 'attachmentable');
    }

    public function scopeHomeCategories($query)
    {
        return $query->where(['is_active' => 1 , 'store_id' => null]);
    }

    public function scopeParentCategory($query)
    {
        return $query->where('parent_id' , null);
    }

    public function scopeStoreCategories($query)
    {
        return $query->where('store_id' , '!=' , null)->where('is_active' , 1);
    }
}