<?php

namespace App\Http\Controllers\Facility;

//use App\Charts\VisitLineChart;
use App\Charts\AmbulanceAvailability;
use App\Charts\Orders;
use App\Http\Controllers\Controller;
use App\Models\Delivery as Ambulance;
use App\Models\Order;
use App\Models\Service;
use App\Models\User;
use App\MyHelper\Helper;
use Illuminate\Http\Request;
//use Charts;
use DB;

class HomeController extends Controller
{
    protected $guard = 'facilities';

    public function __construct()
    {
        $this->guard = 'facilities';
    }

    public function index(Request $request)
    {
        $facility = auth($this->guard)->user()->hospital;

        $totalCount = $facility->reviews()->count();
        $reviews = [
            "status" => 1,
            "massage" => "SUCCESS",
            'provider_rate' => number_format($facility->reviews()->avg('rate'), 1),
            'total_count_reviews' => $totalCount,
            'counts' => (new Helper())->ratePresent($facility, $facility->reviews()->count())
        ];

        $services = Service::whereIn('type', ['care', 'event', 'provider', 'pharmacy'])
            ->withCount(['orders' => function ($query) use ($request, $facility) {

                $query->where(['hospital_id' => $facility->id, 'status' => 'pending'])
                    ->whereHas('delivery', function ($q) use ($facility) {
                        $q->where('hospital_id', $facility->id);
                    });

            }])->where(function ($query) use ($request) {

            })->get();

        $eventsCount = Service::findOrFail(3)->orders()->where('status', 'pending')->whereHas('hospitals', function ($q) use ($facility) {
            $q->where('hospital_id', $facility->id);
        })->count();

        $pie = $this->buildPieChart($request);
        $amb = $this->buildAmbChart($request);

        return view('facilities.layouts.home', compact('reviews', 'pie', 'amb', 'facility', 'services', 'eventsCount'));
    }

    public function buildPieChart(Request $request, $serviceID = null)
    {

        $relation = $serviceID && $serviceID == 3 ? 'events' : 'orders';

        $facility = auth($this->guard)->user()->hospital;

        $newOrders = $facility->$relation()->whereIn('status', ['pending'])
            ->whereHas('delivery', function ($q) use ($facility) {
                $q->where('hospital_id', $facility->id);
            })
            ->where(function ($query) use ($request, $serviceID) {
                if ($serviceID) {
                    $query->where('service_id', $serviceID);
                }
            })
            ->count();

        $successOrders = $facility->$relation()->whereIn('status', ['End_journey'])
            ->whereHas('delivery', function ($q) use ($facility) {
                $q->where('hospital_id', $facility->id);
            })
            ->where(function ($query) use ($request, $serviceID) {
                if ($serviceID) {
                    $query->where('service_id', $serviceID);
                }
            })
            ->count();

        $canceledOrders = $facility->$relation()->whereIn('status', ['cancel'])
            ->whereHas('delivery', function ($q) use ($facility) {
                $q->where('hospital_id', $facility->id);
            })
            ->where(function ($query) use ($request, $serviceID) {
                if ($serviceID) {
                    $query->where('service_id', $serviceID);
                }
            })
            ->count();

        $orderPieChart = new Orders();
        $orderPieChart
            ->labels(['طلبات ناجحة', 'طلبات ملغاة', 'طلبات حالية'])
            ->dataset("الطلبات", 'pie', [$successOrders, $canceledOrders, $newOrders])
            ->backgroundcolor(["#1abc9c", "#c0392b", "#2980b9"]);

        return $orderPieChart;
    }

    public function buildAmbChart(Request $request, $serviceID = null)
    {
        $facility = auth($this->guard)->user()->hospital;
        $open = $facility->ambulances()->where(function ($q) use ($request, $serviceID) {

            $q->where('status', 'active');

            if ($serviceID) {
                $q->whereHas('services', function ($q) use ($serviceID) {
                    $q->where('services.id', $serviceID);
                });
            }

        })->get()->where('types', 'open')->count();
        $busy = $facility->ambulances()->where(function ($q) use ($request, $serviceID) {

            $q->where('status', 'active');

            if ($serviceID) {
                $q->whereHas('services', function ($q) use ($serviceID) {
                    $q->where('services.id', $serviceID);
                });
            }

        })->get()->where('types', 'busy')->count();
        $closed = $facility->ambulances()->where(function ($q) use ($request, $serviceID) {

            $q->where('status', 'active');

            if ($serviceID) {
                $q->whereHas('services', function ($q) use ($serviceID) {
                    $q->where('services.id', $serviceID);
                });
            }

        })->get()->where('types', 'closed')->count();

        $orderAmbChart = new AmbulanceAvailability();

        $orderAmbChart
            ->labels(['متاح', 'مشغول', 'مغلق'])
            ->dataset("الإسعاف", 'pie', [$open, $busy, $closed])
            ->backgroundcolor(["#1abc9c", "#f39c12", "#c0392b"]);

        return $orderAmbChart;
    }

}
