@extends('facilities.layouts.main',[
                                'page_header'       => 'عرض رحلات الأعياد والمناسبات ',
                                'page_description'  => '  ',
                                'link' => url('facilities/events')
                                ])
@section('content')
    <div class="ibox-content">
        <div class="box-body">
            <div class="box-header">
                {!! Form::open([
                       'method' => 'get'
                       ]) !!}
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::text('order_id',request()->input('order_id'),[
                                'class' => 'form-control',
                                'placeholder' => 'رقم الطلب'
                            ]) !!}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::text('ambulance_id',request()->input('ambulance_id'),[
                                'class' => 'form-control',
                                'placeholder' => 'كود الإسعاف'
                            ]) !!}
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::select('status',
                                [
                                    'pending' => 'رحلة جديد',
                                    'accepted' =>'مقبول في إنتظار الدفع',
                                    'check_out' => 'العميل أتم الدفع',
                                    'scheduled' => 'طلب مقبول',

                                ],\Request::input('status'),[
                                    'class' => 'form-control',
                                    'placeholder' => 'كل حالات الطلبات'
                            ]) !!}
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::text('from',request()->input('from'),[
                                'class' => 'form-control datepicker',
                                'placeholder' => 'من',
                                'autocomplete' => 'off',
                            ]) !!}
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            {!! Form::text('to',\Request::input('to'),[
                                'class' => 'form-control datepicker',
                                'placeholder' => 'إلى',
                                'autocomplete' => 'off',
                            ]) !!}
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <button class="btn btn-flat btn-block btn-primary">بحث</button>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
            @include('flash::message')
            @if(count($records))
                <div class="row">
                    <div class="table-responsive">
                        <table class="data-table table table-bordered">
                            <thead>
                            <tr>

                                <th>#</th>
                                <th>رقم الطلب</th>
                                <th class="text-center">العميل</th>
                                <th class="text-center">توقيت الطلب</th>
                                <th class="text-center">تاريخ بدء الرحلة</th>
                                <th class="text-center">العنوان</th>
                                <th class="text-center">عدد السيارات <br> المطلوبة</th>
                                <th class="text-center"> سياراتك في <br> الرحلة</th>
                                <th class="text-center">الحالة</th>
                                <th class="text-center">عرض</th>

                            </tr>
                            </thead>
                            <tbody>

                            @foreach($records as $record)
                                <tr id="removable{{$record->id}}">
                                    <td>{{$loop->iteration}}</td>
                                    <td><a href="{{url('facilities/orders/'.$record->id)}}">#{{$record->id}}</a></td>
                                    <td class="text-center">
                                        {{optional($record->client)->phone}} <br>
                                        {{optional($record->client)->name}}
                                    </td>

                                    <td class="text-center">
                                        {{\App\MyHelper\Helper::convertDateTimeNotString($record->asked_at)->locale('ar')->isoFormat('dddd  , MMMM  ,  Do / YYYY ')}}
                                    </td>

                                    <td class="text-center">
                                        {{\App\MyHelper\Helper::convertDateTimeNotString($record->schedule_date)->locale('ar')->isoFormat('dddd  , MMMM  ,  Do / YYYY ')}}
                                    </td>

                                    <td class="text-center">
                                        <a href="{{url('http://maps.google.com/?q='.optional($record->startAddress)->latitude.','.optional($record->startAddress)->longitude)}}"
                                           target="_blank">
                                            <i class="fa fa-map-marker"></i>

                                            {{optional($record->startAddress)->title}}
                                        </a>
                                    </td>

                                    <td class="text-center">
                                        <span class="mr-3 badge badge-primary">{{optional($record->patient)->cars_number}} </span>

                                    </td>

                                    <td class="text-center">
                                        @if($record->eventDeliveries()->count())

                                            <span class="mr-3 badge badge-warning">{{optional($record->eventDeliveries())->count()}} </span>

                                        @else
                                            <span class="label label-danger">لا يوجد </span>
                                        @endif
                                    </td>

                                    <td class="text-center">{{$record->client_status_text}}</td>

                                    <td class="text-center">
                                        <a href="{{url('facilities/events/'.$record->id)}}"
                                           class="btn btn-info btn-xs"><i class="fa fa-eye"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>

        </div>
        <div class="text-center">
            {!! $records->render() !!}
        </div>
        @else
            <div class="col-md-4 col-md-offset-4">
                <div class="alert alert-info md-blue text-center">لا يوجد سجلات</div>
            </div>
        @endif
    </div>
    </div>
@endsection
