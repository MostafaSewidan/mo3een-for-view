<?php

namespace App\Models;

use App\MyHelper\Helper;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use JustBetter\PaginationWithHavings\PaginationWithHavings;
use Laravel\Passport\HasApiTokens;

class Delivery extends Authenticatable
{
    use PaginationWithHavings;
    use HasApiTokens, Notifiable;

    protected $table = 'deliveries';
    public $timestamps = true;
    protected $fillable = array(
        'nationality',
        'region_id',
        'name',
        'user_name',
        'phone',
        'gender',
        'd_o_b',
        'status',
        'car_type_id',
        'password',
        'pin_code',
        'pin_code_date_expired',
        'types',
        'rate',
        'email',
        'accept_conditions',
        'have_ambulance',
        'commercial_register',
        'car_model',
        'cars_num',
        'other_apps',
        'car_number'
    );
    protected $appends = array(
        'photo',
        'transport_photo',
        'case',
        'case_label',
        'points',
        'wallet_balance'
    );


    public function region()
    {
        return $this->belongsTo('App\Models\Region');
    }

    public function deliveryRejectReasons()
    {
        return $this->hasMany(DeliveryRejectReasons::class);
    }

    public function reviews()
    {
        return $this->morphMany('App\Models\Review', 'reviewable');
    }

    public function contacts()
    {
        return $this->morphMany('App\Models\Contact', 'contactable');
    }

    public function getCaseAttribute()
    {
        if ($this->types == 'busy')
            return 'مشغول';

        if ($this->types == 'closed')
            return 'مغلق';

        if ($this->types == 'open')
            return 'متاح';
    }

    public function getCaseLabelAttribute()
    {
        if ($this->types == 'busy')
        return '<label class="label-warning">مشغول</label>';

        if ($this->types == 'closed')
        return '<label class="label-danger">مغلق</label>';

        if ($this->types == 'open')
            return '<label class="label-success">متاح</label>';
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function eventOrders()
    {
        return $this->belongsToMany('App\Models\Order' , 'delivery_order');
    }

    public function requestEventOrders()
    {
        return $this->belongsToMany('App\Models\Order' , 'delivery_order_requests');
    }

    public function pointsTransactions()
    {
        return $this->morphMany('App\Models\PointsTransaction', 'pointable');
    }

    public function walletTransactions()
    {
        return $this->morphMany('App\Models\WalletTransaction', 'walletable');
    }

    public function hospital()
    {
        return $this->belongsTo(Hospital::class);
    }


    public function photos()
    {
        return $this->morphMany(Attachment::class, 'attachmentable');
    }

    public function getTransportPhotoAttribute()
    {
        return $this->photos()->where('usage', 'transport')->first() ? $this->photos()->where('usage', 'transport')->first()->path : null;
    }

    public function getPhotoAttribute()
    {
        return $this->photos()->where('usage', null)->first() ? $this->photos()->where('usage', null)->first()->path : null;
    }

    public function getPointsAttribute()
    {
        return $this->pointsTransactions()->latest()->first() ? $this->pointsTransactions()->latest()->first()->points_after : 0;
    }

    public function getWalletBalanceAttribute()
    {
        return $this->walletTransactions()->latest()->first() ? $this->walletTransactions()->latest()->first()->balance_after : 0;
    }

    public function notifications()
    {
        return $this->morphToMany('App\Models\Notification', 'notifiable')->withPivot('is_read');
    }

    public function tokens()
    {
        return $this->morphMany('App\Models\Token', 'tokenable');
    }

    public function services()
    {
        return $this->belongsToMany('App\Models\Service');
    }

    public function scopeCheckArea($query, $points)
    {
        $distance = (new Helper)->settingValue('delivery_filter_area', 6);

        return $query->where(function ($q) use ($points, $distance) {
            $q->select(\DB::raw('*, ( 6367 * acos( cos( radians(' . $points['latitude'] . ') ) * cos( radians( latitude ) ) * 
                    cos( radians( longitude ) - radians(' . $points['longitude'] . ') ) + sin( radians(' . $points['latitude'] . ') ) * sin( radians( latitude ) ) ) ) AS distance'))->having('distance', '<', $distance);
        });

    }
}
