<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Facility extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {


        return [
            'id'        => $this->id,
            'name'      => $this->name,
            'latitude'  => $this->latitude,
            'longitude' => $this->longitude,
            'photo' => asset('photos/logo-png.png'),
        ];
    }
}
