<?php

namespace App\Http\Controllers\Api\Provider;

use App\Http\Controllers\Api\ParentApi;
use App\Http\Resources\NotificationOrder;
use App\Http\Resources\Order;
use App\Http\Resources\OrderCollection;
use App\Models\Order as OrderModel;
use App\Models\Delivery;
use App\MyHelper\Helper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class OrderController extends ParentApi
{
    public function __construct()
    {
        $this->helper = new Helper();
        $this->guard = 'api_delivery';
        $this->model = new Delivery();
        $this->table = 'deliveries';
        $this->apiResource = '\App\Http\Resources\Provider\Provider';
        $this->uniqueRow = 'user_name';
    }

    /**
     * @param $user
     * @param $order
     * @param Request $request
     * @return mixed
     */
    private function wallet($order , Request $request)
    {
        $user = $order->client;
        $wallet = $user->walletTransactions()->latest()->first();

        if($wallet && $wallet->balance_after >= $request->amount_paid)
        {

            $user->walletTransactions()->create(
                [
                    'order_id' => $order->id,
                    'balance_before' => $wallet->balance_after,
                    'amount' => - $request->amount_paid,
                    'balance_after' => $wallet->balance_after - $request->amount_paid,
                    'action' => 'order',
                ]
            );

            $order->amount_paid = $request->amount_paid;
            $order->remaining_amount = $order->real_price - $request->amount_paid;
            $order->save();

            return 'true';
        }

        return 'false';
    }
    /**
     * @param mixed $order
     * @return mixed
     */
    public function CalculatePrice($order)
    {
        $carType = $order->carType;
        $user = $order->client;

        $expectedTime = $order->arrival_Expected_time;
        $realTime = $order->real_time;

        $distance = $order->distance;

        $kmPrice = $carType->km_price;
        $minutePrice = $carType->minute_price;
        $specialClientDiscount = $this->helper->settingValue('special_client_discount', 0);
        $minimumPriceTrip = $carType->min_price;

        $price = 0;

        if ($realTime > $expectedTime) {
            $price = $realTime * $minutePrice;

        } else {
            $price = $distance * $kmPrice;
        }

        if ($user->type == 'special') {
            $price = ($price) - ($price * $specialClientDiscount / 100);
        }

        if($price < $minimumPriceTrip)
            $price = $minimumPriceTrip;

        return $price;

    }

    public function listOrders(Request $request)
    {
        $user = $request->user($this->guard);

        $orders = $user->orders()->where('status', '!=', 'pending')->where(function ($q) use ($request) {

            if ($request->status) {
                $q->where('status', $request->status);
            }

            if ($request->order_id) {
                $q->where('id', $request->order_id);
            }

        })->latest('asked_at')->paginate(15);

        if ($request->status == 'rejected') {
            $orders = OrderModel::whereHas('deliveryRejectReasons', function ($q) use ($user, $request) {
                $q->where(['delivery_id' => $user->id  , 'status' => 'accepted' ,'type' => 'reject']);


                if ($request->order_id) {
                    $q->where('order_id', $request->order_id);
                }

            })->latest('asked_at')->paginate(15);
        }

        return (Order::collection($orders))->responseTo('delivery')->additional([
            "status" => 1,
            "massage" => "SUCCESS",
        ]);
    }

    public function listOrder(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'order_id' => 'required|exists:orders,id',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $orders = OrderModel::CanUpdateStatus()->where(function ($q) use ($request) {

            if ($request->status) {
                $q->where('status', $request->status);
            }

            if ($request->order_id) {
                $q->where('id', $request->order_id);
            }

        })->latest('asked_at')->paginate(15);

        $object = (\App\Http\Resources\Order::collection($orders))->responseTo('delivery');

        return ($object)->additional([
            "status" => 1,
            "massage" => "SUCCESS",
        ]);
    }

    public function lastOrderWithNotification(Request $request)
    {
        $user = $request->user($this->guard);
        $order = $user->notifications()->where('notifications.notifiable_type', 'App\Models\Order')->latest()->first();

        return $this->helper->responseJson(1, 'تمت العملية بنجاح', $order ? (new \App\Http\Resources\Order($order->notifiable))->responseTo('delivery') : (object)[]);
    }

    public function rejectOrder(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'order_id' => 'required|exists:orders,id',
                'reject_order_reason_id' => 'required|exists:reject_order_reasons,id',
                'description' => 'required',
            ];

        $messages = [
            'reject_order_reason_id.required' => 'سبب الرفض غير محدد',
            'reject_order_reason_id.exists' => 'سبب الرفض غير محدد',
            'description.required' => 'تفاصيل سبب الرفض مطلوبة',
        ];


        $validator = validator()->make($request->all(), $rules, $messages);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
        }

        $order = OrderModel::CanUpdateStatus()->where('status', 'pending')->find($request->order_id);

        if ($order) {

            $user->deliveryRejectReasons()->create($request->merge(['status' => 'accepted']));

            return $this->helper->responseJson(1, 'تم الرفض');
        }

        return $this->helper->responseJson(0, 'تم قبول الطلب بالفعل');
    }

    public function acceptOrder(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'order_id' => 'required|exists:orders,id'
            ];


        $validator = validator()->make($request->all(), $rules);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
        }

        $order = OrderModel::CanUpdateStatus()->whereNotIn('service_id',[3])->where('status', 'pending')->find($request->order_id);

        if ($order && !$order->deliveryRejectReasons()->where('delivery_id' , $user->id)->first()) {

            if ($order->service_id == 7)
                $order->update(['start_trip' => Carbon::now()]);

            $order->update(['status' => 'accepted', 'delivery_id' => $user->id , 'hospital_id' => $user->hospital ? $user->hospital->id : null]);
            $user->types = 'busy';
            $user->save();

            // send notifications to store and client
            $this->helper->sendNotification($order, [$order->client_id], 'clients', 'تم قبول رحلتك ', 'قام معيين بقبول الرحلة التي تقدمت بطلبها', 'order', new NotificationOrder($order));

            return $this->helper->responseJson(1, 'تم القبول بنجاح', (new \App\Http\Resources\Order($order))->responseTo('delivery'));
        }

        return $this->helper->responseJson(2, ' تم قبول الرحلة مسبقاً');
    }

    public function arrivesToPatient(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'order_id' => 'required|exists:orders,id'
            ];


        $validator = validator()->make($request->all(), $rules);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
        }

        $order = $user->orders()->whereNotIn('service_id',[3,7])->where('status', 'accepted')->find($request->order_id);

        if ($order) {
            $order->update(['status' => 'arrives_to_patient', 'start_trip' => Carbon::now()]);

            // send notifications to store and client
            $this->helper->sendNotification($order, [$order->client_id], 'clients', 'معيين وصل ', 'قام معيين بتأكيد الوصول إلي مكانك', 'order', new NotificationOrder($order));

            return $this->helper->responseJson(1, 'تم تأكيد الوصول بنجاح', (new \App\Http\Resources\Order($order))->responseTo('delivery'));
        }

        return $this->helper->responseJson(2, ' تم تأكيد الوصول مسبقاً');
    }

    public function moveWithPatient(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'order_id' => 'required|exists:orders,id'
            ];


        $validator = validator()->make($request->all(), $rules);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
        }

        $order = $user->orders()->whereNotIn('service_id',[3,7])->where('status', 'arrives_to_patient')->find($request->order_id);

        if ($order) {
            $order->update(['status' => 'move_with_patient']);

            $order->patient()->update($request->all());

            // send notifications to store and client
            $this->helper->sendNotification($order, [$order->client_id], 'clients', 'معيين بدء بالتحرك ', 'قام معيين ببدء التحرك بتجاه العنوان الذي قمت بتحديده', 'order', new NotificationOrder($order));

            return $this->helper->responseJson(1, 'تم تأكيد بدء التحرك بنجاح', (new \App\Http\Resources\Order($order))->responseTo('delivery'));
        }

        return $this->helper->responseJson(2, 'الرجاء تأكيد وصولك للعنوان المحدد أولا');
    }

    public function arrivedToHospital(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'order_id' => 'required|exists:orders,id'
            ];


        $validator = validator()->make($request->all(), $rules);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
        }

        $order = $user->orders()->whereNotIn('service_id',[3,7])->where('status', 'move_with_patient')->find($request->order_id);

        if ($order) {

            $order->update(['status' => 'arrived_to_hospital', 'end_trip' => Carbon::now()]);
            $order->real_price = $this->CalculatePrice($order);
            $order->save();

            return $this->helper->responseJson(1, 'تم تأكيد الوصول للمستشفي بنجاح', (new \App\Http\Resources\Order($order))->responseTo('delivery'));
        }

        return $this->helper->responseJson(2, 'الرجاء التأكد من بدء التحرك للعنوان المحدد أولا');
    }

    public function EndJourney(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'order_id' => 'required|exists:orders,id',
            ];

        $validator = validator()->make($request->all(), $rules);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
        }

        $order = $user->orders()->whereNotIn('service_id',[3,7])->where('status', 'arrived_to_hospital')->find($request->order_id);

        if ($order) {
            DB::beginTransaction(); //TODO

            DB::commit();
//            if($order->payment_method_id == 3)
//            {
//                $rules =
//                    [
//                        'paid' => 'required',
//                        'the_rest' => 'required|in:cash,wallet'
//                    ];
//
//                $messages = [
//                    'paid.required' => 'المبلغ المدفوع مطلوب',
//                    'the_rest.required' => 'طريقة تحصيل الباقي',
//                    'the_rest.in' => 'طريقة تحصيل الباقي'
//                ];
//
//
//                $validator = validator()->make($request->all(), $rules, $messages);
//
//                if ($validator->fails()) {
//
//                    return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
//                }
//
//                if ($request->the_rest == 'wallet')
//                {
//
//                    if ($request->paid > $order->real_price) {
//                        $amount = $request->paid - $order->real_price;
//                        $wallet = $user->walletTransactions()->latest()->first();
//
//                        $order->client->walletTransactions()->create(
//                            [
//                                'order_id' => $order->id,
//                                'balance_before' => $wallet ? $wallet->balance_after : 0,
//                                'amount' => $amount,
//                                'balance_after' => $wallet ? $wallet->balance_after + $amount : $amount,
//                                'action' => 'change',
//                            ]
//                        );
//
//                    } elseif ($request->paid < $order->real_price) {
//                        return $this->helper->responseJson(0, 'المبلغ المدفوع أقل من نكلفة الرحلة');
//                    }
//
//                }
//
//            }elseif($order->payment_method_id == 1){
//
////             TODO   $rules =
////                    [
////                        'paid' => 'required',
////                        'the_rest' => 'required|in:cash,wallet'
////                    ];
////
////                $messages = [
////                    'paid.required' => 'المبلغ المدفوع مطلوب',
////                    'the_rest.required' => 'طريقة تحصيل الباقي',
////                    'the_rest.in' => 'طريقة تحصيل الباقي'
////                ];
//
//
//                $validator = validator()->make($request->all(), $rules);
//
//                if ($validator->fails()) {
//
//                    return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
//                }
//
//                $wallet = $this->wallet( $order, $request);
//
//                if ($wallet == 'false') {
//                    return $this->helper->responseJson(0, 'ليس لديك رصيد كافي في المحفظة الخاصة بك');
//                }
//            }

            $order->update(['status' => 'End_journey']);
            $user->types = 'open';
            $user->save();

            // send notifications to store and client
            $this->helper->sendNotification($order, [$order->client_id], 'clients', 'تمت الرحلة بنجاح', 'قام معين بإتمام رحلتك بنجاح الرجاء إضافة تقييم عن مدي رضاك عن خدمتنا', 'order', new NotificationOrder($order));

            return $this->helper->responseJson(1, 'تم إنهاء الرحلة بنجاح', (new \App\Http\Resources\Order($order))->responseTo('delivery'));

        }

        return $this->helper->responseJson(2, 'الرجاء تاكيد الوصول للمستشفي أولا');
    }

    public function cancelOrder(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'order_id' => 'required|exists:orders,id',
                'description' => 'required',
            ];

        $messages = [

            'reject_order_reason_id.required' => 'سبب الإلغاء غير محدد',
            'reject_order_reason_id.exists' => 'سبب الإلغاء غير محدد',
            'description.required' => 'تفاصيل سبب الإلغاء مطلوبة',
        ];


        $validator = validator()->make($request->all(), $rules, $messages);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
        }

        $order = $user->orders()->whereIn('status', ['accepted', 'arrives_to_patient'])->find($request->order_id);

        if ($order) {

            $order->deliveryRejectReasons()->create(
                array_merge($request->all() , [
                    'delivery_id' => $user->id,
                    'type' => 'cancel',
                ])
            );
//            $order->update(['status' => 'cancel', 'reason' => $request->description]);
//            $user->types = 'open';
//            $user->save();

//            // send notifications to store and client
//            $this->helper->sendNotification($order, [$order->client_id], 'clients', 'تم إلغاء رحلتك ', 'قام معيين بإلغاء الرحلة الخاصة بك', 'order', new NotificationOrder($order));

            return $this->helper->responseJson(1, 'تم التقدم بطلب الإلغاء بنجاح , الرجاء انتظار موافقة الإدارة', (new \App\Http\Resources\Order($order))->responseTo('delivery'));
        }

        return $this->helper->responseJson(2, 'لا يمكن إلغاء هذه الرحلة');

    }

    public function arrivesToPharmacy(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'order_id' => 'required|exists:orders,id',
            ];

        $validator = validator()->make($request->all(), $rules);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
        }

        $order = $user->orders()->whereIn('service_id',[7])->where('status', 'accepted')->find($request->order_id);

        if ($order) {
            $order->update(['status' => 'arrives_to_pharmacy']);

            // send notifications to store and client
            $this->helper->sendNotification($order, [$order->client_id], 'clients', 'معين وصل للصيدلية ', 'قام معيين بتأكيد وصوله للصيدلية', 'order', new NotificationOrder($order));

            return $this->helper->responseJson(1, 'تم تأكيد الوصول للصيدلية بنجاح', (new \App\Http\Resources\Order($order))->responseTo('delivery'));
        }

        return $this->helper->responseJson(2, 'تم قبول الطلب مسبقا');
    }

    public function deliveryLoaded(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'order_id' => 'required|exists:orders,id',
            ];


        $validator = validator()->make($request->all(), $rules);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
        }

        $order = $user->orders()->whereIn('service_id',[7])->where('status', 'arrives_to_pharmacy')->find($request->order_id);

        if ($order) {
            $order->update(['status' => 'delivery_loaded']);

            // send notifications to store and client
            $this->helper->sendNotification($order, [$order->client_id], 'clients', 'قام معين بإستلام الطلب', 'قام معيين بإستلام الطلب من الصيدلية', 'order', new NotificationOrder($order));

            return $this->helper->responseJson(1, 'تم تأكيد إستلام الطلب من الصيدلية بنجاح', (new \App\Http\Resources\Order($order))->responseTo('delivery'));
        }

        return $this->helper->responseJson(2, 'الرجاء تأكيد الوصول للصيدلية أولا');
    }

    public function deliveryGo(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'order_id' => 'required|exists:orders,id',
            ];



        $validator = validator()->make($request->all(), $rules);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
        }

        $order = $user->orders()->whereIn('service_id',[7])->where('status', 'delivery_loaded')->find($request->order_id);

        if ($order) {
            $order->update(['status' => 'delivery_go']);

            // send notifications to store and client
            $this->helper->sendNotification($order, [$order->client_id], 'clients', 'معين يتحرك الأن بإتجاهك', 'قام معيين ببدا التحرك بإتجاهك', 'order', new NotificationOrder($order));

            return $this->helper->responseJson(1, 'تم تأكيد بدأ التحرك بنجاح', (new \App\Http\Resources\Order($order))->responseTo('delivery'));
        }

        return $this->helper->responseJson(2, 'الرجاء تأكيد إستلام الطلب من الصيدلية أولا');
    }

    public function arrivedToClient(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'order_id' => 'required|exists:orders,id',
            ];

        $validator = validator()->make($request->all(), $rules);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
        }

        $order = $user->orders()->whereIn('service_id',[7])->where('status', 'delivery_go')->find($request->order_id);

        if ($order) {
            $order->update(['status' => 'arrived_to_client' , 'end_trip' => Carbon::now()]);
            $order->real_price = $this->CalculatePrice($order);
            $order->save();

            // send notifications to store and client
            $this->helper->sendNotification($order, [$order->client_id], 'clients', 'معين أكد وصول لك', 'قام معيين أكد الوصول بالطلب لك', 'order', new NotificationOrder($order));

            return $this->helper->responseJson(1, 'تم تأكيد الوصول للعميل بنجاح', (new \App\Http\Resources\Order($order))->responseTo('delivery'));
        }

        return $this->helper->responseJson(2, 'الرجاء تأكيد التحرك أولا');
    }

}
