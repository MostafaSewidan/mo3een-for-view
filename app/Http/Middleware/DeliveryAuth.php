<?php

namespace App\Http\Middleware;


use App\MyHelper\Helper;
use Closure;


class DeliveryAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $helper = new Helper();
        if(auth('api_delivery')->user()->status == 'deactivate')
        {
            return $helper->responseJson(0, 'تم حظر هذا الحساب');

        }elseif(auth('api_delivery')->user()->status == 'pending')
        {
            return $helper->responseJson(0, 'لم يتم قبول هذا الحساب بعد');

        }elseif(auth('api_delivery')->user()->status == 'rejected')
        {
            return $helper->responseJson(0, 'تم رفض هذا الحساب');
        }
        return $next($request);
    }
}
