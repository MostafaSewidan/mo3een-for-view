<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryRejectReasons extends Model
{

    protected $table = 'delivery_reject_reasons';
    public $timestamps = true;
    protected $fillable = array('order_id','delivery_id','reject_order_reason_id','description','type','status','rejectable_type','rejectable_id');

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function delivery()
    {
        return $this->belongsTo(Delivery::class);
    }

    public function rejectable()
    {
        return $this->morphTo();
    }

    public function rejectOrderReason()
    {
        return $this->belongsTo(RejectOrderReason::class);
    }

    public function scopeaccepted($query)
    {
        return $query->where(['status' => 'accepted']);
    }

    public function scopetype($query , $type)
    {
        return $query->where([ 'type' => $type]);
    }
}