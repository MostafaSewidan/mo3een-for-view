@extends('admin.layouts.main',[
								'page_header'		=> 'الصيدليات',
								'page_description'	=> 'عرض ',
								'link' => url('admin/pharmacies')
								])
@section('content')
    <div class="ibox ibox-primary">
        <div class="ibox-title">
            {!! Form::open([
                   'method' => 'get'
                   ]) !!}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::text('id',request()->input('id'),[
                        'placeholder' => 'كود الصيدلية',
                        'class' => 'form-control'
                        ]) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::text('name',request()->input('name'),[
                        'placeholder' => 'اسم الصيدلية',
                        'class' => 'form-control'
                        ]) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::text('phone',request()->input('phone'),[
                        'class' => 'form-control',
                        'placeholder' => 'تليفون المفوض'
                        ]) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::text('owner_name',request()->input('owner_name'),[
                        'class' => 'form-control',
                        'placeholder' => 'اسم المفوض'
                        ]) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::select('pagination',[
                            10 => 10,
                            15 => 15,
                            25 => 25,
                            50 => 50,
                            100 => 100
                        ],request('pagination'),[
                            'class' => 'form-control',
                            'placeholder' => 'عدد النتائج في الصفحة'
                        ]) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    @inject('countries','App\Models\Country')
                    <div class="form-group">
                        {!! Form::select('country',$countries->pluck('name','id')->toArray(),request('country'),[
                            'class' => 'form-control',
                            'id' => 'country',
                            'placeholder' => 'كل المناطق'
                        ]) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::select('city',$cities,request('city'),[
                            'class' => 'form-control',
                            'id' => 'city',
                            'placeholder' => 'كل المدن'
                        ]) !!}
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        {!! Form::select('region_id',$regions,request('region_id'),[
                            'class' => 'form-control',
                            'id' => 'region_id',
                            'placeholder' => 'كل الأحياء'
                        ]) !!}
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <button class="btn btn-primary btn-block" type="submit"><i class="fa fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>


            @push('scripts')
                <script>
                    $('#country').change(function (){

                        var url = '{!! url('admin/country/cities') !!}/' + $('#country').val();

                        $.ajax({
                            url: url,
                            type: 'get',
                            success:function(data){

                                var builtSelectCategory ='<option value="" selected>قم بالاخيتار</option>';
                                $.each(data.data, function(index, item) {
                                    var option = '<option value="'+item.id+'">'+item.name+'</option>';
                                    builtSelectCategory+= option;
                                });

                                $('#city').text('').append(builtSelectCategory);
                            },
                            error: function (data) {

                                $('#city').text('').append('<option value="" selected>لا يوجد مدن تابعة لهذه المنطقة</option>');
                            }
                        });
                    });
                    $('#city').change(function (){

                        var url = '{!! url('admin/city/regions') !!}/' + $('#city').val();

                        $.ajax({
                            url: url,
                            type: 'get',
                            success:function(data){

                                var builtSelectCategory ='<option value="" selected>قم بالاخيتار</option>';
                                $.each(data.data, function(index, item) {
                                    var option = '<option value="'+item.id+'">'+item.name+'</option>';
                                    builtSelectCategory+= option;
                                });

                                $('#region_id').text('').append(builtSelectCategory);
                            },
                            error: function (data) {

                                $('#region_id').text('').append('<option value="" selected>لا يوجد مدن تابعة لهذه المدينة</option>');
                            }
                        });
                    });
                </script>
            @endpush
            <div class="clearfix"></div>
            {!! Form::close() !!}
        </div>
        <div class="ibox-content">
            @include('flash::message')
            @if(count($records))
                <div class="table-responsive">
                    <table class="data-table table table-bordered">
                        <thead>
                        <th>الكود </th>
                        <th>الاسم </th>
                        <th>اسم المفوض</th>
                        <th>التليفون</th>
                        <th class="text-center">الحالة</th>
                        <th class="text-center">عدد الطلبات</th>
                        <th class="text-center">عرض</th>
                        </thead>
                        <tbody>

                        @foreach($records as $record)
                            <tr id="removable{{$record->id}}">
                                <td>#{{$record->id}}</td>
                                <td>{{$record->name}}</td>
                                <td>{{$record->owner_name}}</td>
                                <td>{{$record->phone}}</td>
                                <td class="text-center">

                                    {!! \App\MyHelper\Helper::toggleBooleanView($record , url('admin/pharmacies/toggle-boolean/'.$record->id.'/status/'),'status','open','closed') !!}

                                </td>

                                <td class="text-center">
                                    {{$record->orders_count}}
                                </td>

                                <td class="text-center">
                                    <a href="{{url(route('pharmacies.show',$record->id))}}" class="btn btn-info btn-sm">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="text-center">
                    {!! $records->render() !!}
                </div>
            @else
                <div>
                    <h3 class="text-info" style="text-align: center"> لا توجد بيانات للعرض </h3>
                </div>
            @endif
        </div>
    </div>
@endsection
