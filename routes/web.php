<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear-cache', function () {
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('config:cache');

    return 'done';
});

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

Route::get('/', 'FrontController@index')->name('index');
Route::get('about', 'FrontController@about')->name('about');
Route::get('contact', 'FrontController@contact')->name('contact');
Route::post('contact-us', 'FrontController@contactSubmit')->name('contact-us');
Route::get('job', 'FrontController@jobs')->name('jobs');
Route::get('job-apply/{id}', 'FrontController@jobApply');
Route::post('job-apply/{id}', 'FrontController@jobApplySubmit');
Route::get('partner-signup', 'FrontController@storeSignup');
Route::post('partner-signup', 'FrontController@storeSignupSubmit');
Route::get('terms', 'FrontController@terms')->name('terms');
Route::get('policies', 'FrontController@policies')->name('policy');
