<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{

    protected $table      = 'addresses';
    public    $timestamps = true;
    protected $fillable   = array('title', 'latitude', 'longitude', 'type', 'addressable_type', 'addressable_id', 'is_saved');

    public function addressable()
    {
        return $this->morphTo();
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

//    public function store()
//    {
//        return $this->belongsTo(Pharmacy::class , 'addressable_id');
//    }

}
