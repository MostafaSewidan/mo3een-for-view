<?php

namespace App\Http\Controllers\Facility;

use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationOrder;
use App\Models\City;
use App\Models\Delivery;
use App\Models\Governorate;
use App\Models\Order;
use App\Models\Review;
use App\MyHelper\Helper;
use Illuminate\Http\Request;

class EventController extends Controller
{
    protected $model;
    protected $helper;
    protected $guard = 'facilities';
    protected $url = 'facilities/events';
    protected $viewsDomain = 'facilities.events.';

    public function __construct()
    {
        $this->helper = new Helper;
        $this->guard = 'facilities';
    }

    /**
     * @param $view
     * @param array $params
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $facility = auth($this->guard)->user()->hospital;

        $records = $facility->events()->where('service_id', 3)->where(function ($q) use ($request) {

            if ($request->ambulance_id) {
                $q->where('delivery_id', $request->ambulance_id);
            }

            if ($request->status) {
                $q->where('status', $request->status);
            }

            if ($request->service) {
                $q->where('service_id', $request->service);
            }

            if ($request->order_id) {
                $q->where('id', $request->order_id);
            }


            if ($request->from) {
                $q->whereDate('created_at', '>=', Helper::convertDateTime($request->from));
            }

            if ($request->to) {
                $q->whereDate('created_at', '<=', Helper::convertDateTime($request->to));
            }

        })->orderBy('asked_at')->paginate(15);

        return $this->view('index', compact('records'));
    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($id)
    {
        $facility = auth($this->guard)->user()->hospital;
        $ambulances = $facility->ambulances;
        $order = $facility->events()->where('service_id', 3)->findOrFail($id);

        return $this->view('show', compact('order', 'facility', 'ambulances'));
    }

//    /**
//     * Remove the specified resource from storage.
//     *
//     * @param int $id
//     * @return \Illuminate\Http\Response
//     * @throws \Exception
//     */
//    public function destroy($id)
//    {
//        $record = $this->model->find($id);
//        //dd($record);
//        if (!$record) {
//            return response()->json([
//                'status' => 0,
//                'message' => 'تعذر الحصول على البيانات'
//            ]);
//        }
//
//        if ($record->cities()->count()) {
//            return response()->json([
//                'status' => 0,
//                'message' => 'يوجد مدن مرتبطة بهذه المحافظة',
//            ]);
//        }
//        $record->delete();
//        return response()->json([
//            'status' => 1,
//            'message' => 'تم الحذف بنجاح',
//            'id' => $id
//        ]);
//    }


    public function acceptEvent(Request $request)
    {
        $facility = auth($this->guard)->user()->hospital;

        $order = $facility->events()->where('service_id', 3)->findOrFail($request->order_id);

        $rules =
            [
                'ambulances' => 'required',
                'ambulances.*' => 'exists:deliveries,id',
                'doctors' => 'nullable|numeric|min:1|max:' . ($order->patient->doctors - $order->hospitalDoctors()->sum('number')),
                'nurses' => 'nullable|numeric|min:1|max:' . ($order->patient->nurses - $order->hospitalNurses()->sum('number')),
            ];

        $messages = [
            'ambulances.required' => 'الرجاء اختيار الإسعاف',
            'ambulances.exists' => 'الرجاء اختيار الإسعاف',
            'description.required' => 'تفاصيل سبب الإلغاء مطلوبة',
            'doctors.numeric' => 'الرجاء ادخال الرقم بالشكل الصحيح',
            'doctors.max' => 'عدد الأطباء الذي تود إدخاله أكبر من المطلوب',
            'nurses.numeric' => 'الرجاء ادخال الرقم بالشكل الصحيح',
            'nurses.max' => 'عدد الممرضين الذي تود إدخاله أكبر من المطلوب',
        ];


        $validator = validator()->make($request->all(), $rules, $messages);


        if ($validator->fails()) {
            return response()->json([
                'status' => 0,
//                'message' => 'يوجد مدن مرتبطة بهذه المحافظة',
                'errors' => $validator->errors()
            ], 400);
        }

        if(count($request->ambulances) > ($order->patient->cars_number - $order->eventDeliveries()->where('hospital_id' , '!=' , $facility->id)->count()))
        {
            return response()->json([
                'status' => 0,
//                'message' => 'يوجد مدن مرتبطة بهذه المحافظة',
                'errors' => ['ambulances' => 'عدد السيارات الذي تود إدخاله أكبر من المطلوب']
            ], 400);
        }

        if ($order->status == 'pending') {

            $order->eventDeliveries()->where('hospital_id' , $facility->id)->sync($request->ambulances);

            $order->hospitalDoctors()->where('hospital_id', $facility->id)->detach();
            $order->hospitalNurses()->where('hospital_id', $facility->id)->detach();

            if ($request->input('doctors')) {

                $order->doctorsAndNurses()->attach($facility->id, [
                    'type' => 'doctor',
                    'number' => $request->doctors,
                ]);

            }

            if ($request->input('nurses')) {
                $order->doctorsAndNurses()->attach($facility->id, [
                    'type' => 'nurse',
                    'number' => $request->nurses,
                ]);
            }

            if($order->patient->cars_number == $order->eventDeliveries()->count())
            {
                $order->status = 'accepted';
                $order->save();

                foreach ($order->hospitals as $hospital)
                {

                    $ambulancesCount = $hospital->ambulances()->whereHas('eventOrders' , function ($q) use ($order)
                    {
                        $q->where('order_id' , $order->id);

                    })->count();

                    if(!$ambulancesCount)
                    {
                        $order->hospitals()->detach($hospital->id);
                    }

                }

                $this->helper->sendNotification($order, [$order->client_id], 'clients', 'تم الموافقة علي طلبك ', '#'.$order->id .'قامت المنشأت بالموافقة علي طلبك برقم ', 'order', new NotificationOrder($order));
            }

            flash()->success('تم التسجيل بنجاح');
            return response()->json([
                'status' => 1,
                'message' => '',
                'url' => url('facilities/events/'.$order->id)
            ]);

        }else{

            $order->requestEventDeliveries()->where('hospital_id' , $facility->id)->sync($request->ambulances);

            $order->requestHospitalDoctors()->where('hospital_id', $facility->id)->detach();
            $order->requestHospitalNurses()->where('hospital_id', $facility->id)->detach();

            if ($request->input('doctors')) {

                $order->requestHospitals()->attach($facility->id, [
                    'type' => 'doctor',
                    'number' => $request->doctors,
                ]);

            }

            if ($request->input('nurses')) {
                $order->requestHospitals()->attach($facility->id, [
                    'type' => 'nurse',
                    'number' => $request->nurses,
                ]);
            }

            //TODO notifi admin
            flash()->success('تم التقدم بطلب التعديل بنجاح , الرجاء انتظار موافقة الإدارة');
            return response()->json([
                'status' => 1,
                'message' => '',
                'url' => url('facilities/events/'.$order->id)
            ]);
        }

    }


    public function requestCancel(Request $request)
    {
        $rules =
            [
                'order_id' => 'required|exists:orders,id',
                'description' => 'required',
                'reject_order_reason_id' => 'required|exists:reject_order_reasons,id',
            ];

        $messages = [
            'reject_order_reason_id.required' => 'سبب الإلغاء غير محدد',
            'reject_order_reason_id.exists' => 'سبب الإلغاء غير محدد',
            'description.required' => 'تفاصيل سبب الإلغاء مطلوبة',
        ];


        $validator = validator()->make($request->all(), $rules, $messages);


        if ($validator->fails()) {
            return response()->json([
                'status' => 0,
                'message' => 'يوجد مدن مرتبطة بهذه المحافظة',
                'errors' => $validator->errors()
            ], 400);
        }

        $facility = auth($this->guard)->user()->hospital;


        $order = $facility->events()->where('service_id', 3)->findOrFail($request->order_id);

        $user = $order->delivery;

        if ($order) {

            $facility->orderRejectReasons()->create(
                array_merge($request->all(), [
                    'type' => 'cancel',
                ])
            );

            //TODO notifi admin

            flash()->success('تم التقدم بطلب الإلغاء بنجاح , الرجاء انتظار موافقة الإدارة');
            return response()->json([
                'status' => 1,
                'message' => '',
                'url' => url('facilities/events/'.$order->id)
            ]);
        }

        flash()->error('حدث خطأ حاول في وقت لاحق');
        return response()->json([
            'status' => 1,
            'message' => '',
            'url' => url('facilities/events/'.$order->id)
        ]);
    }
}
