<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAddressesTable extends Migration
{

    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->enum('type', array(''));
            $table->decimal('latitude', 10, 8);
            $table->decimal('longitude', 10, 8);
            $table->string('addressable_type');
            $table->integer('addressable_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('addresses');
    }
}
