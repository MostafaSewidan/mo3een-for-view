<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRejectOrderReasonsTable extends Migration {

	public function up()
	{
		Schema::create('deliveries', function(Blueprint $table) {
			$table->increments('id');
			$table->tinyInteger('is_active')->default(1);
			$table->string('name');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('reject_order_reasons');
	}
}