<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactReason extends Model
{

    protected $table = 'contact_reasons';
    public $timestamps = true;
    protected $fillable = array('reason');

    public function contacts()
    {
        return $this->hasMany('App\Models\Contact');
    }

}