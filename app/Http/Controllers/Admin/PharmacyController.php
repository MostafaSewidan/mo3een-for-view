<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\EmergencyNumber;
use App\Models\Pharmacy;
use App\Models\Post;
use App\Models\Region;
use App\MyHelper\Helper;
use App\MyHelper\Photo;
use App\MyHelper\PhotoV2;
use Illuminate\Http\Request;
use Response;

class PharmacyController extends Controller
{

    protected $model;
    protected $viewsDomain = 'admin/pharmacies.';
    protected $url = 'admin/pharmacies/';

    public function __construct()
    {
        $this->model = new Pharmacy();
    }

    /**
     * @param $view
     * @param array $params
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $records = $this->model->where(function ($q) use ($request) {

            if ($request->id) {

                $q->where('id', $request->id);

            } else {

                if ($request->name) {
                    $q->where(function ($q) use ($request) {
                        $q->where('name', 'like', '%' . $request->name . '%');
                    });
                }

                if ($request->phone)
                    $q->where('phone', $request->phone);

                if ($request->owner_name)
                    $q->where('owner_name', 'like', '%' . $request->owner_name . '%');

                if ($request->country) {
                    $q->whereHas('region', function ($q) use ($request) {
                        $q->whereHas('city', function ($q) use ($request) {
                            $q->where('country_id', $request->country);
                        });
                    });
                }

                if ($request->city) {
                    $q->whereHas('region', function ($q) use ($request) {
                        $q->where('city_id', $request->city);
                    });
                }

                if ($request->region_id) {
                    $q->where('region_id', $request->region_id);
                }

            }

        })->latest()->paginate($request->input('pagination', 10));

        if ($request->country)
            $cities = City::where('country_id', $request->country)->pluck('name', 'id')->toArray();
        else
            $cities = City::pluck('name', 'id')->toArray();

        if ($request->city)
            $regions = Region::where('city_id', $request->city)->pluck('name', 'id')->toArray();
        else
            $regions = Region::pluck('name', 'id')->toArray();

        return $this->view('index', compact('records', 'cities', 'regions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $record = $this->model->findOrFail($id);
        return $this->view('show', compact('record'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = $this->model;
        return $this->view('create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =
            [
                'title' => 'required',
                'number' => 'required',
            ];

        $error_sms =
            [
                'title.required' => 'العنوان  مطلوب',
                'number.required' => 'الرقم مطلوب',
            ];

        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return back()->withInput()->withErrors($data->errors());
        }

        $record = $this->model->create($request->all());

        if (!$request->input('order')) {
            $record->order = $this->model->all()->count();
            $record->save();
        }

        session()->flash('success', 'تمت الاضافة بنجاح');
        return redirect($this->url);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->model->findOrFail($id);
        return $this->view('edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =
            [
                'title' => 'required',
                'number' => 'required',
            ];

        $error_sms =
            [
                'title.required' => 'العنوان  مطلوب',
                'number.required' => 'الرقم مطلوب',
            ];


        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return back()->withInput()->withErrors($data->errors());
        }

        $record = $this->model->findOrFail($id);

        $record->update($request->all());

        session()->flash('success', 'تمت تحديث بنجاح');
        return back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = $this->model->findOrFail($id);

        $record->delete();

        $data = [
            'status' => 1,
            'message' => 'تم الحذف بنجاح',
            'id' => $id
        ];

        return Response::json($data, 200);
    }


    public function toggleBoolean($id, $action)
    {
        $record = $this->model->findOrFail($id);

        $activate = Helper::toggleBoolean($record, $action, 'open', 'closed');

        if ($activate) {
            return Helper::responseJson(1, 'تمت العملية بنجاح');
        }

        return Helper::responseJson(0, 'حدث خطأ');
    }
}
