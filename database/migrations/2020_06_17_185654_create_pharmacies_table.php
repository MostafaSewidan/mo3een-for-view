<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePharmaciesTable extends Migration {

	public function up()
	{
		Schema::create('pharmacies', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('region_id');
			$table->string('name');
			// upload
			$table->string('owner_name');
			$table->string('phone');
			$table->string('email');
			$table->enum('status', array(''));
			$table->enum('date_type', array(''));
			$table->string('password')->nullable();
			$table->string('pin_code')->nullable();
			$table->datetime('pin_code_date_expired')->nullable();
			$table->enum('delivery_type', array('fast', 'normal'));
			$table->float('delivery_cost')->nullable()->default('0');
			$table->integer('delivery_start_time')->nullable();
			$table->integer('delivery_end_time')->nullable();
			$table->integer('rate')->default('0');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('pharmacies');
	}
}