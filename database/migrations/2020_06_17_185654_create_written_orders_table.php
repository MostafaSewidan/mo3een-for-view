<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWrittenOrdersTable extends Migration {

	public function up()
	{
		Schema::create('written_orders', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('order_id');
			$table->enum('type', array(''));
			$table->longText('order_description')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('written_orders');
	}
}