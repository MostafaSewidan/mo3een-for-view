<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    protected $dates = ['death_date','event_start_date','event_end_date'];
    protected $table = 'patients';
    public $timestamps = true;
    public $appends = ['days_number'];
    protected $fillable = array(
        'order_id',
        'type',
        'name',
        'dead_name',
        'facility_name',
        'national_id',
        'dead_national_id',
        'diseases',
        'clinic_status',
        'length',
        'weight',
        'blood_type_id',
        'floor',
        'elevator',
        'cases_num',
        'notes',
        'insurance_number',
        'death_date',
        'event_start_date',
        'event_end_date',
        'burial_permit_number',
        'relative_relation',
        'event_type',
        'cars_number',
        'nurses',
        'doctors',
    );

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }


    public function bloodType()
    {
        return $this->belongsTo(BloodType::class);
    }

    public function getDaysNumberAttribute()
    {
        return $this->d_o_b ? $this->event_end_date->diffInYears($this->event_start_date) : 0;
    }
}
