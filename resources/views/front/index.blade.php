@extends('front.master')
@section('content')
    <!-- Top Navbar -->
    <!-- main carousel -->
    <section class="carousel-wrapper">
        <div class="owl-carousel main-carousel">
            <div>
                <img src="{{asset('front/images/mo3een-slider.png')}}" alt="Sonqr slider 1">
            </div>
            <div>
                <img src="{{asset('front/images/mo3een-slider.png')}}" alt="Sonqr slider 2">
            </div>
            <div>
                <img src="{{asset('front/images/mo3een-slider.png')}}" alt="Sonqr slider 3">
            </div>
        </div>
        <div class="download-apps">
            <h2 class="text-center mb-5">حمل التطبيق</h2>
            <div class="row">
                <div class="col-sm-6 text-center">
                    <a href="#">
                        <img src="{{asset('front/images/google-play.png')}}" alt="">
                    </a>
                </div>
                <div class="col-sm-6 text-center">
                    <a href="#">
                        <img src="{{asset('front/images/app-store.png')}}" alt="">
                    </a>
                </div>
            </div>
        </div>
    </section>
    <!-- main carousel -->

    <!-- about us -->
    <section class="about-us pt-5 pb-5">
        <div class="container">
            <h2 class="text-center text-primary mt-4 mb-4">عن مُعين</h2>
            <div class="about-content">
                <p>مُعين شركة متخصصة في توصيل المرضى .. لديها فريق على أعلى مستوى من الإحترافية والمهنية لإيجاد الطرق
                    السهلة
                    والبسيطة التي توفر بشكل غير تقليدي الاحتياجات اليومية بطريقة سهلة وآمنة وسريعة ، كما توفر خدمة عملاء
                    على
                    مدار الساعة لتلبية احتياجاتكم وتوفير أفضل خدمة ممكنة.</p>
            </div>
        </div>
    </section>
    <!-- about us -->

    <!-- statistics -->
    <section class="statistics pt-4">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 text-center mb-5">
                    <img src="{{asset('front/images/governorates.jpg')}}" alt="" class="statistics-icon">
                    <h1 class="text-primary">{{$countries->count()}}</h1>
                    <h2 class="text-primary">مناطق</h2>
                </div>
                <div class="col-sm-4 text-center mb-5">
                    <img src="{{asset('front/images/users.jpg')}}" alt="" class="statistics-icon">
                    <h1 class="text-primary">10,000</h1>
                    <h2 class="text-primary">مستخدم في السعودية</h2>
                </div>
                <div class="col-sm-4 text-center mb-5">
                    <img src="{{asset('front/images/service1.png')}}" alt="" class="statistics-icon">
                    <h1 class="text-primary">50,000</h1>
                    <h2 class="text-primary">طلب ناجح</h2>
                </div>
            </div>
        </div>
    </section>
    <!-- statistics -->

    <!-- categories -->
    <section class="categories pt-5 pb-5">
        <div class="container">
            <h3 class="text-primary">التخصصات</h3>
            <div class="owl-carousel categories-carousel">
                <div class="categories-item">
                    <a href="#"></a>
                    <div class="img-box">
                        <img src="{{asset('front/images/service1.png')}}" alt="">
                    </div>
                    <h4>الحالات الطارئة</h4>

                </div>
                <div class="categories-item">
                    <a href="#"></a>
                    <div class="img-box">
                        <img src="{{asset('front/images/service2.png')}}" alt="">
                    </div>
                    <h4>الحالات المستقرة</h4>

                </div>
                <div class="categories-item">
                    <a href="#"></a>
                    <div class="img-box">
                        <img src="{{asset('front/images/service3.png')}}" alt="">
                    </div>
                    <h4>التجمعات والمناسبات</h4>

                </div>
                <div class="categories-item">
                    <a href="#"></a>
                    <div class="img-box">
                        <img src="{{asset('front/images/service4.png')}}" alt="">
                    </div>
                    <h4>مشتركى المرافق الطبية</h4>
                </div>
                <div class="categories-item">
                    <a href="#"></a>
                    <div class="img-box">
                        <img src="{{asset('front/images/service5.png')}}" alt="">
                    </div>
                    <h4>نقل الجثمان</h4>
                </div>
                <div class="categories-item">
                    <a href="#"></a>
                    <div class="img-box">
                        <img src="{{asset('front/images/service6.png')}}" alt="">
                    </div>
                    <h4>كبار السن وذوى الاحتياجات"</h4>
                </div>
                <div class="categories-item">
                    <a href="#"></a>
                    <div class="img-box">
                        <img src="{{asset('front/images/service7.png')}}" alt="">
                    </div>
                    <h4>أقرب صيدلية</h4>
                </div>
                <div class="categories-item">
                    <a href="#"></a>
                    <div class="img-box">
                        <img src="{{asset('front/images/service8.png')}}" alt="">
                    </div>
                    <h4>أقرب مستشفى</h4>
                </div>
                <div class="categories-item">
                    <a href="#"></a>
                    <div class="img-box">
                        <img src="{{asset('front/images/service9.png')}}" alt="">
                    </div>
                    <h4>أرقام الطوارئ</h4>
                </div>
            </div>
        </div>
    </section>
    <!-- categories -->

    <!-- join us -->
    <section class="join-us pt-5 pb-5">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 mb-3 offset-sm-3">
                    <div class="join-us-box">
                        <h3 class="text-primary"><img src="{{asset('front/images/join-us.png')}}" alt=""> انضم إلينا
                            كشريك نجاح</h3>
                        <p>ابدأ مرحلة جديدة من النجاح مع مُعين ، كن من ضمن المميزين ، هدفنا نقل نشاطك التجاري إلى مرحلة
                            مختلفة من التنافس</p>
                        <h3 class="text-primary text-center">انضم الان</h3>
                        <a href="{{url('partner-signup')}}" class="join-btn shadow"><i
                                class="fas fa-2x fa-arrow-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- join us -->

    <!-- video -->
    <section class="video-intro pt-5 pb-5 bg-secondary">
        <div class="container">
            <h3 class="text-primary text-center mb-5">
                شرح استخدام التطبيق
            </h3>
            <div class="row">
                <div class="col-sm-6 offset-sm-3">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/1LSKA6wWHIg"
                                allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- video -->

@endsection
