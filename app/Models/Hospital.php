<?php

namespace App\Models;

use App\MyHelper\Helper;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Role;

class Hospital extends Model 
{

    protected $table = 'hospitals';
    public $timestamps = true;
    protected $fillable = array('latitude', 'longitude');

    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }

    public function hospitalUsers()
    {
        return $this->hasMany(HospitalUser::class);
    }

    public function orderRejectReasons()
    {
        return $this->morphMany(DeliveryRejectReasons::class,'rejectable');
    }

    public function orders()
    {
        return $this->hasMany(Order::class)->where('service_id','!=',3);
    }

    public function events()
    {
        return $this->belongsToMany(Order::class,'event_hospital_requests');
    }

    public function orderDoctors()
    {
        return $this->belongsToMany(Order::class)->where('type','doctor')->withPivot('type','number');
    }

    public function orderNurses()
    {
        return $this->belongsToMany(Order::class)->where('type','nurse')->withPivot('type','number');
    }

    public function requestOrders()
    {
        return $this->belongsToMany(Order::class , 'hospital_order_requests')->withPivot('type','number');
    }

    public function requestOrderDoctors()
    {
        return $this->belongsToMany(Order::class , 'hospital_order_requests')->where('type','doctor')->withPivot('type','number');
    }

    public function requestOrderNurses()
    {
        return $this->belongsToMany(Order::class , 'hospital_order_requests')->where('type','nurse')->withPivot('type','number');
    }

    public function reviews()
    {
        return $this->hasMany(Review::class);
    }

    public function notifications()
    {
        return $this->morphToMany('App\Models\Notification', 'notifiable')->withPivot('is_read');
    }

    public function Roles()
    {
        return $this->hasMany(Role::class, 'guard_id');
    }

    public function users()
    {
        return $this->hasMany(HospitalUser::class);
    }

    public function ambulances()
    {
        return $this->hasMany(Delivery::class ,'hospital_id');
    }

    public function scopeCheckArea($query,$points)
    {
        $distance = (new Helper)->settingValue('hospital_filter_area', 6);

        return $query->where(function ($q) use ($points, $distance){
            $q->select(\DB::raw('*, ( 6367 * acos( cos( radians(' . $points['latitude'] . ') ) * cos( radians( latitude ) ) * 
                    cos( radians( longitude ) - radians(' . $points['longitude'] . ') ) + sin( radians(' . $points['latitude'] . ') ) * sin( radians( latitude ) ) ) ) AS distance'))->having('distance', '<', $distance);
        });

    }

    public function scopeCheckActive($query)
    {
        $query->where('status','active');
    }
}