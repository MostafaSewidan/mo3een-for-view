<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->Increments('id');
            $table->unsignedInteger('order_id');
            $table->enum('type', ['reporter', 'injured']);
            $table->string('name');
            $table->string('national_id');
            $table->string('diseases');
            $table->string('clinic_status');
            $table->integer('length');
            $table->integer('weight');
            $table->enum('blood_type', ['O+', 'O-', 'A+', 'A-', 'AB+', 'AB-', 'B+', 'B-']);
            $table->integer('floor');
            $table->enum('elevator', ['available', 'not-available']);
            $table->integer('cases_num');
            $table->text('notes');
            $table->string('insurance_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cases');
    }
}
