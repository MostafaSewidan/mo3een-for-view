<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CarType extends Model
{

    protected $table      = 'car_types';
    public    $timestamps = true;
    protected $fillable   = array('name', 'km_price', 'minute_price', 'level','min_price');


    public function orders()
    {
        return $this->belongsTo(Order::class);
    }
}
