<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $table      = 'reservations';
    public    $timestamps = true;
    protected $fillable   = array('name', 'client_id', 'event_type', 'cars_num', 'event_from', 'event_to', 'time_from', 'time_to',
                                  'days_num', 'doctors_num', 'nursing_num', 'price', 'notes');
}
