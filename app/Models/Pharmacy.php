<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use JustBetter\PaginationWithHavings\PaginationWithHavings;

class Pharmacy extends Model
{
    use PaginationWithHavings;
    protected $table      = 'pharmacies';
    public    $timestamps = true;
    protected $fillable   = array('delivery_type', 'region_id', 'name', 'longitude', 'latitude','type');
    protected $appends    = array('cover', 'photo','case');

    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }

    public function reviews()
    {
        return $this->morphMany('App\Models\Review', 'reviewable');
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function attachments()
    {
        return $this->morphMany('App\Models\Attachment', 'attachmentable');
    }

    public function getPhotoAttribute()
    {
        return $this->attachments()->where('type', 'avatar')->first() ? $this->attachments()->where('type', 'avatar')->first()->path : null;
    }

    public function getCoverAttribute()
    {
        return $this->attachments()->where('type', 'cover')->first() ? $this->attachments()->where('type', 'cover')->first()->path : null;
    }

    public function getOrdersCountAttribute()
    {
        return $this->orders()->count();
    }

    public function getCaseAttribute()
    {
        if($this->status == 'closed')
            return 'مغلق';

        if($this->status == 'open')
            return 'متاح';
    }

    public function scopeCheckArea($query,$points)
    {

        $distance = Setting::where(['key' => 'pharmacy_filter_area'])->first() ? Setting::where(['key' => 'pharmacy_filter_area'])->first()->value : 6;

        return $query->where(function ($q) use ($points, $distance){
            $q->select(\DB::raw('*, ( 6367 * acos( cos( radians(' . $points['latitude'] . ') ) * cos( radians( latitude ) ) * 
                    cos( radians( longitude ) - radians(' . $points['longitude'] . ') ) + sin( radians(' . $points['latitude'] . ') ) * sin( radians( latitude ) ) ) ) AS distance'))->having('distance', '<', $distance);
        });

    }

}
