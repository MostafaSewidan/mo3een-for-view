<?php

namespace App\Http\Controllers\Api\Provider;

use App\Http\Controllers\Api\ParentApi;
use App\Http\Resources\Provider\RejectOrderReason;
use App\Http\Resources\Review;
use App\Models\Delivery;
use App\Models\Order;
use App\MyHelper\Helper;
use App\MyHelper\Photo;
use Carbon\Carbon;
use Illuminate\Http\Request;

class GeneralTransactionController extends ParentApi
{

    public function __construct()
    {
        $this->helper = new Helper();
        $this->guard = 'api_delivery';
        $this->model = new Delivery();
        $this->table = 'deliveries';
        $this->apiResource = '\App\Http\Resources\Provider\Provider';
        $this->uniqueRow = 'user_name';
    }

    public function rejectOrderReasons()
    {
        $records = \App\Models\RejectOrderReason::where('is_active', 1)->get();

        return $this->helper->responseJson(1, 'SUCCESS', RejectOrderReason::collection($records));

    }

    public function showOrders(Request $request)
    {
        $user = $request->user($this->guard);
        $rules =
            [
                'order_id' => 'exists:orders,id',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $orders = $user->orders()->where('status', '!=', 'cart')->where(function ($q) use ($request) {

            if ($request->status) {
                $q->where('status', $request->status);
            }

            if ($request->order_id) {
                $q->where('id', $request->order_id);
            }

        })->latest('asked_at')->paginate(15);

        $object = $this->guard == 'api_store' ? StoreOrder::collection($orders) : DeliveryOrder::collection($orders);

        return ($object)->additional([
            "status" => 1,
            "massage" => "SUCCESS",
        ]);
    }

    public function listOrder(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'order_id' => 'required|exists:orders,id',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $orders = Order::where(function ($q) use ($request) {

            if ($request->status) {
                $q->where('status', $request->status);
            }

            if ($request->order_id) {
                $q->where('id', $request->order_id);
            }

        })->latest('asked_at')->paginate(15);

        $object = (\App\Http\Resources\Order::collection($orders))->responseTo('delivery');

        return ($object)->additional([
            "status" => 1,
            "massage" => "SUCCESS",
        ]);
    }

    public function lastOrderWithNotification(Request $request)
    {
        $user = $request->user($this->guard);
        $order = $user->notifications()->where('notifications.notifiable_type', 'App\Models\Order')->latest()->first();

        return $this->helper->responseJson(1, 'تمت العملية بنجاح', $order ? (new \App\Http\Resources\Order($order->notifiable))->responseTo('delivery') : (object)[]);
    }

    public function reviews(Request $request)
    {
        $user = $request->user($this->guard);

        $reviews = $user->reviews()->latest()->paginate(15);
        $totalCount = $user->reviews()->count();
        return Review::collection($reviews)->additional([
            "status" => 1,
            "massage" => "SUCCESS",
            'provider_rate' => number_format($user->rate, 1),
            'total_count_reviews' => $totalCount,
            'counts' => [
                '1' => (int)($user->reviews()->where('rate', 1)->count() ? ($user->reviews()->where('rate', 1)->count() / $totalCount) * 100 : 0),
                '2' => (int)($user->reviews()->where('rate', 2)->count() ? ($user->reviews()->where('rate', 2)->count() / $totalCount) * 100 : 0),
                '3' => (int)($user->reviews()->where('rate', 3)->count() ? ($user->reviews()->where('rate', 3)->count() / $totalCount) * 100 : 0),
                '4' => (int)($user->reviews()->where('rate', 4)->count() ? ($user->reviews()->where('rate', 4)->count() / $totalCount) * 100 : 0),
                '5' => (int)($user->reviews()->where('rate', 5)->count() ? ($user->reviews()->where('rate', 5)->count() / $totalCount) * 100 : 0),
            ]
        ]);

    }

    public function updateLocation(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'latitude' => 'required',
                'longitude' => 'required',
            ];


        $validator = validator()->make($request->all(), $rules);


        if ($validator->fails()) {

            return $this->helper->responseJson(0, $validator->errors()->first(), $validator->errors());
        }

        $user->latitude = $request->latitude;
        $user->longitude = $request->longitude;
        $user->save();

        return $this->helper->responseJson(1, 'تم التحديث بنجاح');
    }
}
