<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\EmergencyNumber;
use App\Models\Post;
use App\MyHelper\Helper;
use App\MyHelper\Photo;
use App\MyHelper\PhotoV2;
use Illuminate\Http\Request;
use Response;

class EmergencyController extends Controller
{

    protected $model;
    protected $viewsDomain = 'admin/emergencies.';
    protected $url = 'admin/emergencies/';

    public function __construct()
    {
        $this->model = new EmergencyNumber();
    }

    /**
     * @param $view
     * @param array $params
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $records = $this->model->where(function ($q) use ($request) {

            if ($request->name) {
                $q->where(function ($q) use ($request) {
                    $q->where('title','like', '%' . $request->name . '%');
                });
            }

            if ($request->number) {
                $q->where('number', $request->number);
            }

        })->orderBy('order')->paginate(10);

        return $this->view('index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = $this->model;
        return $this->view('create', compact('model'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules =
            [
                'title' => 'required',
                'number' => 'required',
            ];

        $error_sms =
            [
                'title.required' => 'العنوان  مطلوب',
                'number.required' => 'الرقم مطلوب',
            ];

        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return back()->withInput()->withErrors($data->errors());
        }

        $record = $this->model->create($request->all());

        if (!$request->input('order')) {
            $record->order = $this->model->all()->count();
            $record->save();
        }

        session()->flash('success', 'تمت الاضافة بنجاح');
        return redirect($this->url);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = $this->model->findOrFail($id);
        return $this->view('edit', compact('model'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules =
            [
                'title' => 'required',
                'number' => 'required',
            ];

        $error_sms =
            [
                'title.required' => 'العنوان  مطلوب',
                'number.required' => 'الرقم مطلوب',
            ];


        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {
            return back()->withInput()->withErrors($data->errors());
        }

        $record = $this->model->findOrFail($id);

        $record->update($request->all());

        session()->flash('success', 'تمت تحديث بنجاح');
        return back();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $record = $this->model->findOrFail($id);

        $record->delete();

        $data = [
            'status' => 1,
            'message' => 'تم الحذف بنجاح',
            'id' => $id
        ];

        return Response::json($data, 200);
    }


    public function toggleBoolean($id, $action)
    {
        $record = $this->model->findOrFail($id);

        $activate = Helper::toggleBoolean($record, $action, 1, 0);

        if ($activate) {
            return Helper::responseJson(1, 'تمت العملية بنجاح');
        }

        return Helper::responseJson(0, 'حدث خطأ');
    }
}
