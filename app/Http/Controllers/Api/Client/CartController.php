<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Api\ParentApi;
use App\Http\Resources\OrderCollection;
use App\Models\OrderProduct;
use App\Models\Product;
use App\Models\Store;
use App\MyHelper\Helper;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CartController extends ParentApi
{

    public function __construct()
    {

        $this->helper = new Helper();
    }


    public function addToCart(Request $request)
    {
        $rules =
            [
                'product_id' => 'required|exists:products,id',
                'store_id'   => 'required|exists:stores,id',
                'quantity'   => 'required|numeric|min:1',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $client = $request->user();
        $store = Store::find($request->store_id);
        $product = Product::Store($store->id)->find($request->product_id);

        $cartCheck = null;

        // check if the product in this store
        if (!$product || $product->status != 'active')
            return $this->helper->responseJson(0, 'product id is wrong please try again with valid product');

        //check if the user has cart list or not
        if ($client->orders()->where(['status' => 'cart', 'type' => 'cart'])->count()) {
            //check store in cart
            if (!$client->orders()->CheckStore($request->store_id)->first()) {
                return $this->helper->responseJson(0, 'you can\'t add to cart from this store');
            }

            $cartCheck = $client->orders()->CheckStore($request->store_id)->first();
        }

        DB::beginTransaction();

        if ($cartCheck == null) {
            //if user don't have a cart create new order in status cart
            $cart = $client->orders()->create(['status' => 'cart', 'type' => 'cart', 'delivery_cost' => $store->delivery_cost, 'total_price' => $store->delivery_cost]);
            $cart->products()->attach($store->id);

        } else {

            //if user have a cart
            $cart = $cartCheck;
            //check if product in cart or not
            if ($cart->products()->count() && $cart->products()->where('product_id', $product->id)->first()) {
                return $this->helper->responseJson(0, 'product is already added to cart');
            }
        }

        $productPrice = $product->is_offered == 0 ? $product->price : $product->offer_price;
        $offerPrice = $product->is_offered == 1 ? $product->price - $product->offer_price : 0;

        $cart->products()->attach($product->id,
                                  [
                                      'price'       => $productPrice,
                                      'total_price' => $productPrice * $request->quantity,
                                      'quantity'    => $request->quantity,
                                      'offer_price' => $offerPrice,
                                  ]);

        if ($product->additions()->count()) {
            foreach ($product->additions()->get() as $add) {
                OrderProduct::find($cart->products()->find($product->id)->pivot->id)->additions()->attach($add->id, ['price' => $add->price]);
            }
        }


        $cart->increment('price', $productPrice * $request->quantity);
        $cart->increment('total_price', $productPrice * $request->quantity);
        $cart->increment('offer_price', $offerPrice * $request->quantity);

        DB::commit();
        return $this->helper->responseJson(1, 'تم الاضافة المنتج للعربة بنجاح');
    }

    public function editProductQuantity(Request $request)
    {

        $data = validator()->make($request->all(), ['product_id' => 'required|exists:order_product,id', 'quantity' => 'required|min:1']);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $client = $request->user();
        $cart = $client->orders()->where(['status' => 'cart', 'type' => 'cart'])->first();

        if (!$cart) {
            return $this->helper->responseJson(0, 'العربة خالية من المنتجات');
        }

        $productInCart = OrderProduct::where('order_id', $cart->id)->find($request->product_id);


        if (!$productInCart) {
            return $this->helper->responseJson(0, 'لم يتم االعثور علي المنتج');
        }

        $cart->decrement('price', $productInCart->total_price);
        $cart->decrement('total_price', $productInCart->total_price);
        $cart->decrement('offer_price', $productInCart->offer_price * $productInCart->quantity);

        $productInCart->update(['total_price' => $productInCart->price * $request->quantity, 'quantity' => $request->quantity]);

        $cart->increment('price', $productInCart->price * $request->quantity);
        $cart->increment('total_price', $productInCart->price * $request->quantity);
        $cart->increment('offer_price', $productInCart->offer_price * $request->quantity);

        return $this->helper->responseJson(0, 'تم الاضافة بنجاح');
    }

    public function editAdditionQuantity(Request $request)
    {
        $rules =
            [
                'product_id'  => 'required|exists:order_product,id',
                'addition_id' => 'required|exists:additions,id',
                'quantity'    => 'required|min:0',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $client = $request->user();
        $cart = $client->orders()->where(['status' => 'cart', 'type' => 'cart'])->first();

        if (!$cart) {
            return $this->helper->responseJson(0, 'العربة خالية من المنتجات');
        }

        $productInCart = OrderProduct::where('order_id', $cart->id)->find($request->product_id);

        if ($productInCart) {
            $addition = $productInCart->additions()->find($request->addition_id);

            if ($addition) {
                $cart->decrement('price', $addition->price * $addition->pivot->quantity);
                $cart->decrement('total_price', $addition->price * $addition->pivot->quantity);

                $productInCart->additions()->updateExistingPivot($addition->id, ['quantity' => $request->quantity, 'price' => $addition->price]);

                $cart->increment('price', $addition->price * $request->quantity);
                $cart->increment('total_price', $addition->price * $request->quantity);

                return $this->helper->responseJson(0, 'تم الاضافة بنجاح');
            }

            return $this->helper->responseJson(0, 'لم يتم االعثور علي الاضافة');
        }

        return $this->helper->responseJson(0, 'لم يتم االعثور علي المنتج');

    }

    public function removeFromCart(Request $request)
    {
        $rules =
            [
                'product_id' => 'required|exists:order_product,id',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $client = $request->user();
        $cart = $client->orders()->where(['status' => 'cart', 'type' => 'cart'])->first();

        if (!$cart) {
            return $this->helper->responseJson(0, 'العربة خالية من المنتجات');
        }

        $productInCart = OrderProduct::where('order_id', $cart->id)->find($request->product_id);


        if (!$productInCart) {
            return $this->helper->responseJson(0, 'لم يتم االعثور علي المنتج');
        }

        $cart->decrement('price', $productInCart->total_price);
        $cart->decrement('total_price', $productInCart->total_price);
        $cart->decrement('offer_price', $productInCart->offer_price * $productInCart->quantity);

        $productInCart->additions()->detach();
        $productInCart->delete();

        if (!$cart->products()->count() && $cart->stores()->first()->pivot->type == null) {
            $cart->stores()->detach();
            $cart->delete();
        }

        return $this->helper->responseJson(0, 'تم الحذف بنجاح');
    }

    public function removeAllCart(Request $request)
    {
        $client = $request->user();
        $cart = $client->orders()->where(['status' => 'cart', 'type' => 'cart'])->first();

        if (!$cart) {
            return $this->helper->responseJson(0, 'العربة خالية من المنتجات');
        }

        foreach (OrderProduct::where('order_id', $cart->id)->get() as $product) {
            $product->additions()->detach();
        }

        $cart->products()->detach();
        $cart->stores()->detach();
        $cart->delete();

        return $this->helper->responseJson(1, 'تم الحذف بنجاح');
    }

    public function listCart(Request $request)
    {
        $client = $request->user();
        $cart = $client->orders()->where(['status' => 'cart', 'type' => 'cart'])->first();

        if (!$cart) {
            return $this->helper->responseJson(0, 'العربة خالية من المنتجات');
        }

        $products = OrderProduct::where('order_id', $cart->id)->latest()->paginate(10);

        return (new OrderCollection($products))->additional([
                                                                       "status"        => 1,
                                                                       "massage"       => "SUCCESS",
                                                                       "store"         => new \App\Http\Resources\Pharmacy($cart->stores()->first()),
                                                                       "written_order" =>
                                                                           $cart->stores()->first()->pivot->type != null ?
                                                                               [
                                                                                   'type'              => $cart->stores()->first()->pivot->type,
                                                                                   'order_description' => $cart->stores()->first()->pivot->type == 'photo' ?
                                                                                       asset($cart->stores()->first()->pivot->order_description) : $cart->stores()->first()->pivot->order_description,
                                                                               ]
                                                                               : null,

                                                                       'store_case'                => $cart->stores()->first()->case,
                                                                       'delivery_start_time_range' => $cart->stores()->first()->delivery_start_time,
                                                                       'delivery_end_time_range'   => $cart->stores()->first()->delivery_end_time,
                                                                       'delivery_time_unit'        => 'دقيقة',
                                                                       "delivery_cost"             => $cart->delivery_cost,
                                                                       "price"                     => $cart->price,
                                                                       "total_price"               => $cart->total_price,
                                                                       "offer_price"               => $cart->offer_price,

                                                                   ]);
    }

    public function addTextOrPhoto(Request $request)
    {
        $rules =
            [
                'type'     => 'required|in:photo,text',
                'text'     => 'required_if:type,text',
                'store_id' => 'required:exists:stores,id',
                'photo'    => 'required_if:type,photo|image|mimes:jpeg,jpg,png,gif,svg',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $client = $request->user();
        $store = Store::find($request->store_id);

        $cartCheck = null;

        //check if the user has cart list or not
        if ($client->orders()->where(['status' => 'cart', 'type' => 'cart'])->count()) {
            //check store in cart
            if (!$client->orders()->CheckStore($request->store_id)->first()) {
                return $this->helper->responseJson(0, 'you can\'t add to cart from this store');
            }

            $cartCheck = $client->orders()->CheckStore($request->store_id)->first();
        }

        //create a description if photo return path of the photo , if text return the text
        if ($request->type == 'photo') {
            $image = $request->file('photo');
            $destinationPath = public_path() . '/uploads/thumbnails/written_orders/';
            $extension = $image->getClientOriginalExtension(); // getting image extension
            $name = 'original' . time() . '' . rand(11111, 99999) . '.' . $extension; // renaming image
            $image->move($destinationPath, $name); // uploading file to given

            $description = 'uploads/thumbnails/written_orders/' . $name;
        } else {

            $description = $request->text;
        }

        if ($cartCheck == null) {
            //if user don't have a cart create new order in status cart

            $cart = $client->orders()->create(['status' => 'cart', 'type' => 'cart']);
            $cart->stores()->attach($store->id,
                                    [
                                        'type'              => $request->type,
                                        'order_description' => $description,
                                    ]);

        } else {

            //if user have a cart
            $cartCheck->stores()->updateExistingPivot($store->id,
                                                      [
                                                          'type'              => $request->type,
                                                          'order_description' => $description,
                                                      ]);
        }


        return $this->helper->responseJson(1, 'تم الاضافة بنجاح');
    }
}
