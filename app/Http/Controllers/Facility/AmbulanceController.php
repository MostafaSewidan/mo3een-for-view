<?php

namespace App\Http\Controllers\Facility;

use App\Charts\AmbulanceAvailability;
use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Models\Delivery as Ambulance;
use App\Models\Delivery;
use App\Models\HospitalUser;
use App\Models\Service;
use App\MyHelper\Helper;
use App\MyHelper\Photo;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\User;
use Response;
use Hash;
use Auth;
use Spatie\Permission\Models\Role;

class AmbulanceController extends Controller
{
    protected $model;
    protected $guard = 'facilities';
    protected $url = 'facilities/ambulances/';
    protected $viewsDomain = 'facilities.ambulances.';

    public function __construct()
    {
        $this->model = new Ambulance();
        $this->guard = 'facilities';
    }

    /**
     * @param $view
     * @param array $params
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    public function index(Request $request)
    {
        $facility = auth($this->guard)->user()->hospital;

        $records = $facility->ambulances()->where(function ($q) use ($request) {
            if ($request->name) {
                $q->where(function ($q) use ($request) {

                    $q->where('name', 'LIKE', '%' . $request->name . '%')
                        ->orWhere('email', 'LIKE', '%' . $request->name . '%');
                });
            }
            if ($request->role_name) {

                $q->whereHas('roles', function ($q) use ($request) {

                    $q->where('display_name', 'LIKE', '%' . $request->role_name . '%');
                });
            }

            if ($request->service_id) {

                $q->whereHas('services', function ($q) use ($request) {

                    $q->where('service_id', $request->service_id);
                });
            }

            if ($request->from) {
                $q->whereDate('created_at', '>=', Helper::convertDateTime($request->from));
            }

            if ($request->to) {
                $q->whereDate('created_at', '<=', Helper::convertDateTime($request->to));
            }


        })->latest()->paginate(20);
        return $this->view('index', compact('records'));
    }

    public function create(User $model)
    {
        $facility = auth($this->guard)->user()->hospital;
        $model = new HospitalUser();
        $roles = $facility->roles()->get();

        return $this->view('create', compact('model', 'roles'));
    }

    public function store(Request $request)
    {

        $facility = auth($this->guard)->user()->hospital;

        $rules =
            [
                'name' => 'required',
                'country' => 'required',
                'services' => 'required',
                'd_o_b' => 'required',
                'services.*' => 'exists:services,id',
                'car_type_id' => 'required|exists:car_types,id',
                'city' => 'required',
                'region_id' => 'required|exists:regions,id',
                'user_name' => 'required|unique:deliveries',
                'email' => 'required|email|unique:deliveries',
                'phone' => 'required|unique:deliveries',
                'car_number' => 'required|unique:deliveries',
                'nationality' => 'required',
                'photo' => 'required|image|mimes:jpeg,jpg,png,gif,svg',
                'transport_photo' => 'required|image|mimes:jpeg,jpg,png,gif,svg',
            ];

        $error_sms =
            [
                'name.required' => 'الرجاء ادخال الاسم ',
                'car_type_id.required' => 'الرجاء ادخال نوع السيارة ',
                'services.required' => 'الرجاء ادخال الخدمة ',
                'services.*.exists' => 'الرجاء ادخال الخدمة ',
                'email.unique' => ' البريد الالكتروني موجود بالفعل',
                'email.required' => 'الرجاء ادخال البريد الالكتروني',
                'email.email' => 'الرجاء ادخال البريد الالكتروني بطريقة صحيحة',
                'country.required' => 'المنطقة مطلوبة',
                'city.required' => 'الرجاء ادخال المدينة',
                'region_id.required' => 'الرجاء ادخال الحي',
                'user_name.required' => 'الرجاء ادخال إسم المستخدم ',
                'user_name.unique' => ' إسم المستخدم موجود بالفعل',
                'phone.required' => 'الرجاء ادخال رقم الهاتف',
                'car_number.required' => 'الرجاء ادخال رقم السيارة',
                'car_number.unique' => 'رقم السيارة موجود بالفعل',
                'nationality.required' => 'الرجاء ادخال الجنسية',
                'photo.required' => 'الرجاء ادخال صورة السائق',
                'photo.image' => 'الرجاء التأكد من إدخال صورة',
                'photo.mimes' => 'الرجاء التأكد من إدخال صورة',
                'transport_photo.required' => 'الرجاء ادخال صورة السيارة',
                'transport_photo.image' => 'الرجاء التأكد من إدخال صورة',
                'transport_photo.mimes' => 'الرجاء التأكد من إدخال صورة',

            ];

        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {

            return response()->json([
                'error' => true,
                'errors' => $data->errors(),
                'code' => 400
            ], 400);

        }
        $record = $facility->ambulances()->create($request->all());

        $record->update(['password' => Hash::make($request->password)]);

        $record->services()->attach($request->services);

        Photo::addOriginalPhoto($request->file('photo'), $record, 'deliveries', 'photos');
        Photo::addOriginalPhoto($request->file('transport_photo'), $record, 'transports', 'photos', 'transport');

        session()->flash('success', 'تم إرسال الطلب للإدارة بنجاح');
        return response()->json([
            'success' => true,
            'url' => url('/facilities/ambulances')
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function stats(Request $request)
    {
        $facility = auth($this->guard)->user()->hospital;
        $countries = Country::pluck('name', 'id')->toArray();

        $services = Service::whereIn('type', ['care', 'event', 'provider', 'pharmacy'])

            ->withCount(['workingAmbulances' => function ($query) use ($request,$facility) {
                $query->where('hospital_id' , $facility->id);
            if ($request->country_id) {
                $query->whereHas('region', function ($query) use ($request) {
                    $query->whereHas('city', function ($query) use ($request) {
                        $query->where('country_id', $request->country_id);
                    });
                });
            }
        }])->where(function ($query) use ($request) {

        })->with('workingAmbulances')->get();

        $charts = [];

        foreach ($services as $service)
        {
            $chart = new AmbulanceAvailability();

            $open = $service->workingAmbulances()->where(function($query) use($request){
                if ($request->country_id)
                {
                    $query->whereHas('region',function ($query) use($request){
                        $query->whereHas('city',function ($query) use($request){
                            $query->where('country_id',$request->country_id);
                        });
                    });
                }
            })->get()->where('types','open')->count();

            $busy = $service->workingAmbulances()->where(function($query) use($request){
                if ($request->country_id)
                {
                    $query->whereHas('region',function ($query) use($request){
                        $query->whereHas('city',function ($query) use($request){
                            $query->where('country_id',$request->country_id);
                        });
                    });
                }
            })->get()->where('types','busy')->count();

            $closed = $service->workingAmbulances()->where(function($query) use($request){
                if ($request->country_id)
                {
                    $query->whereHas('region',function ($query) use($request){
                        $query->whereHas('city',function ($query) use($request){
                            $query->where('country_id',$request->country_id);
                        });
                    });
                }
            })->get()->where('types','closed')->count();

//            info($service->name.'['.$open.','.$busy.','.$closed.']');
            $chart->title($service->name)
                ->labels(['متاح', 'مشغول', 'مغلق'])
                ->dataset("الإسعاف",'pie',[$open,$busy,$closed])
                ->backgroundcolor(["rgb(76, 175, 80)","rgb(253, 221, 17)","rgb(175, 20, 20)"]);
            $charts[] = $chart;
        }

        return $this->view('stats', compact('countries', 'services', 'charts'));
    }

    public function show($id)
    {

        $facility = auth($this->guard)->user()->hospital;
        $record = $facility->ambulances()->findOrFail($id);

        $totalCount = $record->reviews()->count();
        $reviews = [
            "status" => 1,
            "massage" => "SUCCESS",
            'provider_rate' => number_format($record->rate, 1),
            'total_count_reviews' => $totalCount,
            'counts' => (new Helper())->ratePresent($record , $record->reviews->count())
        ];
        return $this->view('show', compact('record','reviews'));
    }

    public function edit($id)
    {

        $facility = auth($this->guard)->user()->hospital;
        $model = $facility->ambulances()->findOrFail($id);

        return $this->view('edit', compact('model'));
    }

    public function update(Request $request, $id)
    {
        $facility = auth($this->guard)->user()->hospital;
        $record = $facility->ambulances()->findOrFail($id);

        $rules =
            [
                'name' => 'required',
                'country' => 'required',
                'services' => 'required',
                'd_o_b' => 'required',
                'services.*' => 'exists:services,id',
                'car_type_id' => 'required|exists:car_types,id',
                'city' => 'required',
                'region_id' => 'required|exists:regions,id',
                'user_name' => 'required|unique:deliveries,user_name,'.$record->id,
                'email' => 'required|email|unique:deliveries,email,'.$record->id,
                'phone' => 'required|unique:deliveries,phone,'.$record->id,
                'car_number' => 'required|unique:deliveries,car_number,'.$record->id,
                'nationality' => 'required',
                'photo' => 'image|mimes:jpeg,jpg,png,gif,svg',
                'transport_photo' => 'image|mimes:jpeg,jpg,png,gif,svg',
            ];

        $error_sms =
            [
                'name.required' => 'الرجاء ادخال الاسم ',
                'car_type_id.required' => 'الرجاء ادخال نوع السيارة ',
                'services.required' => 'الرجاء ادخال الخدمة ',
                'services.*.exists' => 'الرجاء ادخال الخدمة ',
                'email.unique' => ' البريد الالكتروني موجود بالفعل',
                'email.required' => 'الرجاء ادخال البريد الالكتروني',
                'email.email' => 'الرجاء ادخال البريد الالكتروني بطريقة صحيحة',
                'country.required' => 'المنطقة مطلوبة',
                'city.required' => 'الرجاء ادخال المدينة',
                'region_id.required' => 'الرجاء ادخال الحي',
                'user_name.required' => 'الرجاء ادخال إسم المستخدم ',
                'user_name.unique' => ' إسم المستخدم موجود بالفعل',
                'phone.required' => 'الرجاء ادخال رقم الهاتف',
                'car_number.required' => 'الرجاء ادخال رقم السيارة',
                'car_number.unique' => 'رقم السيارة موجود بالفعل',
                'nationality.required' => 'الرجاء ادخال الجنسية',
                'photo.required' => 'الرجاء ادخال صورة السائق',
                'photo.image' => 'الرجاء التأكد من إدخال صورة',
                'photo.mimes' => 'الرجاء التأكد من إدخال صورة',
                'transport_photo.required' => 'الرجاء ادخال صورة السيارة',
                'transport_photo.image' => 'الرجاء التأكد من إدخال صورة',
                'transport_photo.mimes' => 'الرجاء التأكد من إدخال صورة',

            ];

        $data = validator()->make($request->all(), $rules, $error_sms);

        if ($data->fails()) {

            return response()->json([
                'error' => true,
                'errors' => $data->errors(),
                'code' => 400
            ], 400);

        }

        $record->services()->sync($request->services);

        if($request->hasFile('photo'))
        {
            Photo::updatePhoto($request->file('photo'), $record->photos()->where('usage', null)->first() , $record, 'deliveries', 'photos');
        }
        if($request->hasFile('transport_photo'))
        {
            Photo::updatePhoto($request->file('transport_photo'), $record->photos()->where('usage', 'transport')->first() , $record, 'transports', 'photos', 'transport');
        }

        session()->flash('success', 'تم التحديث بنجاح بنجاح');
        return response()->json([
            'success' => true,
            'url' => url('/facilities/ambulances/'.$record->id.'/edit')
        ]);

    }

    public function destroy($id)
    {
        $facility = auth($this->guard)->user()->hospital;
        $record = $facility->ambulances()->findOrFail($id);

        if (auth($this->guard)->user()->id == $record->id) {
            $data = [
                'status' => 0,
                'msg' => 'This email, you cannot deactivate it',
                'id' => $id
            ];

            return Response::json($data, 200);

        }

        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $record->delete();

        $data = [
            'status' => 1,
            'msg' => 'Deleted successfully',
            'id' => $id
        ];

        return Response::json($data, 200);
    }

    public function toggleBoolean($id, $action)
    {

        $facility = auth($this->guard)->user()->hospital;
        $record = $facility->ambulances()->findOrFail($id);

        if ($action == 'types') {
            $open = 'open';
            $close = 'closed';
        } else {
            $open = 1;
            $close = 0;
        }

        $activate = Helper::toggleBoolean($record, $action, $open, $close);

        if ($activate) {
            return Helper::responseJson(1, 'تمت العملية بنجاح');
        }

        return Helper::responseJson(0, 'حدث خطأ');
    }

    public function home()
    {
        return view('facilities.layouts.home');
    }



    /**
     * @param Request $request
     * @return array|string
     * @throws \Throwable
     */
    public function getAmbulance(Request $request)
    {
        $facility = auth($this->guard)->user()->hospital;
        $delivery = $facility->ambulances()->find($request->ambulance_id);

        $view = $this->view('get-ambulance',compact('delivery'))->render();
        return ['view' => $view];
    }
}
