<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WalletTransaction extends Model
{

    protected $table = 'wallet_transactions';
    public $timestamps = true;
    protected $fillable = array('walletable_id','walletable_type', 'order_id', 'balance_before', 'amount', 'balance_after', 'action');

    public function walletable()
    {
        return $this->morphTo();
    }

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

}