<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsCategoriesTable extends Migration {

	public function up()
	{
		Schema::create('settings_categories', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name')->nullable();
			$table->string('phone');
			$table->enum('gender', array(''))->nullable();
			$table->date('d_o_b')->nullable();
			$table->string('pin_code')->nullable();
			$table->enum('status', array(''));
			$table->datetime('pin_code_date_expired')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('settings_categories');
	}
}