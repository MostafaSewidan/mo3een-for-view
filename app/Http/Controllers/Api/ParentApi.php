<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\Notification as NotificationResources;
use App\Http\Resources\PointsReport;
use App\Models\CharitableActor;
use App\Models\Notification;
use App\Models\Contact;
use App\Models\Delivery;
use App\Models\Setting;
use App\MyHelper\Helper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ParentApi extends Controller
{

    public $helper;
    public $guard;
    public $model;
    public $table;
    public $uniqueRow;
    public $sendPinCodeErrorMessage;
    public $apiResource;
    public $pointsCosts = ['api_client' => 'client_points_cost', 'api_delivery' => 'provider_points_cost'];
    public $relations = ['api_client' => 'clients', 'api_delivery' => 'deliveries'];

    /**
     * @return int
     */
    public function getPinCode(): int
    {
        //TODO rand when confirm rand(1000 , 9999)
        return 1111;
    }

    /**
     * @return string
     */
    public function getPinCodeExpiredDate(): string
    {
        return Carbon::now()->addMinutes(1);
    }

    /**
     * @return string
     */
    public function checkPinCodeExpiredDate($expired_date): string
    {
        $check = Carbon::now() > $expired_date ? false : true;
        return $check;
    }

    public function sendPinCode(Request $request, $scape_rules = false, $sendTo = 'phone', $model = null)
    {
        if ($scape_rules == false) {
            $rules = [$this->uniqueRow => 'required|exists:' . $this->table . ',' . $this->uniqueRow];

            $sms =
                [
                    $this->uniqueRow . '.required' => 'المدخلات مطلوبة',
                    $this->uniqueRow . '.exists' => ' لم تم العثور علي حسابك ',
                ];

            $data = validator()->make($request->all(), $rules, $sms);

            if ($data->fails()) {

                return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
            }
        }

        //resend pin code in update phone
        if ($request->user($this->guard)) {

            $record = auth($this->guard)->user();

            $pin_code = $this->getPinCode();
            $record->pin_code = $pin_code;

            //TODO
//           $this->sendSmsWithCode($record->phone , $pin_code);

            $record->pin_code_date_expired = $this->getPinCodeExpiredDate();
            $record->save();

            return $this->helper->responseJson(1, 'تم ارسال الكود بنجاح');
        }

        // resend pin code in register or login
        $record = $model == null ? $this->model->where($this->uniqueRow, $request[$this->uniqueRow])->first() : $model;

        if ($record) {

            $pin_code = $this->getPinCode();
            $record->pin_code = $pin_code;

            //TODO
//            $this->sendSmsWithCode($record->phone , $pin_code);

            $record->pin_code_date_expired = $this->getPinCodeExpiredDate();
            $record->save();

            return $this->helper->responseJson(1, 'تم ارسال الكود بنجاح');
        }
        return $this->helper->responseJson(0, $this->sendPinCodeErrorMessage);
    }

    public function showProfile(Request $request)
    {
        $user = $request->user($this->guard);

        return $this->helper->responseJson(1, 'SUCCESS', ['token' => $request->bearerToken(), 'user' => new $this->apiResource($user)]);

    }

    public function showWalletBalance(Request $request)
    {
        $user = $request->user($this->guard);

        return $this->helper->responseJson(1, 'SUCCESS', ['wallet_balance' => $user->wallet_balance]);

    }

    public function logOut(Request $request)
    {
        $rules =
            [
                'token' => 'required|exists:tokens,token',
            ];


        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $request->user($this->guard)->tokens()->where('token', $request->token)->delete();

        return $this->helper->responseJson(1, 'SUCCESS');
    }


    public function resetPasswordOutAuth(Request $request)
    {

        $rules =
            [
                'name' => 'required',
                'pin_code' => 'required|numeric',
                'password' => 'required|confirmed|min:6',
            ];


        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $record = $this->model->where('user_name', $request->name)->first();

        if ($record) {
            if ($record->pin_code == $request->pin_code) {

                //check pin code date time expired
                if ($this->checkPinCodeExpiredDate($record->pin_code_date_expired)) {

                    $record->pin_code = $this->getPinCode();
                    $record->password = Hash::make($request->password);
                    $record->save();

                    return $this->helper->responseJson(1, 'تم تغيير كلمة المرور بنجاح');
                }

                return $this->helper->responseJson(0, 'انتهت صلاحية كود التفعيل , حاول مرة اخري');
            }

            return $this->helper->responseJson(0, 'كود التاكيد خطأ');
        }
        return $this->helper->responseJson(0, 'اسم المستخدم غير صحيح');

    }

    public function resetPasswordInAuth(Request $request)
    {
        $rules =
            [
                'password' => 'required|confirmed|min:6',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $record = auth($this->guard)->user();
        $record->password = Hash::make($request->password);
        $record->save();

        return $this->helper->responseJson(1, 'عملية ناجحة');

    }

    public function resetPassword(Request $request)
    {
        return auth($this->guard)->user() ? $this->resetPasswordInAuth($request) : $this->resetPasswordOutAuth($request);
    }

    public function acceptPrivacy(Request $request)
    {
        $user = $request->user($this->guard);

        $user->accept_privacy = 1;
        $user->save();

        return $this->helper->responseJson(1, 'SUCCESS', ['token' => $request->bearerToken(), 'user' => new $this->apiResource($user)]);
    }

    /////////////////////////////////////////////

    public function contactUs(Request $request)
    {
        $rules =
            [
                'name' => !auth($this->guard)->check() ? 'required' : '',
                'phone' => !auth($this->guard)->check() ? 'required' : '',
                'contact_reason_id' => 'required|exists:contact_reasons,id',
                'contact' => 'required',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        auth($this->guard)->check() ? $request->user($this->guard)->contacts()->create(['contact' => $request->contact, 'contact_reason_id' => $request->contact_reason_id])
            : Contact::create($request->all());

        return $this->helper->responseJson(1, 'تم الارسال بنجاح');
    }


    ///////// Notifications //////////////////////////


    public function notifications(Request $request)
    {
        $records = $request->user($this->guard)->notifications()->where(function ($q) use ($request) {

            if ($request->notification_id) {

                $q->where('notification_id', $request->notification_id);
            }

        })->latest()->paginate(20);


        return (NotificationResources::collection($records))->responseTo($this->guard == 'api_delivery' ? 'delivery' : 'client')->additional([
            'status' => 1,
            'massage' => 'تمت العملية',
        ]);
    }

    public function deleteNotification(Request $request)
    {
        $relations = $this->relations[$this->guard];
        $user = $request->user($this->guard);

        $rules =
            [
                'notification_id' => 'required|exists:notifications,id'
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first());
        }

        $notification = $user->notifications()->find($request->notification_id);

        if (!$notification)
            return $this->helper->responseJson(0, 'حدث خطأ ما');

        $notification->$relations()->detach($user->id);

        if (!$notification->clients()->count() || !$notification->deliveries()->count())
            $notification->delete();

        return $this->helper->responseJson(1, 'تم الحذف بنجاح');
    }

    public function readNotification(Request $request)
    {
        $relations = $this->relations[$this->guard];
        $user = $request->user($this->guard);

        $rules =
            [
                'notification_id' => 'required|exists:notifications,id'
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first());
        }

        $notification = $user->notifications()->find($request->notification_id);

        if (!$notification)
            return $this->helper->responseJson(0, 'حدث خطأ ما');

        $notification->$relations()->updateExistingPivot($user->id, ['is_read' => 1]);

        return $this->helper->responseJson(1, 'تمت العملية');
    }

    ///////// ///////////// //////////////////////////


    ////////////// points cycle ////////////////////

    public function changePoints(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'amount' => 'required',
                'type' => 'in:change,donation',
                'charitable_actor_id' => 'required_if:type,donation',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }


        $oldPoints = $user->pointsTransactions()->latest()->first();
        $oldWallet = $user->walletTransactions()->latest()->first();

        if ($oldPoints && $oldPoints->points_after > $request->amount) {
            $pointsCost = Setting::where(['key' => $this->pointsCosts[$this->guard]])->first();

            if ($pointsCost) {

                DB::beginTransaction();

                $rechargeAmount = $pointsCost->value * $request->amount;

                if ($request->type == 'donation') {
                    $charitable_actor = CharitableActor::find($request->charitable_actor_id);

                    if (!$charitable_actor)
                        return $this->helper->responseJson(0, 'الجهة التي تود التبرع لها غير متاحه حاليا');

                    $user->pointsTransactions()->create(
                        [
                            'points_before' => $oldPoints->points_after,
                            'quantity' => -$request->amount,
                            'points_after' => $oldPoints->points_after - $request->amount,
                            'factor' => $pointsCost->value,
                            'type' => 'donation',
                        ]
                    );

                } else {

                    $user->pointsTransactions()->create(
                        [
                            'points_before' => $oldPoints->points_after,
                            'quantity' => -$request->amount,
                            'points_after' => $oldPoints->points_after - $request->amount,
                            'factor' => $pointsCost->value,
                            'type' => 'change',
                        ]
                    );

                    $user->walletTransactions()->create(
                        [
                            'balance_before' => $oldWallet ? $oldWallet->balance_after : 0,
                            'amount' => $rechargeAmount,
                            'balance_after' => $oldWallet ? $oldWallet->balance_after + $rechargeAmount : $rechargeAmount,
                            'action' => 'points',
                        ]
                    );
                }


                DB::commit();

                return $this->helper->responseJson(1, 'تمت العملية بنجاح', ['token' => $request->bearerToken(), 'user' => new $this->apiResource($user)]);
            } else {
                return $this->helper->responseJson(0, ' شحن المحفظة  غير متوقف حاليا');
            }

        } else {

            return $this->helper->responseJson(0, 'رصيدك من النقاط لا يكفي لإتمام العملية');
        }
    }

    public function pointsReport(Request $request)
    {
        $user = $request->user($this->guard);

        $records = $user->pointsTransactions()->where('type', 'order')->latest()->paginate(25);

        return PointsReport::collection($records)->additional([
            "status" => 1,
            "massage" => "SUCCESS",
        ]);

    }

    /////////////////////////////////////////////
}
