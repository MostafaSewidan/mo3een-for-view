<?php

namespace App\Http\Controllers\Api\Client;

use App\Http\Controllers\Api\ParentApi;
use App\Http\Resources\NotificationOrder;
use App\Http\Resources\Order;
use App\Models\Address;
use App\Models\CarType;
use App\Models\Coupon;
use App\Models\Delivery;
use App\Models\Hospital;
use App\Models\Pharmacy;
use App\Models\Service;
use App\Models\Store;
use App\Models\Setting;
use App\MyHelper\Helper;
use App\MyHelper\Photo;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class OrderController extends ParentApi
{

    public $paymentMethod;

    /**
     * OrderController constructor.
     */
    public function __construct()
    {

        $this->helper = new Helper();
        $this->guard = 'api_client';
        $this->paymentMethod = new PaymentMethodController();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function parentOrder(Request $request)
    {
        $user = $request->user($this->guard);

        $rules =
            [
                'service_id' => 'required|exists:services,id',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $service = Service::find($request->service_id);

        $type = $service->type;

        return $this->$type($request, $service, $user);

    }

    /**
     * @param Request $request
     * @param $service
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function event(Request $request, $service, $user)
    {

        $rules =
            [
                'service_id' => 'required|exists:services,id',

                // addresses
                'start_address_type' => 'required|in:address,location',

                'start_latitude' => 'required_if:start_address_type,location',
                'start_longitude' => 'required_if:start_address_type,location',
                'start_address_title' => 'required_if:start_address_type,location',

                'start_address' => 'required_if:start_address_type,address',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        DB::beginTransaction();

        $address = $this->getStartAddress($request, $user);

        if (!$address) {
            return $this->helper->responseJson('0', 'العنوان غير صحيح حاول مرة اخري');
        }

        $order = $service->orders()->create([
            'client_id' => $user->id,
            'start_address_id' => $address->id,
//            'start_price_range' => $priceRange['start_price'],
//            'end_price_range' => $priceRange['end_price'],
            'phone' => $request->input('phone', $user->phone),
            'asked_at' => Carbon::now(),
//            'arrival_Expected_time' => $this->getExpectedTime($request),
//            'distance' => $request->distance,
        ]);

        $patient = $order->patient()->create($request->all());
        $patient->event_start_date = Carbon::parse($request->event_start_date . ' ' . $request->event_start_time)->toDateTimeString();
        $patient->event_end_date = Carbon::parse($request->event_end_date . ' ' . $request->event_end_time)->toDateTimeString();
        $patient->save();
        DB::commit();

        $order->refresh();

        $hospitals = Hospital::CheckArea(['latitude' => $address->latitude, 'longitude' => $address->longitude])->CheckActive();
        $notifierIds = $hospitals->pluck('id')->toArray();

        if (count($notifierIds)) {
            $notification = $order->notifications()->create([
                'title' => 'يوجد ليدك طلب اعياد ومناسبات جيد',
                'body' => ' سيارة إسعاف  لمناسبة لديه' . $request->cars_number . ' قام عميل في نطاقك بطلب عدد '
            ]);

            $order->hospitals()->attach($notifierIds);
            $notification->hospitals()->attach($notifierIds);
        }

        return $this->helper->responseJson('1', 'تم الطلب بنجاح', new Order($order));

    }

    /**
     * @param Request $request
     * @param $service
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function care(Request $request, $service, $user)
    {
        $formData = [

        ];

        return $this->makeOrder($request, $service, $user, $formData);
    }

    /**
     * @param Request $request
     * @param $service
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function provider(Request $request, $service, $user)
    {
        $formData = [

        ];

        return $this->makeOrder($request, $service, $user, $formData);
    }

    /**
     * @param Request $request
     * @param $service
     * @param $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function pharmacy(Request $request, $service, $user)
    {

        $rules =
            [
                'latitude' => 'required',
                'longitude' => 'required',
                'time' => 'required',
                'distance' => 'required',
                'pharmacy_id' => 'required|exists:pharmacies,id',
                'photo' => 'nullable|image|mimes:jpeg,jpg,png,gif,svg',
            ];


        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {

            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        if (!$request->hasFile('photo') && !$request->input('description')) {
            return $this->helper->responseJson('0', 'الرجاء قم بإدخال صورة أو وصف لطلبك');
        }

        $address = $user->addresses()->create([
            'title' => $request->input('start_address_title'),
            'latitude' => $request->latitude,
            'longitude' => $request->longitude,
            'is_saved' => 0,
        ]);

        if (!$address) {
            return $this->helper->responseJson('0', 'العنوان غير صحيح حاول مرة اخري');
        }

        $pharmacy = Pharmacy::find($request->pharmacy_id);

        //TODO
        $priceRange = $this->CalculateDistancePrice($request->time, $request->distance, $carType = 'default', $user);


        DB::beginTransaction();

        $endAddress = Address::create([
            "title" => optional($pharmacy)->name,
            'is_saved' => 0,
            'latitude' => $pharmacy->latitude,
            'longitude' => $pharmacy->longitude,
        ]);

        $order = $service->orders()->create([
            'client_id' => $user->id,
            'car_type_id' => $request->car_type_id,
            'start_address_id' => $address->id,
            'end_address_id' => $endAddress->id,
            'pharmacy_id' => $pharmacy->id,
            'delivery_id' => $request->delivery_id,
            'payment_method_id' => $request->input('payment_method_id', 3),
            'phone' => $request->input('phone', $user->phone),
            'asked_at' => Carbon::now(),
            'schedule_date' => Carbon::now(),
            'arrival_Expected_time' => $request->time,
            'description' => $request->input('description', null),
        ]);

        //create a description if photo return path of the photo , if text return the text
        if ($request->hasFile('photo')) {

            Photo::addPhoto($request->file('photo'), $order, 'written_orders');
        }


        $order->refresh();

        DB::commit();

        $deliveries = Delivery::where('types', 'open')->CheckArea(['latitude' => $address->latitude, 'longitude' => $address->longitude]);

        $this->helper->sendNotification(
            $order,
            $deliveries->pluck('id')->toArray(),
            'deliveries',
            'يوجد لديك طلب رحلة جديد ',
            ' برجاء الرد بالقبول أو الرفض ' . $service->name . 'قام عميل بطلب رحلة جديدة من خدمة ',
            'order', new NotificationOrder($order));

        return $this->helper->responseJson('1', 'تم الطلب بنجاح', new Order($order));
    }

    /**
     * @param mixed $time
     * @param mixed $carType
     * @param mixed $distance
     * @param mixed $user
     * @param mixed $scheduleDate
     * @return mixed
     */
    public function CalculateDistancePrice($time, $distance, $carType, $user, $scheduleDate = null)
    {


        $time = Carbon::createFromTimestamp($time)->subHours(2)->toTimeString();

        $minutes = (Carbon::parse($time)->hour * 60) + (Carbon::parse($time)->minute);

        $expectedDate = Carbon::parse($scheduleDate)->addMinutes($minutes)->format('d/m/Y | g:i.a');

        $expectedDateArray = explode('.',$expectedDate);

        $expectedDateArray[1] = $expectedDateArray[1] == 'am' ? ' ص ' : ' م ';
        $expectedDate = implode(' ' , $expectedDateArray);

        if ($carType == 'default') {
            $minPrice = $this->helper->settingValue('minimum_price_trip', 0);
            $kmPrice = $this->helper->settingValue('km_price', 0);
            $minutePrice = $this->helper->settingValue('minute_price', 0);

        } else {

            $minPrice = $carType->min_price;
            $kmPrice = $carType->km_price;
            $minutePrice = $carType->minute_price;
        }

        $specialClientDiscount = $this->helper->settingValue('special_client_discount', 0);

        $startPrice = $distance * $kmPrice;

        if ($user->type == 'special') {
            $startPrice = ($startPrice) - ($startPrice * $specialClientDiscount / 100);
        }

        if ($startPrice < $minPrice)
            $startPrice = $minPrice;

        return [
            'start_price' => $startPrice,
            'end_price' => ($startPrice) + ($startPrice * 10 / 100),
            'expected_date' => $expectedDate,
            'minutes' => $minutes,
        ];
    }

    /**
     * @param float $time
     * @param float $distance
     * @return mixed
     */
    public function CalculateDistancePriceResponse(Request $request)
    {
        $user = $request->user($this->guard);
        $rules =
            [
                'type' => 'required|in:pharmacy,all',
                'car_type_id' => 'required_if:type,all',
                'time' => 'required',
                'distance' => 'required',
                'schedule_date' => 'required',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        if ($request->type == 'all') {

            $carType = CarType::where('is_active', 1)->find($request->car_type_id);

            if (!$carType) {
                return $this->helper->responseJson(0, 'نوع السيارة مطلوب');
            }

        } else {

            $carType = 'default';
        }

        $schedule_date = $request->input('schedule_date', null);

        return $this->helper->responseJson(1, 'تمت العملية بنجاح', $this->CalculateDistancePrice($request->time, $request->distance, $carType, $user, $schedule_date));
    }

    /**
     * @param $latitude
     * @param $longitude
     * @return mixed
     */
    public function nearestFacility($latitude, $longitude)
    {
        $record = Hospital::select(DB::raw('*, ( 6367 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude . ') ) * sin( radians( latitude ) ) ) ) AS distance'))
            ->orderBy("distance")
            ->first();

        return $record;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function nearestFacilityResponse(Request $request)
    {
        $rules =
            [
                'latitude' => 'required',
                'longitude' => 'required',
            ];

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        $record = $this->nearestFacility($request->latitude, $request->longitude);

        if ($record) {
            return $this->helper->responseJson(1, 'تمت العملية بنجاح', new \App\Http\Resources\Hospital($record));
        } else {

            return $this->helper->responseJson(0, 'لم يتم العثور علي منشأه طبية قريبة منك');
        }
    }

    /**
     * @param Request $request
     * @param $user
     * @return mixed
     */
    private function getStartAddress(Request $request, $user)
    {
        if ($request->start_address_type == 'address') {
            $startAddress = $user->addresses()->find($request->start_address);

        } elseif ($request->start_address_type == 'location') {
            $startAddress = $user->addresses()->create([
                'title' => $request->input('start_address_title'),
                'is_saved' => 0,
                'latitude' => $request->start_latitude,
                'longitude' => $request->start_longitude,
            ]);
        }
        return $startAddress;
    }

    /**
     * @param Request $request
     * @param $user
     * @param $startAddress
     * @return  mixed
     */
    private function GetEndAddress(Request $request, $user)
    {
        if ($request->end_address_type == 'address') {
            $endAddress = $user->addresses()->find($request->end_address);

        } else {
            $endAddress = $user->addresses()->create([
                'title' => $request->input('end_address_title'),
                'is_saved' => 0,
                'latitude' => $request->end_latitude,
                'longitude' => $request->end_longitude,
            ]);

        }

        return $endAddress;
    }

    /**
     * @param Request $request
     * @param $service
     * @param $user
     * @return mixed
     * @throws \Exception
     */
    private function makeOrder(Request $request, $service, $user, $formData)
    {
        $addressID = $user->addresses()->pluck('id')->implode(',');

        $rules =
            [

                'car_type_id' => 'required|exists:car_types,id',
                'time' => 'required',
                'distance' => 'required',

                'schedule_date' => 'required|date',

                // addresses
                'start_address_type' => 'required|in:address,location',
                'end_address_type' => 'required|in:address,location,nearest_facility',

                'start_latitude' => 'required_if:start_address_type,location',
                'start_longitude' => 'required_if:start_address_type,location',

                'end_latitude' => 'required_if:end_address_type,location,nearest_facility',
                'end_longitude' => 'required_if:end_address_type,location,nearest_facility',

                'start_address' => 'required_if:start_address_type,address',
                'end_address' => 'required_if:end_address_type,address',

                // payments
                'payment_method_id' => 'required|exists:payment_methods,id',
                //TODO payment methods data
            ];

        $rules += $formData;

        $data = validator()->make($request->all(), $rules);

        if ($data->fails()) {
            return $this->helper->responseJson(0, $data->errors()->first(), $data->errors());
        }

        DB::beginTransaction();

        $carType = CarType::where('is_active', 1)->find($request->car_type_id);

        if (!$carType) {
            return $this->helper->responseJson(0, 'نوع السيارة مطلوب');
        }

        $startAddress = $this->getStartAddress($request, $user);
        $endAddress = $this->GetEndAddress($request, $user);
        $priceRange = $this->CalculateDistancePrice($request->time, $request->distance, $carType, $user);

        if (!$request->start_address_type == 'address') {
            return $this->helper->responseJson('0', 'عنوان البداية غير صحيح حاول مرة اخري');
        }

        if (!$request->end_address_type == 'address') {
            return $this->helper->responseJson('0', 'عنوان النهاية غير صحيح حاول مرة اخري');
        }

        $order = $service->orders()->create([
            'client_id' => $user->id,
            'car_type_id' => $request->car_type_id,
            'start_address_id' => $startAddress->id,
            'end_address_id' => $endAddress->id,
            'start_price_range' => $priceRange['start_price'],
            'end_price_range' => $priceRange['end_price'],
            'payment_method_id' => $request->payment_method_id,
            'phone' => $request->input('phone', $user->phone),
            'asked_at' => Carbon::now(),
            'arrival_Expected_time' => $request->time,
            'distance' => $request->distance,
            'schedule_date' => Carbon::parse($request->schedule_date)->toDateTimeString(),
        ]);

        $patient = $order->patient()->create($request->all());

        DB::commit();

        $order->refresh();

        if ($order->schedule_date->lte(Carbon::now())) {
            $deliveries = Delivery::CheckArea(['latitude' => $startAddress->latitude, 'longitude' => $startAddress->longitude])->where('types', 'open');
            //TODO scadulae
            $this->helper->sendNotification($order, $deliveries->pluck('id')->toArray(), 'deliveries', 'يوجد لديك طلب رحلة جديد ', '  برجاء الرد بالقبول أو الرفض  ' . $service->name . '  قام عميل بطلب رحلة جديدة من خدمة  ', 'order', new NotificationOrder($order));
        }


        return $this->helper->responseJson(1, 'تم التقدم بطلب الرحلة بنجاح');
    }

}
