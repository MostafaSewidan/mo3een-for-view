
{!! \App\MyHelper\Field::text('name' , 'الاسم الصلاحية' )!!}
{!! \App\MyHelper\Field::text('display_name' , 'الاسم المعروض' ) !!}
{!! \App\MyHelper\Field::textarea('description' , 'الوصف' ) !!}
<br>
<div class="form-group" id="permissions_wrap">
    <label for="permissions">الصلاحيات</label>
    <div class="">
        <br>

        <div class="form-group col-lg-12">

            <div class="">
                <span style="    color: white;
    background-color: #3c8cbc;
    padding: 9px 9px;
    border-radius: 6px;">
                    <input type="checkbox" onClick="toggle(this , 'permissions')" name="sellectAll" >

                    <label for="sellectAll">تحديد الكل</label>
                </span>
            </div>
        </div>

        <div class="clearfix"></div>

        @php
            $title = '';
        @endphp
    @foreach( $permissions as $permission)
            @if ($loop->first)
                @php
                    $title = $permission->description;
                @endphp

                <div class="clearfix"></div>
            <br>
            <br>
            <div class="text-center">
                <label style="    font-size: 1.6rem;
    color: #3c8cbc;">{{$permission->description}}</label>
                <hr style="    width: 149px;
    padding: 1px 2px;
    background-color: #3c8cbc;">
            </div>
            @endif
        @if($permission->description != $title)
                    <div class="clearfix"></div>
                    <br>
                    <br>
                    <div class="text-center">
                        <label style="    font-size: 1.6rem;
    color: #3c8cbc;">{{$permission->description}}</label>
                        <hr style="    width: 149px;
    padding: 1px 2px;
    background-color: #3c8cbc;">
                    </div>
        @endif

        @if($model->hasPermissionTo($permission->name))

            {!! \App\MyHelper\Field::checkBox('permissions', $permission  , 'checked') !!}
        @else
            {!! \App\MyHelper\Field::checkBox('permissions', $permission ) !!}
        @endif

        @php
            $title = $permission->description;
        @endphp

    @endforeach
    </div>

    <div class="clearfix"></div>

</div>
<br>
<br>