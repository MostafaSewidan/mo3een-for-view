<?php

namespace App\Http\Resources;

use App\Models\Setting;
use App\MyHelper\Helper;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Client extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'               => $this->id,
            'name'             => $this->name,
            'phone'            => $this->phone,
            "national_ID"      => $this->national_ID,
            "Length"           => $this->Length,
            "the_weight"       => $this->the_weight,
            'blood_type' => $this->bloodType,
            'type' => $this->type,
            "d_o_b"       => $this->d_o_b ? Carbon::parse($this->d_o_b)->format('Y-m-d') : null,
            "age"       => $this->age,
            "gender"       => $this->gender,
            "gender_text"       => $this->gender ?($this->gender == 'male' ? 'ذكر' : 'أنثي') : null  ,
            "nationality"       => $this->nationality,
            "insurance_number" => $this->insurance_number,
            "notes"            => $this->notes,
            "accept"           => $this->accept,
            "chronic_diseases"           => $this->chronic_diseases,
            "photo"           => $this->photo ? asset($this->photo->path) : asset('photos/cartoon.png'),
            'points' => $this->points,
            'points_in_pounds' => $this->points * (new Helper)->settingValue('client_points_cost' , 1),
            'wallet_balance' => $this->wallet_balance,
            'region'           => new Region($this->region),
            'points_to_egp' => (new Helper)->settingValue('client_points_cost' , 1),
        ];
    }
}
