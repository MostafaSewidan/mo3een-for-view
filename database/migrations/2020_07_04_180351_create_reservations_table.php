<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->Increments('id');
            $table->timestamps();
            $table->string('name');
            $table->unsignedInteger('client_id');
            $table->string('event_type');
            $table->integer('cars_num');
            $table->date('event_from');
            $table->date('event_to');
            $table->time('time_from');
            $table->time('time_to');
            $table->integer('days_num');
            $table->integer('doctors_num');
            $table->integer('nursing_num');
            $table->decimal('price');
            $table->text('notes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
