<?php

namespace App\Http\Controllers\Facility;

use App\MyHelper\Helper;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Permission;
use \Spatie\Permission\Models\Role;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Response;

class RoleController extends Controller

{
    protected $model;
    protected $guard = 'facilities';
    protected $url = 'facilities/roles';
    protected $viewsDomain = 'facilities.roles.';

    public function __construct()
    {
        $this->model = new Role();
        $this->guard = 'facilities';
    }

    /**
     * @param $view
     * @param array $params
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    private function view($view, $params = [])
    {
        return view($this->viewsDomain . $view, $params);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $facility = auth($this->guard)->user()->hospital;

        $roles = $facility->Roles()->where(function ($q) use ($request) {

            if ($request->name) {
                $q->where(function ($q) use ($request) {

                    $q->where('name', 'LIKE', '%' . $request->name . '%');
                });
            }

            if ($request->from) {
                $q->whereDate('created_at', '>=', Helper::convertDateTime($request->from));
            }

            if ($request->to) {
                $q->whereDate('created_at', '<=', Helper::convertDateTime($request->to));
            }


        })->latest()->paginate(20);

        return $this->view('index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        Artisan::call('permission:cache-reset');
        $model = new Role();
        $permissions = Permission::Facility()->get();
        return $this->view('create', compact('model', 'permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $facility = auth($this->guard)->user()->hospital;

        $permissions = Permission::Facility()->count() ? 'in:' . Permission::Facility()->pluck('id')->toArray()->implode(',') : '';

        $rules =
            [
                'name'         => 'required|unique:roles',
                'display_name' => 'required',
                'permissions'  => '',
                'permissions.*'  => $permissions,

            ];

        $message =
            [
                'name.required'         => 'الرجاء ادخال الاسم',
                'name.unique'           => 'تم اخال هذه الصلاحية من قبل',
                'display_name.required' => 'الرجاء اخال الاسم المعروض',
            ];

        $data = validator()->make($request->all(), $rules, $message);
        if ($data->fails()) {
            return $data->errors();
            return back()->withInput()->withErrors($data->errors());
        }
        //dd($request->all());
        $record = $facility->Roles()->create($request->except('permissions'));
        $record->permissions()->attach($request->permissions);

        session()->flash('success', 'تمت الاضافة بنجاح');

        return redirect($this->url);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $facility = auth($this->guard)->user()->hospital;

        Artisan::call('permission:cache-reset');
        $model = $facility->Roles()->findOrFail($id);

        $permissions = Permission::Facility()->get();

        return $this->view('edit', compact('model', 'permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $facility = auth($this->guard)->user()->hospital;
        $record = $facility->Roles()->findOrFail($id);
        $permissions = Permission::Facility()->count() ? 'in:' . Permission::Facility()->pluck('id')->toArray()->implode(',') : '';

        $rules =
            [
                'name'         => 'required|unique:roles,name,' . $record->id . '',
                'display_name' => 'required',
                'description'  => 'required',
                'permissions.*'  => $permissions,

            ];

        $message =
            [
                'name.required'         => 'Please Enter the name',
                'name.unique'           => 'The name has been added before',
                'display_name.required' => 'Please,Enter the display name',
                'description.required'  => 'Please,Enter the description',
                'permissions.required'  => 'Please,Enter any permission',

            ];
        $data = validator()->make($request->all(), $rules, $message);

        if ($data->fails()) {
            return back()->withInput()->withErrors($data->errors());
        }

        $record->update($request->except('permissions'));
        $record->permissions()->sync($request->permissions);

        session()->flash('success', 'تم التعديل بنجاح');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $facility = auth($this->guard)->user()->hospital;
        $record = $facility->Roles()->findOrFail($id);

        $users = $record->users()->get();

        if (!count($users)) {

            $record->permissions()->detach();
            $record->delete();

            $data = [
                'status' => 1,
                'message'    => 'تم الحذف بنجاح',
                'id'     => $id
            ];

            return Response::json($data, 200);
        } else {
            $data = [
                'status' => 0,
                'message'    => 'يوجد مستخدمين مرتبطين بهذه الرتبة',
                'id'     => $id
            ];
            return Response::json($data, 200);
        }
    }
}
