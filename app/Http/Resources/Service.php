<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Service extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {

        return [
            'id'                 => $this->id,
            'name'               => $this->name,
            'type'               => $this->type,
            'level'              => $this->level,
            'time'               => $this->time,
            'cost_range'         => $this->cost_range,
            'cost_unit'          => 'ريال',
            'delivery_time_unit' => 'دقيقة',
            'photo'              => $this->attachment ? asset(optional($this->attachment)->path) : null,
        ];
    }
}
