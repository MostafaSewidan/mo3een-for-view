<?php

/*
|--------------------------------------------------------------------------
| facilities Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => ['guest:admin']], function () {
    Route::get('/login', 'AuthController@viewLogin');
    Route::post('/login', 'AuthController@login');
});

Route::group(['middleware' => ['auth:admin']], function () {

    Route::get('/', 'HomeController@index');

    Route::get('home', 'HomeController@index');

    Route::get('reset-password', 'AuthController@ResetPasswordView');
    Route::post('reset-password', 'AuthController@ResetPassword');

    Route::get('settings', 'SettingController@view');
    Route::post('settings', 'SettingController@update');

    Route::resource('developer/setting', 'DeveloperSetting');
    Route::resource('developer/settings/categories', 'SettingCategoryController');

    Route::resource('users', 'UserController');
    Route::get('users/activation/{id}', 'UserController@activation')->name('users.users_activation');

    Route::resource('countries', 'CountryController');
    Route::resource('cities', 'CityController');
    Route::resource('regions', 'RegionController');

    Route::resource('posts', 'PostController');
    Route::get('posts/toggle-boolean/{id}/{action}', 'PostController@toggleBoolean')->name('admin.posts.toggleBoolean');

    Route::resource('emergencies', 'EmergencyController');
    Route::get('emergencies/toggle-boolean/{id}/{action}', 'EmergencyController@toggleBoolean')->name('admin.emergencies.toggleBoolean');

    Route::resource('pharmacies', 'PharmacyController');
    Route::get('pharmacies/toggle-boolean/{id}/{action}', 'PharmacyController@toggleBoolean')->name('admin.pharmacies.toggleBoolean');
});

Route::get('country/cities/{id}', 'CityController@getCity');
Route::get('city/regions/{id}', 'RegionController@getRegion');

