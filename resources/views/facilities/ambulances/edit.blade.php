@extends('facilities.layouts.main',[
                                    'page_header'       => 'سيارات الأسعاف',
                                    'page_description'  => 'تعديل السيارة',
                                'link' => url('facilities/ambulances')
                                ])
@section('content')
    <!-- general form elements -->
    <div class="ibox ibox-primary">
        <!-- form start -->
        {!! Form::model($model,[
                                'url'=>url('facilities/ambulances/'.$model->id),
                                'id'=>'myForm',
                                'role'=>'form',
                                'method'=>'PUT',
                                'files' => true
                                ])!!}

        <div class="ibox-content">
            <div class="clearfix"></div>
            <br>
            @include('facilities.ambulances.form')

            <div class="ibox-footer">
                <button type="submit" class="btn btn-primary" id="myBtn">حفظ</button>
            </div>

        </div>
        {!! Form::close()!!}

    </div><!-- /.box -->

@endsection
